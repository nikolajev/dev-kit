<?php

require_once dirname(__DIR__, 3) . "/vendor/autoload.php";

$dirPath = "/var/www/html/micro-apps/historical-data/data/Binance/historical-data/BTC/NEO/Binance.NEO-BTC.1m";

$candlesFilePath = "$dirPath/20180209-0000__20180209-1200.20180209-0959.json";

// @todo File::json($candlesFilePath)->toArrayObject()->_count()

$countCandles = count(json_decode(file_get_contents($candlesFilePath), true));
$countMissing = count(json_decode(file_get_contents("$dirPath/missing.20180209-0000__20180209-1200.20180209-0959.json"), true));
$DateStart = DateTime::createFromFormat("Ymd-Hi", "20180209-0959");
$DateEnd = DateTime::createFromFormat("Ymd-Hi", "20180209-1200");
$diff = date_diff($DateEnd, $DateStart);
$diffInMinutes = $diff->days * 24 * 60;
$diffInMinutes += $diff->h * 60;
$diffInMinutes += $diff->i;

show(
    $diffInMinutes,
    $countCandles,
    $countMissing,
    $countCandles + $countMissing
);