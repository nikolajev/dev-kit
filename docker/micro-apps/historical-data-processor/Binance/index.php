<?php

require_once dirname(__DIR__, 3) . "/vendor/autoload.php";

require_once dirname(__DIR__, 2) . "/functions.php";

use \Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;

require_once dirname(__DIR__) . "/Lib/Math.php";

// @todo array vs file: array = __(), file = __jsonZip() + string __(null, 'Hello')
function __(array $array = null, string $string = null): ArrayObject
{
    return Data::array($array);
}

// @todo existing file = File::jsonZip(array|StdClass|JsonSerializable $array)->toJsonZipFile(),
// new file = __(array|StdClass|JsonSerializable $array)->toJsonZipFile()...()->save()
// new file shorthand = __jsonZip(string $filePath, array|StdClass|JsonSerializable $array)->...()->save()
// automatically transforms to Data::array()

function compactCandleData_prices($candleData)
{
    // @todo __($candleData)->Columns([1, 2, 3, 4], function($value){ return rtrim($value, 0); })
    $result = [
        rtrim($candleData[1], 0),
        rtrim($candleData[2], 0),
        rtrim($candleData[3], 0),
        rtrim($candleData[4], 0),
    ];
    if (Data::array($result)->_validate()->valuesAreSame()) {
        $result = array_pad([$result[0]], 2, "");
    }

    return $result;
}

function compactCandleData_all($candleData)
{
    // @todo __($candleData)->Columns([1, 2, 3, 4], function($value){ return rtrim($value, 0); }) // ? callback
    // @todo ->Slice(1, 4)
    $result = [];


    if (Data::array(array_slice($candleData, 1, 4))->_validate()->valuesAreSame()) {
        $result[] = $candleData[1];
    } else {
        $result = array_slice($candleData, 1, 4);
    }

    $result = array_merge($result, [$candleData[5]], array_slice($candleData, 7, 4));

    /*
        if ($candleData[1] == "6433.99" && $candleData[5] == 0) {
            show($candleData, $result);
            Data::array($result)
                ->walk(function (&$value) {
                    show($value, empty($value), $value == "0.0");
                    if (empty($value) || $value == "0.0") {
                        return ArrayObject::WALK__UNSET;
                    }
                    $value = rtrim($value, 0);
                })
                ->debugSelected();
            exit;
        }*/


    $Result = Data::array($result)
        ->walk(function (&$value) {
            if (empty($value) || $value == "0.0") {
                //return ArrayObject::WALK__UNSET;
                $value = "";
                return; // continue
            }
            $value = rtrim($value, 0);
        });
    // @todo ->pad(2, "")
    //return array_pad($Result->return(), 2, "");

    // @todo $Result->_count() === 6 && $Result->Slice(1, 5)->_allValuesEqualTo("")
    // @todo! NB!!! Performance issue, PHP process is killed (appeared after adding this section), PC restart helped
    if ($Result->_get()->count() === 6 && Data::array(array_slice($Result->return(), 1, 5))->_validate()->allValuesEqualTo("")) {
        $Result = Data::array(array_slice($Result->return(), 0, 1))->merge([[""],]);
    } elseif ($Result->_get()->count() === 9 && Data::array(array_slice($Result->return(), 4, 5))->_validate()->allValuesEqualTo("")) {
        $Result = Data::array(array_slice($Result->return(), 1, 4))->merge([[""],]);
    }


    return $Result->return();
}

// @todo DRY
/*
function parseIntervalString($interval)
{
    $amount = preg_replace("/[^0-9]+/", "", $interval);
    $measureUnit = preg_replace("/[^a-zA-Z]+/", "", $interval);

    switch ($measureUnit) {
        case "m":
            $measureUnit = "minutes";
            break;
        case "h":
            $measureUnit = "hours";
            break;
        case "d":
            $measureUnit = "days";
            break;
        case "w":
            $measureUnit = "weeks";
            break;
        case "M":
            $measureUnit = "months";
            break;
        default:
            failure("Unknown measure unit inside an interval string '$interval'");
            exit;
    }
    return [$amount, $measureUnit];
}
*/

function prepareMissingPeriods($jsonFiles_data)
{
    $incompleteFilesByTitle = [];

    foreach (array_slice($jsonFiles_data, 1) as $jsonFile_data) {
        $exploded = explode(".", basename($jsonFile_data));
        if (count($exploded) === 3) {
            // @todo Develop something more elegant
            list($requestedTimestamps, $actualTimestamp) = $exploded;
            list($startTimestamp, $a) = explode("__", $requestedTimestamps);

            $startDateTime = \DateTime::createFromFormat("Ymd-Hi", $startTimestamp);
            $actualDateTime = \DateTime::createFromFormat("Ymd-Hi", $actualTimestamp);

            if ($startDateTime >= $actualDateTime) {
                failure();
                showexit($jsonFile_data);
            }
            $incompleteFilesByTitle[$startTimestamp] = $actualTimestamp;
        }
    }

    return $incompleteFilesByTitle;
}

function isInsideMissingPeriod($timestamp, $missingDataPeriodsFromFilenames)
{
    $CurrentDateTime = \DateTime::createFromFormat("Y-m-d H:i:s", $timestamp);

    foreach ($missingDataPeriodsFromFilenames as $startTimestamp => $endTimestamp) {
        $StartDateTime = \DateTime::createFromFormat("Ymd-Hi", $startTimestamp);
        $EndDateTime = \DateTime::createFromFormat("Ymd-Hi", $endTimestamp);

        if ($CurrentDateTime >= $StartDateTime && $CurrentDateTime <= $EndDateTime) {
            return true;
        }
    }

    return false;
}

function processMissingBuffer($missingBuffer, $previousValid, $currentValid)
{
    $result = [];

    $step = Math::divide(Math::subtract($currentValid[1], $previousValid[4]), count($missingBuffer));

    $currentAverage = $currentValid[1];

    foreach ($missingBuffer as $timestamp) {
        //$candleMock = array_pad([], 12, null);

        $previousValidClose = rtrim($previousValid[4], 0);
        $currentValidOpen = rtrim($currentValid[1], 0);

        $currentAverage = rtrim(Math::add($currentAverage, $step), 0);
        if (Data::array([$previousValidClose, $currentValidOpen, $currentAverage])->_validate()->valuesAreSame()) {
            $candleMock = ["$previousValidClose;."];
        } else {
            $candleMock = ["$previousValidClose;$currentValidOpen;$currentAverage"];
        }

        // @todo Can be removed for a better performance
        $timestamp = \DateTime::createFromFormat("Y-m-d H:i:s", $timestamp)->format("YmdHi");
        $result[$timestamp] = $candleMock;
    }


    return $result;
}

function prepareProcessedAllFileTitle($basenameRaw)
{
    // @todo There are better solutions (more stable)
    return "processed.all." . substr($basenameRaw, 8, -5);
}

function prepareProcessedPricesFileTitle($basenameRaw)
{
    // @todo There are better solutions (more stable)
    return "processed.prices." . substr($basenameRaw, 8, -5);
}

// @todo DRY
$_candleAmounts = [
    "1m" => 720,
    "3m" => 960,
    "5m" => 864,
    "15m" => 960,
    "30m" => 960,
    "1h" => 960,
    "2h" => 960,
    "4h" => 960,
    "6h" => 960,
    "8h" => 960,
    "12h" => 960,
    "1d" => 1000,
    "3d" => 1000,
    "1w" => 1000,
    "1M" => 1000,
];

define("DATA_DIR", dirname(__DIR__, 2) . "/historical-data/data/Binance/historical-data");
define("RESULT_DIR", dirname(__DIR__, 2) . "/historical-data-processor/data");
define("TMP_DIR", dirname(__DIR__) . "/tmp");
$dataDir = DATA_DIR;
$tmpDir = TMP_DIR;
$resultDir = RESULT_DIR;

$broker = "Binance";

// @todo Better solution for this path
foreach (
    json_decode(file_get_contents(dirname(__DIR__, 2) . "/historical-data/data/Binance/required-pairs.json"), true)
    as $symbol
) {
    list($assetTitle, $counterAsset) = explode(":", $symbol);

    foreach ($_candleAmounts as $interval => $candleAmount) {

        success("$assetTitle $counterAsset $interval");

        $currentCacheTmpDir = "$tmpDir/cache/$broker/$counterAsset/$assetTitle/$interval";
        $currentDataAllDir = "$resultDir/full/$broker/$counterAsset/$assetTitle/$interval";
        $currentDataPricesDir = "$resultDir/prices/$broker/$counterAsset/$assetTitle/$interval";

        $readyZipFilesAllPaths = \Nikolajev\Filesystem\FilesList::list($currentDataAllDir);
        $readyZipFilesPricesPaths = \Nikolajev\Filesystem\FilesList::list($currentDataPricesDir);

        $countReadyZipFilesAllPaths = count($readyZipFilesAllPaths);
        $countReadyZipFilesPricesPaths = count($readyZipFilesPricesPaths);

        if ($countReadyZipFilesAllPaths == 1 && $countReadyZipFilesPricesPaths == 1) {
            echo "Results already exist ...\n";
            continue;
        } elseif ($countReadyZipFilesAllPaths > 1 || $countReadyZipFilesPricesPaths > 1) {
            failure("Wrong result files amount: $countReadyZipFilesAllPaths/$countReadyZipFilesPricesPaths instead of 1");
            exit;
        }


        // @todo DRY
        $zipTitle = "$broker.$assetTitle-$counterAsset.$interval.zip";
        $currentDataZip = "$dataDir/$counterAsset/$assetTitle/$zipTitle";

        if (!file_exists($currentDataZip)) {
            failure("Collected data missing: $currentDataZip");
            exit;
        }

        // @todo:Data::array __(['new'])->debugSelected();

        $prepareRawCandles = function () use ($dataDir, $tmpDir, $currentCacheTmpDir, $broker, $assetTitle, $counterAsset, $interval) {

            // @todo:debugger echoln();
            echo date("H:i:s") . " Merging data ...\n";


            $currentTmpDir = "$tmpDir/$broker/$counterAsset/$assetTitle/$interval";
            $zipTitle = "$broker.$assetTitle-$counterAsset.$interval.zip";
            $currentDataZip = "$dataDir/$counterAsset/$assetTitle/$zipTitle";

            // @todo Linux::remove()
            exec("rm -rf $currentTmpDir");
            // @todo Linux::makeDir()
            exec("mkdir -p $currentTmpDir");
            exec("mkdir -p $currentCacheTmpDir");
            // @todo Linux::copy()
            exec("cp -R $currentDataZip $currentTmpDir");
            // @todo Linux::unzip("$currentTmpDir$zipTitle")->remove();
            exec("cd $currentTmpDir && unzip -q $zipTitle && rm $zipTitle");


            $jsonFiles_data = array_filter(glob("$currentTmpDir/*"), function ($element) {
                return is_file($element) && substr(basename($element), 0, 7) !== 'missing';
            });

            $missingPeriods = prepareMissingPeriods($jsonFiles_data);


            /**
             * @todo Data::string($jsonFiles_data[0]) // NB!!! not worth it.
             * ->explodeMultiDimensional() || ->explode_multiDimensional(
             * [
             *  ".",
             *  basename($jsonFiles_data[0]),
             *  [$requestTimestamps, $actualTimestamp],
             * ],
             * [
             *  "__",
             *  $requestTimestamps,
             *  []
             * ]
             * )
             *
             */

            // @todo ->_explode(".:0;__:1") // '_' in '_explode' returns a string
            // @todo ->explode() returns an array

            // @todo Data::array(...)->list($a, $b, $c); show($a) vs echo($a) + echoln()

            list($requestTimestamps, $actualTimestamp) = explode(".", basename($jsonFiles_data[0]));
            $startTimestamp = $actualTimestamp !== "json" ? $actualTimestamp : $requestTimestamps;
            list($requestTimestamps, $extension) = explode(".", basename(array_reverse($jsonFiles_data)[0]));
            if ($extension !== 'json') {
                /*
                failure();
                exit;
                */
                $startTimestamp = $extension;
            }
            list($ignore, $endTimestamp) = explode("__", $requestTimestamps);

            $fileTitleCandles = "raw.all.$broker.$assetTitle-$counterAsset.$interval.{$startTimestamp}__$endTimestamp";
            //$processedAllFilePath = "processed.all.{$startTimestamp}__$endTimestamp";

            $candles = [];
            foreach ($jsonFiles_data as $jsonFile_data) {
                $candles = array_merge($candles, json_decode(file_get_contents($jsonFile_data), true));
                echo ".";
            }

            $jsonFiles_missing = array_filter(glob("$currentTmpDir/*"), function ($element) {
                return is_file($element) && substr(basename($element), 0, 7) === 'missing';
            });
            $missing = [];
            foreach ($jsonFiles_missing as $jsonFile_missing) {
                $missing = array_merge($missing, json_decode(file_get_contents($jsonFile_missing), true));
                echo ".";
            }

            $fileTitleMissing = "missing.candles.$broker.$assetTitle-$counterAsset.$interval.{$startTimestamp}__$endTimestamp";

            // $missingPeriods
            jsonZip($candles, $currentCacheTmpDir, $fileTitleCandles);
            jsonZip($missing, $currentCacheTmpDir, $fileTitleMissing);
            jsonZip($missingPeriods, $currentCacheTmpDir, "missing.periods.$broker.$assetTitle-$counterAsset.$interval.{$startTimestamp}__$endTimestamp");

            exec("rm -rf $currentTmpDir");

            echo "\n"; // @todo:debugger nl();
        };


        $processedZipFilesPaths = \Nikolajev\Filesystem\FilesList::list(
            $currentCacheTmpDir,
            (new \Nikolajev\Filesystem\FilesListParams())
                // @todo FilenamePatterns with basenames, FilepathPatterns with full paths
                ->includedFilenamePatterns(['*/processed.*.json.zip'])
        );

        // @todo Files::list()
        $rawDataZipFilesPaths = \Nikolajev\Filesystem\FilesList::list(
            $currentCacheTmpDir,
            (new \Nikolajev\Filesystem\FilesListParams())
                // @todo FilenamePatterns with basenames, FilepathPatterns with full paths
                ->includedFilenamePatterns(['*/raw.all.*.json.zip'])
        );


        if (count($processedZipFilesPaths) !== 0) {
            echo "Processed json ZIP already exists ...\n";

            // @todo DRY
            $destinationAllDirPath = str_replace("tmp/cache", "data/full", $currentCacheTmpDir);
            exec("mkdir -p $destinationAllDirPath");
            exec("cp -R {$processedZipFilesPaths[0]} $destinationAllDirPath"); // processed.all

            $destinationPricesDirPath = str_replace("tmp/cache", "data/prices", $currentCacheTmpDir);
            exec("mkdir -p $destinationPricesDirPath");
            exec("cp -R {$processedZipFilesPaths[1]} $destinationPricesDirPath"); // processed.prices

            exec("rm -rf $currentCacheTmpDir");
            continue;
        } elseif (count($rawDataZipFilesPaths) === 0) {
            $prepareRawCandles();

            // @todo DRY
            $rawDataZipFilesPaths = \Nikolajev\Filesystem\FilesList::list(
                $currentCacheTmpDir,
                (new \Nikolajev\Filesystem\FilesListParams())
                    // @todo FilenamePatterns with basenames, FilepathPatterns with full paths
                    ->includedFilenamePatterns(['*/raw.all.*.json.zip'])
            );
        }


        $basenameRaw = basename($rawDataZipFilesPaths[0], '.zip');

        list($raw, $dataType, $_broker, $pair, $_interval, $requestTimestamps) = explode(".", $basenameRaw);

        $exploded = explode("__", $requestTimestamps);
        $startDateTime = \DateTime::createFromFormat("Ymd-Hi", $exploded[0]);
        $endDateTime = \DateTime::createFromFormat("Ymd-Hi", $exploded[1]);

// @todo Data::string()->Explode('.')->_set(0, 'missing')->Implode('.')
        $basenameMissing = "missing.candles.$_broker.$pair.$_interval.{$exploded[0]}__{$exploded[1]}.json";


        $rawCandles = fileGetZippedJson($currentCacheTmpDir, $basenameRaw); // @todo Pass full path only
        $missingTimestamps = fileGetZippedJson($currentCacheTmpDir, $basenameMissing);
        $missingDataPeriodsFromFilenames = fileGetZippedJson(
            $currentCacheTmpDir, "missing.periods.$_broker.$pair.$_interval.{$exploded[0]}__{$exploded[1]}.json"
        );

        // @todo DRY
        list($intervalAmount, $units) = parseIntervalString($interval);

        $candleTimestamps = array_keys($rawCandles);

        $missingBuffer = [];
        $previousValid = null;

        $processedAllResult = [];
        $processedPricesResult = [];

        $startDateTimeClone = clone $startDateTime;

        echo date("H:i:s") . " Processing data ...\n";

        while ($startDateTime < $endDateTime) {
            //echo ".";

            $timestamp = $startDateTime->format("Y-m-d H:i:s");

            $candleData = $rawCandles[$timestamp] ?? null;
            $currentValid = $candleData;

            if ($candleData === null) {

                if (
                    in_array($timestamp, $missingTimestamps) || isInsideMissingPeriod($timestamp, $missingDataPeriodsFromFilenames)
                ) {
                    $missingBuffer[] = $timestamp;
                    $startDateTime->modify("+ $intervalAmount $units");
                    continue;
                } else {
                    failure($timestamp);
                    show($missingDataPeriodsFromFilenames);
                    exit;
                }

            } else {

                if (count($missingBuffer) > 0 && $currentValid !== null) {
                    $processedMissingBuffer = processMissingBuffer($missingBuffer, $previousValid, $currentValid);
                    $processedAllResult = array_merge($processedAllResult, $processedMissingBuffer);
                    $processedMissingBufferCompact = processMissingBuffer($missingBuffer, $previousValid, $currentValid);
                    //showexit($processedMissingBufferCompact, $processedPricesResult);
                    // @todo Data::array()->merge() || Data::array()->merge()->reindex(): if you want it to be reindexed
                    // __(...)->mergeReindex(): in order not to loose anything
                    // @todo NB!!! BUG. array_merge reindexes array
                    // @link https://stackoverflow.com/questions/13160237/array-merge-with-indexes
                    //$processedPricesResult = array_merge($processedPricesResult, $processedMissingBufferCompact);
                    $processedPricesResult = $processedPricesResult + $processedMissingBufferCompact;
                    $missingBuffer = [];
                }

            }

            $previousValid = $candleData;

            $processedAllResult[$timestamp] = compactCandleData_all($candleData);
            $timestamp = \DateTime::createFromFormat("Y-m-d H:i:s", $timestamp)->format("YmdHi");
            $processedPricesResult[$timestamp] = compactCandleData_prices($candleData);

            $startDateTime->modify("+ $intervalAmount $units");
        }

        $count = count($processedAllResult) * $intervalAmount;
        $endDateTime->modify("- $count $units");

        if ($endDateTime != $startDateTimeClone) {

            if ($units === 'weeks') {
                $diffInHours = round(
                    (strtotime($endDateTime->format("Y-m-d H:i:s")) -
                        strtotime($startDateTimeClone->format("Y-m-d H:i:s"))) / (60 * 60)
                );
                if ($diffInHours > 168) { // 7 days * 24 hours
                    failure();
                    show($endDateTime, $startDateTimeClone, $units);
                    exit;
                }
            } else if ($units === 'days' && (int)$intervalAmount === 3) {

                $NewEndDate = \DateTime::createFromFormat(
                    "Y-m-d H:i:s",
                    __($processedAllResult)->Keys()->_get()->last()
                )->modify("+ 3 days");

                $NewEndDate->modify("- $count $units");

                if ($NewEndDate != $startDateTimeClone) {
                    failure();
                    showln($endDateTime, $startDateTimeClone, "- $count $units");
                    exit;
                }

            } else {
                failure();
                showln($endDateTime, $startDateTimeClone, "- $count $units");
                exit;
            }
        }

        if (count($processedAllResult) !== count($processedPricesResult)) {
            failure("Compacted data has a different length");
            exit;
        }


        echo date("H:i:s") . " ZIP processed json ... \n";

        $step = $intervalAmount;
        $counter = 0;
        $result = [];
        $firstElem = true;
        $latestCandleData = null;

        foreach ($processedAllResult as $timestamp => $data) {
            if ($firstElem) {
                $timestamp = \DateTime::createFromFormat("Y-m-d H:i:s", $timestamp)->format("YmdHi");
                $result["$broker;$assetTitle-$counterAsset;$units;$timestamp"] = $data;
                $latestCandleData = $data;
                $firstElem = false;
                continue;
            }

            $counter += $step;

            if ($latestCandleData === $data) {
                $data = [];
            } else {
                $latestCandleData = $data;
            }

            $result[$counter] = $data;
        }

        $processedAllFilePath = prepareProcessedAllFileTitle($basenameRaw);
        jsonZip($result, $currentCacheTmpDir, $processedAllFilePath);

        # COMPACT PRICES

        $counter = 0;
        $result = [];
        $firstElem = true;
        $latestCandleData = null;

        foreach ($processedPricesResult as $timestamp => $data) {
            if ($firstElem) {
                $result["$broker;$assetTitle-$counterAsset;$units;$timestamp"] = $data;
                $latestCandleData = $data;
                $firstElem = false;
                continue;
            }
            $counter += $step;

            if ($latestCandleData === $data) {
                $data = [];
            } else {
                $latestCandleData = $data;
            }

            $result[$counter] = $data;
        }

        /*
            __($processedPricesResult)
                ->walk(function () use (&$counter, $step) {
                    $counter += $step;
                    return [ArrayObject::WALK__UPDATE_KEY, $counter]; // NB!!! Low performance
                });
        */

        $processedPricesFilePath = prepareProcessedPricesFileTitle($basenameRaw);
        jsonZip($result, $currentCacheTmpDir, $processedPricesFilePath);

        // @todo DRY
        $destinationAllDirPath = str_replace("tmp/cache", "data/full", $currentCacheTmpDir);
        exec("mkdir -p $destinationAllDirPath");
        exec("cp -R $currentCacheTmpDir/$processedAllFilePath.json.zip $destinationAllDirPath");

        $destinationPricesDirPath = str_replace("tmp/cache", "data/prices", $currentCacheTmpDir);
        exec("mkdir -p $destinationPricesDirPath");
        exec("cp -R $currentCacheTmpDir/$processedPricesFilePath.json.zip $destinationPricesDirPath");

        exec("rm -rf $currentCacheTmpDir");

    }
}