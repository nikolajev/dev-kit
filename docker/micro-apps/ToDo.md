historical-data

--- OK

Run `php micro-apps/historical-data/scripts/Binance/fixCollected.php`

Get this:

```
 SHOW  /var/www/html/micro-apps/historical-data/scripts/Binance/fixCollected.php:116
 0  2
 1  80
 2  80
 3  /var/www/html/micro-apps/historical-data/data/Binance/historical-data/BNB/AAVE/Binance.AAVE-BNB.1w/missing.20030302-0000__20220501-0000.20201012-0000.json
 4  2
 5  0
 EXIT /var/www/html/micro-apps/historical-data/scripts/Binance/fixCollected.php:116
```

Remove `Binance.AAVE-BNB.1w.zip`

A new validation should be added to signal the same error.

### + Update logics

`Problem`: 


File `missing.` has the timestamps that are not inside the fileTitle period

`+` If week ends at the fileTitle endTimestamp, it is also included in `missing.`, but it is a Monday! 
(at least once in collected data)

---

Leave dates in millisecond in `processed.full.*` files (in order to keep original timestamp with seconds)

---

Remove `Binance.NEO-BTC.1m.zip`

Get this:

```
/var/www/html/micro-apps/historical-data/data/Binance/historical-data/BTC/NEO/1m/20180209-0000__20180209-1200.20180209-0959.json
 FAILURE  /var/www/html/micro-apps/historical-data/src/Binance.php:355
121 candles missing
 FAILURE  /var/www/html/micro-apps/historical-data/src/Binance.php:547
Too many candles
 SHOW  /var/www/html/micro-apps/historical-data/src/Binance.php:549
 0  TOTAL candles: 241
 1  MAX allowed candles: 121
 2  /var/www/html/micro-apps/historical-data/data/Binance/historical-data/BTC/NEO/1m/20180209-0000__20180209-1200.20180209-0959.json
 EXIT /var/www/html/micro-apps/historical-data/src/Binance.php:549
```


---

`.../fixCollected.php`

Catch all 

(calculate missing and existing, compare with total available ) 

Update timestamp seconds (only in caught examples)

Update `missing.` files (if required, only in caught examples)

---

Check all collected data (all candle timestamps' seconds should be 00:00)

Check that there are no minute-duplicated candles (only 1 candle for a certain time in hours:minutes)

Get a list of invalid data json files

---

Update all collected data json files (already zipped) + zip back

Get rid of seconds in timestamp keys

---

Update timestamp format in data collection logics (Y-m-d H:i:s -> Y-m-d H:i)

---

Update timestamp format in data processing logics (Y-m-d H:i:s -> Y-m-d H:i)

---

Process all data from scratch

--- 

Continue with data collection

---

Collect ready ZIP files (replace with empty ZIP files).

At first time, collect `.raw, .full.min, .price.min`

Most valuable is `.full.min`

`+` When collecting next time: ignore empty ZIP files

---

Move to proper places from `micro-apps/functions.php`

---

Rename

historical-data -> historical-data-collector

`+` historical-data-processor (already exists)

`+` historical-data-analyzer

#### NB! Better keep them all together:

```
historical-data/collect
historical-data/process
historical-data/analyze
```

---

`Refactor MicroApps` using an updated version of `philippn/php-data`

---

Create a `historical-data` MicroApp as a `standalone project` (appropriate APP)

---

Implement `console-lite` in `historical-data`

---

prepareRequiredSymbols():

add whitelist (list of required assets that are not in TOP segment)

```
whitelist 1: ["ETH", "PYRO"]
whitelist 2: ["ETHBTC", "PYROEUR", "PYROUSDT"]
```

---



