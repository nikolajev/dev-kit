debugger: set global config to throw all warnings as exceptions

---

php-data: _zip($filePath)->isEmpty() 

---

dev-kit: Collect all `@todo` lines and prepare a summary

---

dev-kit: all vendor dirs (composer) to `vendor.zip` (use instead of composer id needed)

---

cache: Protected cache files that are automatically backed up when rewritten (if are not same)

---

php-data: 
_cacheJson() `... .cache.json`
_jsonZip() `... .json.zip`
_twig()
_htmlTwig()

---

php-data:
_a([...])->debug(function(){return $_this->_count(); })

---

php-data:

JSON file might be same or similar (JSON_PRETTY_PRINT, etc.)

```
_json($filePath)->isSame(_json($file2Path)); // false
_json($filePath)->isSimilar(_json($file2Path)); // true
```

---

php-data:
`_a(...)->pad(4, null[default])`

---

debugger: Add execution time in `*h *m *s *ms` after LABELS

---

php-data:
`_a(...)->Columns([... , ...], [... , ...])` - `n` amount of keyOrKeys parameters

---

debugger:
`nl()` - new line `echo "...\n";`

---

micro-app:

Coingecko notifications (existing `nikolajev.ee` CRONTAB)

---

debugger: Add `warning()` - same as `success()` and `failure()`, orange color

---

debugger: global config + local script config for `label format`

```
*(all): typeCounter, callerCounter, caller, timeStart, timeEnd, timeDiff, backtrace
show: callerCounter, timeDiff, caller
warning: typeCounter callerCounter
failure: backtrace
```

---

debugger:

Add counter for labels

```
SHOW  1
...
SHOW 99
```

`typeCounter` - count all SHOW labels, caller does not matter

`callerCounter` - count labels by caller (label type will be always same)

---

php-data:

`_s(...)->parseInt()`

`_s(...)->parseFloat()`

---

php-data:

`_s(...)->parseByRegex(REGEX)`

---

php-data:

`_dir() = directory, _d() -> _date() = DateTime (advanced), _d() is deprecated`

---

debugger: OUTPUT understands, that current terminal output is not on a new line and adds "\n" automatically

Avoid:

```
docker@f62a6f1477af:/var/www/html$ php micro-apps/historical-data/scripts/Binance/fixCollected.php 
..................... SHOW  /var/www/html/micro-apps/historical-data/scripts/Binance/fixCollected.php:68
  test
```

---

debugger: showln() does not display a caller!

---

debugger: EXIT label - no space between label and caller info

---