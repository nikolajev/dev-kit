<?php

require_once dirname(__DIR__, 3) . "/vendor/autoload.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\File;
use Nikolajev\Filesystem\FilesList;
use Nikolajev\Filesystem\FilesListParams;

// @todo FilesList::list() -> Files::list()
// @todo New FILE type: jsonList (json on each line separately except empty lines)
/*
$Files = Files::list(
    __DIR__ . '/data/orderitems', // @todo Implement same path processing as in File - './data'
    null,
    true // return File objects, parse extension and choose object's class automatically, throw exception if required object class does not exist
);

$Data = Data::array()->merge($Files);
*/

$filePaths = FilesList::list(
    __DIR__ . '/data/orderitems', // @todo Implement same path processing as in File - './data'
);

$filesArrays = [];

foreach ($filePaths as $filePath) {
    $filesArrays[] = explode(str_repeat(PHP_EOL, 2), file_get_contents($filePath));
}


$Data = Data::array()->merge($filesArrays);

$filtered = [];

$Data
    ->walk(function ($value) use (&$filtered) {
        // @todo Data::json($value)->toArrayObject();

        if (json_decode($value, true) === null) {
            return; // continue
        }

        $LogItem = Data::array(json_decode($value, true));

        if ($LogItem->_get()->byKeyOrKeys('User id') === "377") {
            $filtered[$LogItem->return()['Order item']['sub_order_id']][] =
                $LogItem
                    ->unset('User id')
                    ->unset('Backtrace')
                    ->select('Order item')
                    // @todo One method for that! ->unsetMultiple()
                    ->unset('sub_order_id')
                    //->unset('type')
                    ->select()
                    ->return();
        }
    });


file_put_contents(__DIR__ . '/result/orderitems.json', json_encode($filtered));