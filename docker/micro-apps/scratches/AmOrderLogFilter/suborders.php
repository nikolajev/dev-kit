<?php

require_once dirname(__DIR__, 3) . "/vendor/autoload.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\FilesList;

$filePaths = FilesList::list(
    __DIR__ . '/data/suborders', // @todo Implement same path processing as in File - './data'
);

$filesArrays = [];

foreach ($filePaths as $filePath) {
    $filesArrays[] = explode(str_repeat(PHP_EOL, 2), file_get_contents($filePath));
}

$Data = Data::array()->merge($filesArrays);

$filtered = [];

$Data
    ->walk(function ($value) use (&$filtered) {

        if (json_decode($value, true) === null) {
            return; // continue
        }

        $LogItem = Data::array(json_decode($value, true));

        if ($LogItem->_get()->byKeyOrKeys('User id') === "377") {
            $filtered[$LogItem->return()['Suborder id']][] =
                $LogItem
                    ->unset('User id')
                    ->unset('Suborder id')
                    ->unset('Backtrace')
                    ->return();
        }
    });

file_put_contents(__DIR__ . '/result/suborders.json', json_encode($filtered));