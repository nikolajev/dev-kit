<?php

class _ArrayObject
{

    public array $data;

    private mixed $var = null;

    private bool $varDeclared = false;

    public function __construct(array $array)
    {
        $this->data = $array;
    }

    public function VAR(mixed &$var): self
    {
        $this->var = &$var;
        $this->varDeclared = true;

        return $this;
    }

    public function _count()
    {
        if ($this->varDeclared) {
            $this->var = count($this->data);
            $this->varDeclared = false;
            return $this;
        }

        return count($this->data);
    }
}

function Arr(array $array)
{
    return new _ArrayObject($array);
}

$Arr = Arr([1, 2, 3])
    ->VAR($test)->_count();

$Arr->data = [1];

$Arr
    ->VAR($test2)->_count();

$Arr->data = [1, 2];

var_dump($Arr->_count());

var_dump([$test, $test2]);