<?php

class _ArrayObject
{

    public array $data;

    private bool $waitForException = false;

    private string|null $exceptionMessage = null;

    public function __construct(array $array)
    {
        $this->data = $array;
    }

    public function THROW(string $exceptionMessage = null): self
    {
        $this->waitForException = true;
        $this->exceptionMessage = $exceptionMessage;

        return $this;
    }

    public function _isEmpty(bool $bool = true)
    {

        $isEmpty = $bool ? empty($this->data): !empty($this->data);

        if ($this->waitForException) {
            if($isEmpty){
                throw new Exception($this->exceptionMessage);
            }
            $this->waitForException = false;
            $this->exceptionMessage = null;
            return $this;
        }

        return $isEmpty;
    }
}

function Arr(array $array)
{
    return new _ArrayObject($array);
}

// @todo NB! __throw() naming is bad because PHPStorm won't display it
$result = Arr([])
    ->THROW("Test exception")->_isEmpty(false)
    ->_isEmpty();

var_dump($result);

$Arr = Arr([])
    ->THROW("Test exception")->_isEmpty();

