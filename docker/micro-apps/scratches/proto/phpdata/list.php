<?php

class _ArrayObject
{
    public array $data;

    public function __construct(array $array)
    {
        $this->data = $array;
    }

    // @todo Improve
    public function LIST(
        &$_1 = null, &$_2 = null, &$_3 = null, &$_4 = null, &$_5 = null,
        &$_6 = null, &$_7 = null, &$_8 = null, &$_9 = null, &$_10 = null
    ): self
    {
        $i = 0;

        foreach ([&$_1, &$_2, &$_3, &$_4, &$_5, &$_6, &$_7, &$_8, &$_9, &$_10] as &$arg) {
            if (!array_key_exists($i, $this->data)) {
                break;
            }

            $arg = $this->data[$i];
            $i++;
        }

        return $this;
    }

}

function Arr(array $array)
{
    return new _ArrayObject($array);
}

Arr([1, 2, 3])->LIST($a, $b, $c)->data;

var_dump($b);