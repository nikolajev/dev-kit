<?php
// Some browsers will not display the content if it is too short
// We use str_pad() to make the output long enough
echo str_pad("Hello World!", 4096);

// Use flush() to send the string to the browser
flush();

// Display the rest of the content three seconds later
sleep(2);
echo "<br>";
echo "Hello World!";

// @todo Does not work

/*
flush();

sleep(3);
echo "<br>";
echo "Hello World!";
*/