<?php

// Set user-defined exception handler function
set_exception_handler(function (Exception $exception) {

    var_dump($exception->getFile());
    var_dump($exception->getLine());
    var_dump($exception->getTrace());

    echo "Exception: ", $exception->getMessage() . "\n";
});

// Throw exception
throw new Exception("Uncaught exception occurred!");
