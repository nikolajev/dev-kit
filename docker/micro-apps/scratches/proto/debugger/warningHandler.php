<?php

// Set user-defined exception handler function
set_error_handler(function ( $errno, $errstr, $errfile, $errline) {
    var_dump([$errno, $errstr, $errfile, $errline]);
    echo "Warning: $errstr\n";

    $err = error_get_last();

    var_dump($err);

    var_dump(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));

    if (! is_null($err)) {
        print 'PHP Fatal Error: '. explode("in /", $err['message'])[0] . "\n";
        print 'Line: '.$err['line']. "\n";
        print 'File: '.$err['file']. "\n";
    }

}, E_ALL);

file_get_contents("missing-file.txt");

trigger_error("Warning here", E_USER_WARNING);

// warning(); // trigger_error("Warning here", E_USER_WARNING);
