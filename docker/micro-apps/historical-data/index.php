<?php

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

use HistoricalData\BinanceUrl;

define("DATA_DIR", __DIR__ . "/data/");


// https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#enum-definitions
$intervals =  [
    "1m" => 12, # 720 candles // default
    "3m" => 48,  # 960 candles
    "5m" => 72, # 864 candles
    "15m" => 240, # 960 candles
    "30m" => 480, # 960 candles
    "1h" => 960, # 960 candles
    "2h" => 1920, # 960 candles
    "4h" => 3840, # 960 candles
    "6h" => 5760, # 960 candles
    "8h" => 7680, # 960 candles
    "12h" => 11520, # 960 candles
    "1d" => 24000, # 960 candles
    "3d" => 72000, # 960 candles
    "1w" => 168000, # 960 candles
    "1M" => 168000, # ? candles // No need for more
];

echo BinanceUrl::candleSticks('BTC', 'EUR', new DateTime("- 500 day"), 7680, '8h'). PHP_EOL;

//$filename = DATA_DIR . "binance-historical-data/$fiat/$product/$dateTimeStamp.json";
