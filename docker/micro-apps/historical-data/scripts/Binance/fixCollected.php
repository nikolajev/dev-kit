<?php

require_once dirname(__DIR__, 4) . "/vendor/autoload.php";

require_once dirname(__DIR__, 3) . "/functions.php";


function validateMissingFile($missingFilePath)
{
    $dataFilePath = dirname($missingFilePath) . DIRECTORY_SEPARATOR . substr(basename($missingFilePath), strlen('missing.'));

    // @todo File::json($dataFilePath)->_Array() || File::json($dataFilePath)->__() || _json($dataFilePath)->_a()->_count()
    // @todo File::json($dataFilePath)->_String() || File::json($dataFilePath)->_()
    // @todo File::json($dataFilePath)->toArrayObject()->_get()->count() || File::json($dataFilePath)->__()->_count()
    // || ??? File::json($dataFileBasename)->_count()  // NB!!! NO! As there might be either array or string instance of data
    // @todo Data::array($array)->toJsonFile($filePath)->save(); || __($array)->_json($filePath)->save()
    // @todo ->_f() = ->backToFile(), ->_a() = ->toArrayObject(), ->_s() = ->toStringObject(), ->_json() = ->toJsonFile()
    // @todo _f() = _txt() = File::txt(), _a() = Data::array(), ->_s() = Data::string(), _json() = File::json()

    // @todo
    /*
    $count = _json($dataFilePath)
        ->_a()
        ->callback(function ($_this) use (&$count) {
            $count[] =$_this->_count();
        })
        ->walk(...)
        ->callback(function ($_this) use (&$count) {
            $count[] =$_this->_count();
        })
        ->_f()
        ->save();
        // $count is set, returns [count1, count2]
    */


    if (
        count(json_decode(file_get_contents($missingFilePath), true) ?? []) +
        count(json_decode(file_get_contents($dataFilePath), true))
        > calculateMaxCandleAmountFromJsonTitle($dataFilePath)
    ) {
        /*
        showexit(
            $dataFilePath,
            count(json_decode(file_get_contents($missingFilePath), true)),
            count(json_decode(file_get_contents($dataFilePath), true)),
            calculateMaxCandleAmountFromJsonTitle($dataFilePath)
        );
        */

        $countWrongTimestamps = 0;

        // @todo nikolajev/data-object -> philippn/data (Data:: + File:: + Files:: + '...Params' php files)
        // || philippn/data-processor, manipulator etc. || philippn/data-php || ./php-data

        // @todo $json = _json($filePath); $json->construct($filePath); // Refreshes $variable, no need to spend extra memory space


        $data = json_decode(file_get_contents($dataFilePath), true);

        Arr($data)
            ->Keys()
            ->walk(function ($value) use (&$countWrongTimestamps) {
                if ((new DateTime($value))->format("s") !== "00") {
                    $countWrongTimestamps++;
                }
            });

        if ($countWrongTimestamps !== count(json_decode(file_get_contents($missingFilePath), true))) {

            failure("Too many candles: $dataFilePath");

            list($BunchFirstDateTime, $BunchFinalDateTime) = parseStartAndEndFromFilePath($dataFilePath);

            $interval = array_reverse(explode(".", dirname($dataFilePath)))[0];

            (new \HistoricalData\Binance())->registerMissing(
                $data, $BunchFirstDateTime, $BunchFinalDateTime, $interval, $dataFilePath
            );

            (new \HistoricalData\Binance())->checkMissing($dataFilePath);

            list($__dir) = parseFilePath($dataFilePath);

            remove(dirname($__dir), basename($__dir), 'zip');

            fileZip(dirname($__dir), basename($__dir));

            // @todo checkMissing() // removeEmptyMissing()

            success("Fixed");

            $countWrongTimestamps = 0;

            $data = json_decode(file_get_contents($dataFilePath), true);

            Arr($data)
                ->Keys()
                ->walk(function ($value) use (&$countWrongTimestamps) {
                    if ((new DateTime($value))->format("s") !== "00") {
                        $countWrongTimestamps++;
                    }
                });


            $missingFileContents = file_exists($missingFilePath) ? file_get_contents($missingFilePath) : "[]";

            if ($countWrongTimestamps !== 0) {

                failure('Oh no');
                // @todo Do something
/*
                $data = json_decode(file_get_contents($dataFilePath), true);

                Arr($data)
                    ->Keys()
                    ->walk(function ($value, &$key) use (&$countWrongTimestamps) {
                        if ((new DateTime($value))->format("s") !== "00") {
                            $countWrongTimestamps++;
                        }
                    });
*/
                showexit($dataFilePath,  /*$countWrongTimestamps,*/ "2018-02-09 23:13:16");

            } elseif (count($data) === 0 && $BunchFirstDateTime->format("YmdHis") === $BunchFinalDateTime->format("YmdHis")) {

                call_user_func_array('remove', parseFilePath($dataFilePath));
                call_user_func_array('remove', parseFilePath($missingFilePath));

            } elseif (
                count(json_decode($missingFileContents, true)) + count($data) >
                calculateMaxCandleAmountFromJsonTitle($dataFilePath)
            ) {

                // @todo msg('Too many candles AGAIN!', 'Fixing failure');
                // @todo A separate YAML config 'config/debuggerMsg.yml':
                // Fixing failure: failure || Fixing failure: warning + main: warning
                // failure() exits, warning() displays, success() displays, finish() exits - finish() is green

                // @todo _f($filePath)->switchDir("results", -3, 2)

                /*
                 * $filePath = "/var/www/html/micro-apps/historical-data/data/file.json";
                 *
                 * _f($filePath)->switchDir("results", -3[default = 0], 2[default = 1])
                 * 2nd param = 0: All '/var/www/html/micro-apps/historical-data/data/' will be switched
                 * 2nd param = -1: 'data' only will be switched
                 *
                 * _f($filePath)
                 *  ->switchDir("results")
                 *  ->__get()->path() = ->_path() // 'results/file.json'
                 *  ->__get()->initPath() = ->_initPath() // '/var/www/html/micro-apps/historical-data/data/file.json'
                 *
                 * _f($filePath)
                 *  ->switchDir("my-project", -3, 2)
                 *  ->_path() // '/var/www/html/my-project/data/file.json'
                 *  ->__validate()->exists(); = ->_exists() // returns FALSE
                 *
                 * // @todo ->_throw()->exists() // throws Exception || failure() if file with new path exists
                 * // @todo ->_throw()->exists(false) // throws Exception || failure() if file with new path does not exist
                 * _f($filePath)
                 *  ->switchDir("my-project", -3, 2) // '/var/www/html/my-project/data/file.json'
                 *  ->__throw()->exists() // @todo ->_throw()->callback(function($_){ return $_->_exists()})
                 *  ->save()
                 *
                 * // @todo ->replaceInPath("micro-apps/historical-data", "my-project") // '/var/www/html/my-project/data/file.json'
                 *
                 * _f($filePath)
                 *  ->replaceInPath("micro-apps/historical-data", "my-project") // '/var/www/html/my-project/data/file.json'
                 *
                 * // @todo _a([])->toJsonFile() = _a([])->JSON() // NB!!! _a([])->_json() is confusing for naming conventions
                 *
                 * // @todo _a() -> a()
                 *
                 * // arr() = ArrayObject; str() = StringObject; file() = FileObject(), fs()::list() = FilesObject; ??? BETTER Files::list() // BETTER dir($path)->list()
                 * // f_json() = JsonFileObject; a()->f_json()->switchDir(...)->save()
                 * // s_json() = JsonStringObject
                 * // Data::dateTime() = dt()
                 * // File::dir() = dir()
                 *
                 * f_json(...)->copy($newPath) // saves actual file and returns a new instance of JsonFile
                 *
                 * f_jsonZip(...)->a()->f() // if no params, then ->backToFile(); if a new $filePath provided, then ->toJsonFile($filePath)
                 *
                 * f($filePath)
                 *  ->__throw()->isInstanceOf(f_json()->__get()->class() || f_json()->_class()) // throws if is an instance of JsonFile class
                 *  ->__throw()->isInstanceOf(f_json()->_class(), false) // throws if is NOT an instance of JsonFile class
                 *  ->__throw()->isObject(false)
                 *
                 *
                 * $a = $b = $newJsonFile = $c = null;
                 *
                 * f_json($filePath)
                 *  ->callback(function($_) use (&$a){ $a = $_->_count() })
                 *  ->debug(function($_){ return $_->_count() }, 'debug'[default = 'show']) // @todo Better use ...Params class instance
                 *  ->walk(function($value) use (&$b){ ... })
                 *  ->close() // @todo Remove instance from memory (destroy itself)
                 *
                 *  ->cloneToVariable(&$newJsonFile)
                 *  ->callback(function($_) use ($newJsonFile, &$c){ $c = $_->Diff($newJsonFile) })
                 *
                 *
                 * // dir() already declared, _dir()
                 * _dir(...)->List()
                 * _file()
                 * f_json(...)->arr() = ->toArray()
                 * f_json(...)->Arr() = ->toArrayObject()
                 *
                 * ->Arr() != ->arr()
                 *
                 * a() || A() || _arr()
                 * OR $dir = function($dirPath){ return File::dir($dirPath) }
                 *
                 * $dir($dirPath)->list()
                 *
                 * $arr(), $str(), $dir(), $file(), $json
                 *
                 * BEST SOLUTION: Arr(), Str(), File(), Dir(), Date() + fJson() + fJsonZip() + fTwig($filePath) + sTwig($tpl, $data)
                 *
                 * // NB!!! Json() is not correct. Might be both File() or Str(), use fJson() and sJson()
                 *
                 * Use constants
                 * # This is quite the same as copy, but it does not allow to rewrite a file
                 */



                failure('Too many candles AGAIN!');
                showexit(
                    $missingFilePath,
                    $countWrongTimestamps,
                    $missingFileContents,
                    count(json_decode($missingFileContents, true)),
                    count($data),
                    calculateMaxCandleAmountFromJsonTitle($dataFilePath)
                );

                // @todo $foo = $bar = $ping = $pong = null;

                showexit(
                    $missingFilePath,
                    count(json_decode(file_get_contents($missingFilePath), true)),
                    count(json_decode(file_get_contents($dataFilePath), true)),
                    calculateMaxCandleAmountFromJsonTitle($dataFilePath),
                    $countWrongTimestamps
                );


            } else {

            }


        } else {
            success('Catched');
        }
    }
}

/*showexit(
    parseFilePath("/var/www/html/micro-apps/historical-data/data/Binance/historical-data/BNB/AAVE/Binance.AAVE-BNB.15m.json.zip")
);*/

// @todo Files::list()
$zipFilePaths = \Nikolajev\Filesystem\FilesList::list(
    dirname(__DIR__, 2) . "/data/Binance/historical-data",
    (new \Nikolajev\Filesystem\FilesListParams())
        ->includedFilenamePatterns(['*.zip'])
);


foreach ($zipFilePaths as $filePath) {

    list($dirPath, $basenameWithoutExtension, $extension) = parseFilePath($filePath);

    if (!file_exists("$dirPath/$basenameWithoutExtension")) { // Package is not unzipped yet
        unzipHere($dirPath, $basenameWithoutExtension);
    }

    $missingFilePaths = \Nikolajev\Filesystem\FilesList::list(
        "$dirPath/$basenameWithoutExtension",
        (new \Nikolajev\Filesystem\FilesListParams())
            ->includedFilenamePatterns(['*/missing.*'])
    );

    if (count($missingFilePaths) > 0) {
        foreach ($missingFilePaths as $missingFilePath) {
            validateMissingFile($missingFilePath);
        }
    }

    remove($dirPath, $basenameWithoutExtension); // remove dir

    echo ".";
}

showexit(count($zipFilePaths), $zipFilePaths[1]);