<?php

require_once dirname(__DIR__, 4) . "/vendor/autoload.php";

use HistoricalData\Binance;

$Binance = new Binance();

$readCache = true;
$writeToFile = true;

// @todo DRY
if (in_array("--no-cache-reading", $argv) || in_array("--ncr", $argv)) {
    $readCache = false;
}

if (in_array("--no-cache-writing", $argv) || in_array("--ncw", $argv)) {
    $writeToFile = false;
}

// @todo A separate parameters object!
$Binance->prepareRequiredSymbols($argv[1] ?? 100, $readCache, $writeToFile);

// @todo $Binance->RequiredSymbols->_count();
// @todo + $Binance->RequiredSymbols->debugCount();
show($Binance->RequiredSymbols/*->debugSelected()*/->_get()->count());