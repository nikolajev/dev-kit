<?php

require_once dirname(__DIR__, 4) . "/vendor/autoload.php";
require_once dirname(__DIR__, 3) . "/functions.php";

use HistoricalData\Binance;

define("DATA_DIR", dirname(__DIR__, 2) . "/data/");

// @todo File::json(DATA_DIR . "Binance/required-pairs.json)->toArray();
foreach (json_decode(file_get_contents(DATA_DIR . "Binance/required-pairs.json"), true) as $symbol) {
    list($asset, $counterAsset) = explode(":", $symbol);
    (new Binance())->collectHistoricalData($asset, $counterAsset);
}




