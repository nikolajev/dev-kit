<?php

require_once dirname(__DIR__, 4) . "/vendor/autoload.php";

use HistoricalData\Binance;

$Binance = new Binance();

$readCache = true;
$writeToFile = true;

if(in_array("--no-cache-reading", $argv) || in_array("--ncr", $argv)){
    $readCache = false;
}

if(in_array("--no-cache-writing", $argv) || in_array("--ncw", $argv)){
    $writeToFile = false;
}

// @todo A separate parameters object!
$Binance->collectAvailableSymbols($readCache, $writeToFile);