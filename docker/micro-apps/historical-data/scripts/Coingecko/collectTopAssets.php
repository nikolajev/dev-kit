<?php

require_once dirname(__DIR__, 4) . "/vendor/autoload.php";

use HistoricalData\Coingecko;
use Nikolajev\DataObject\Data;

$Coingecko = new Coingecko();

$readCache = true;
$writeToFile = true;

// @todo DRY
if (in_array("--no-cache-reading", $argv) || in_array("--ncr", $argv)) {
    $readCache = false;
}

if (in_array("--no-cache-writing", $argv) || in_array("--ncw", $argv)) {
    $writeToFile = false;
}

//$counterAsset = 'eur',int $perPage = 1000, int $page = 1,

    list($script, $counterAsset, $perPage, $page) =
        array_pad(
            Data::array($argv)
                ->filter(function ($value) {
                    //show($value);
                    return substr($value, 0, 2) !== "--";
                })
                // @todo ->arrayPad(4, null) - Second param has a default value NULL
                ->return(),
            4,
            null
        );


$counterAsset = $counterAsset ?? 'eur';
$perPage = $perPage ?? 1000;
$page = $page ?? 1;

// @todo A separate parameters object!
// @todo Get rid of extra parameters: $readCache, $writeToFile
$Coingecko->collectTopAssets($counterAsset, $perPage, $page, $readCache, $writeToFile);