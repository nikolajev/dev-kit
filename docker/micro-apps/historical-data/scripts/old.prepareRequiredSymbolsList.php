<?php

require_once dirname(__DIR__, 3) . "/vendor/autoload.php";

use HistoricalData\BinanceUrl;
use Nikolajev\DataObject\Data;
use Nikolajev\DataObject\ArrayObject;

define("DATA_DIR", dirname(__DIR__) . "/data/"); // @todo bootstrap

// @todo File::jsonUrl(BinanceUrl::exchangeInfo())->toArrayObject();
$Symbols = Data::array(
    json_decode(
        file_get_contents(BinanceUrl::exchangeInfo()), true
    )
);

$counterAssets = ['EUR', 'USDT', 'BTC', 'ETH', 'BNB'];

// @todo $Symbols->_get()->byKeyOrKeys('symbols')->toJsonFile("data/available-symbols"); // @todo Impossible
// @todo $Symbols->_getArrayObject()->byKeyOrKeys('symbols')->toJsonFile("data/available-symbols"); // Required for PHPStorm's help
// @todo $Symbols->_getArrayObject() = $Symbols->_Array()
// @todo $Symbols->_Array('symbols') = $Symbols->_Array()->byKeyOrKeys('symbols') // if _Array() has a parameter, automatically means _Array()->byKeyOrKeys($parameter)
file_put_contents(
    DATA_DIR . "available-symbols.json",

    json_encode(
        $Symbols
            // @todo ->_Array('symbols')
            ->_get()->byKeyOrKeys('symbols', true)
            ->walk(function ($value) use ($counterAssets) {

                if (
                    $value['status'] !== "TRADING"
                ) {
                    return ArrayObject::WALK__UNSET;
                }

                if (
                    !in_array($value['baseAsset'], $counterAssets) && !in_array($value['quoteAsset'], $counterAssets)
                ) {
                    return ArrayObject::WALK__UNSET;
                }

                return [
                    // @todo? Just WALK__REPLACE_WITH; // Specify in bootstrap
                    //@todo Better WALK::REPLACE_WITH
                    ArrayObject::WALK__REPLACE_WITH,
                    // @todo? Data::array(...)->debugCount();
                    // @todo Better ->printCount();
                    // @todo ->get()->count() = ->_count()
                    // @todo debugSelected() -> printSelected(), debugSelector() -> printSelector()
                    // @todo Data::array($value)->leaveValuesByKeys(['symbol', 'status', 'baseAsset', 'quoteAsset']) // Better naming?
                    [
                        $value['symbol'],
                        $value['status'],
                        $value['baseAsset'],
                        $value['quoteAsset'],
                    ]
                ];
            })
            ->return(),
        JSON_PRETTY_PRINT
    )
);


// @todo
/*
File::jsonUrl(BinanceUrl::exchangeInfo())
    ->toArrayObject()
    ->_Array('symbols')
    ->walk(function ($value) use ($counterAssets) {
        // ...
    })
    ->toJsonFile("data/available-symbols");
*/


