<?php

namespace HistoricalData;

final class BinanceUrl
{
    static $apiBaseurl = "https://api.binance.com/api/";

    private static function checkVersion(string $version)
    {
        if (!in_array($version, ["v1", "v3"])) {
            throw new \Exception("Invalid Binance API version '$version'");
        }
    }

    public static function candleSticks(
        string    $asset,
        string    $counterAsset,
        \DateTime $from,
        int       $bundleLengthInHours = 12,
        string    $interval = '1m',
        string    $version = 'v3' // v1, v3
    )
    {
        self::checkVersion($version);

        $startTimestamp = DateTimeAdvanced::getTimestampInMilliseconds($from);
        $endTime = clone $from;
        $endTimestamp = DateTimeAdvanced::getTimestampInMilliseconds($endTime->modify("+ $bundleLengthInHours hours"));

        if ($startTimestamp < 0) {
            return false;
        }

        return self::$apiBaseurl . "$version/klines?" .
            "symbol={$asset}$counterAsset&interval=$interval&startTime=$startTimestamp&endTime=$endTimestamp&limit=1000"; // 1000 is maximum
    }

    public static function exchangeInfo(
        string $version = 'v3' // v1, v3
    )
    {
        self::checkVersion($version);

        return self::$apiBaseurl . "$version/exchangeInfo";
    }
}