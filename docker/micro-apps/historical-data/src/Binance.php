<?php

namespace HistoricalData;

use Nikolajev\DataObject\Data;
use Nikolajev\DataObject\ArrayObject;

// @todo _ArrayObject, otherwise it is messy in PHPStorm sometimes

final class Binance
{
    private $dataDirPath;
    private string $cacheDirPath;
    private string $cachePath_allSymbols;
    private string $cachePath_requiredSymbols;

    public ArrayObject $AllSymbols;
    public ArrayObject $RequiredSymbols;

    // NB! Also used for sorting (order is important)
    public array $counterAssets = ['EUR', 'BTC', 'ETH', 'BNB', 'USD', 'BUSD', 'USDT', 'TUSD', 'USDP', 'USDC',];


    // https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#enum-definitions
    private $intervals = [
        "1m" => 12, # 720 candles // default
        "3m" => 48, // in hours  # 960 candles
        "5m" => 72, # 864 candles
        "15m" => 240, # 960 candles
        "30m" => 480, # 960 candles
        "1h" => 960, # 960 candles
        "2h" => 1920, # 960 candles
        "4h" => 3840, # 960 candles
        "6h" => 5760, # 960 candles
        "8h" => 7680, # 960 candles
        "12h" => 11520, # 960 candles
        "1d" => 24000, # 1000 candles
        "3d" => 72000, # 1000 candles
        "1w" => 168000, # 1000 candles // Check that last candle closing time is not in future!
        "1M" => 168000, # ? candles // No need for more
    ];

    private $candleAmounts = [ // @todo maximum amount
        "1m" => 720,
        "3m" => 960,
        "5m" => 864,
        "15m" => 960,
        "30m" => 960,
        "1h" => 960,
        "2h" => 960,
        "4h" => 960,
        "6h" => 960,
        "8h" => 960,
        "12h" => 960,
        "1d" => 1000,
        "3d" => 1000,
        "1w" => 1000,
        "1M" => 1000,
    ];

    private $timestampFormat = "Ymd-Hi";

    private $glob = [];

    public function __construct()
    {
        $baseDir = dirname(__DIR__);
        $this->dataDirPath = "$baseDir/data/";
        $this->cacheDirPath = "$baseDir/tmp/cache/json/Binance/";
        // @todo $this->cachePath()->allSymbols() etc.
        $this->cachePath_allSymbols = $this->cacheDirPath . "exchangeInfo/symbols.json";
        $this->cachePath_requiredSymbols = $this->cacheDirPath . "required-symbols.json";

    }

    public function collectAvailableSymbols(
        // @todo Use a separate class for these parameters below
        bool $readCache = true,
        bool $writeCache = true,
        bool $saveToObject = true, // Always as a ready ArrayObject:  Data::array(...)
        bool $returnArrayObject = true,
    )
    {
        $symbols = null;

        if ($readCache && file_exists($this->cachePath_allSymbols)) {
            $symbols = json_decode(file_get_contents($this->cachePath_allSymbols), true);
        }

        if ($symbols === null) {
            $symbols = json_decode(file_get_contents(BinanceUrl::exchangeInfo()), true)['symbols'];
        }

        if ($writeCache) {
            file_put_contents($this->cachePath_allSymbols, json_encode($symbols, JSON_PRETTY_PRINT));
        }

        if ($saveToObject) {
            $this->AllSymbols = Data::array($symbols);
        }

        return $returnArrayObject ? Data::array($symbols) : $symbols;
    }

    public function prepareRequiredSymbols(
        int  $topLimit = 100,
        // @todo Use a separate class for these parameters below
        // @todo + Use object params for that + create Trait &/|| Interface + implements JsonCache
        // @todo Better use a parent class (extends)
        bool $readCache = true,
        bool $writeCache = true,
        bool $saveToObject = true, // Always as a ready ArrayObject:  Data::array(...)
        bool $returnArrayObject = true,
    )
    {
        $Symbols = $this->collectAvailableSymbols($readCache, $writeCache);

        $Coingecko = new Coingecko();
        $perPage = $topLimit;
        $topAssets = [];

        foreach ($this->counterAssets as $counterAsset) {
            // @todo DRY
            if (in_array($counterAsset, ['USD', 'USDT', 'USDC', 'BUSD', 'USDP', 'TUSD'])) {
                $counterAsset = 'USD';
            }

            if (!array_key_exists($counterAsset, $topAssets)) {
                $topAssets[$counterAsset] = $Coingecko->collectTopAssets(strtolower($counterAsset), $perPage)->return();
            }
        }


        $counterAssets = $this->counterAssets;

        $Symbols
            ->walk(function ($value) use ($counterAssets, $topAssets) {

                if (
                    $value['status'] !== "TRADING"
                ) {
                    return ArrayObject::WALK__UNSET;
                }

                if (
                    !in_array($value['baseAsset'], $counterAssets) && !in_array($value['quoteAsset'], $counterAssets)
                ) {
                    return ArrayObject::WALK__UNSET;
                }

                if (!in_array($value['quoteAsset'], $counterAssets)) {
                    return ArrayObject::WALK__UNSET;
                }

                foreach ($counterAssets as $counterAsset) {
                    // @todo DRY
                    if (in_array($counterAsset, ['USD', 'USDT', 'USDC', 'BUSD', 'USDP', 'TUSD'])) {
                        $counterAsset = 'USD';
                    }

                    if (
                        !in_array(strtolower($value['baseAsset']), $topAssets[$counterAsset])
                    ) {
                        return ArrayObject::WALK__UNSET;
                    }
                }

                // @todo ->Columns(['symbol', 'status', ...])
                return [
                    ArrayObject::WALK__REPLACE_WITH,
                    [
                        $value['symbol'],
                        $value['baseAsset'],
                        $value['quoteAsset'],
                    ]
                ];
            });

        // @todo Implement a better solution (_ArrayObject) for logics below

        // @todo $Symbols->sortByValues(2);
        $sorted = [];
        $prioritized = [];

        foreach ($this->counterAssets as $counterAsset) {
            $sorted[$counterAsset] = [];
        }

        $registeredSymbols = [];

        $Symbols
            ->walk(function ($value) use (&$sorted, &$registeredSymbols, &$prioritized) {
                list($symbol, $baseAsset, $quoteAsset) = $value;

                $sorted[$value[2]][] = $value;

                if ($baseAsset === 'EUR') {
                    failure($value);
                    exit;
                }

                if ($quoteAsset === 'EUR') {
                    $prioritized['EUR']['high'][] = "$baseAsset:$quoteAsset";
                    $registeredSymbols[] = $baseAsset;
                }
            });

        $prioritized['EUR']['low'] = [];
        unset($sorted['EUR']);

        foreach ($sorted as $assetTitle => $assetsList) {
            foreach ($assetsList as $assetData) {
                list($symbol, $baseAsset, $quoteAsset) = $assetData;

                if (in_array($baseAsset, $registeredSymbols)) {
                    $prioritized[$quoteAsset]['low'][] = "$baseAsset:$quoteAsset";

                } else {
                    $prioritized[$quoteAsset]['high'][] = "$baseAsset:$quoteAsset";
                }

                $registeredSymbols[] = $baseAsset;
            }

        }


        // NB!!! NO NEED! - Let them be (Later it is possible to compare which pair is more volatile)
        // @todo Remove extra PAIRS (if ETHEUR exists, no need for ETHBTC)


        $result = [];

        foreach ($prioritized as $assetTitle => $data) {
            foreach ($data['high'] ?? [] as $pair) {
                $result[] = $pair;
            }
        }

        foreach ($prioritized as $assetTitle => $data) {
            foreach ($data['low'] ?? [] as $pair) {
                $result[] = $pair;
            }
        }


        if ($writeCache) {
            // JSON_PRETTY_PRINT as an additional parameter (Might be global)
            file_put_contents($this->cachePath_requiredSymbols, json_encode($result, JSON_PRETTY_PRINT));
        }

        if ($saveToObject) {
            $this->RequiredSymbols = Data::array($result);
        }

        file_put_contents(
            $this->dataDirPath . "Binance/required-pairs.json",
            json_encode($result, JSON_PRETTY_PRINT)
        );

        return $returnArrayObject ? Data::array($result) : $result;
    }

    // @todo Move to a separate package
    private function checkAvailableSpace()
    {
        $min = 25000; // in MB, ca 25 GB

        exec("df --output=avail -m {$this->dataDirPath} | tail -1", $output);

        $availableSpaceInMb = (int)$output[0];

        if($availableSpaceInMb < $min){
            echo PHP_EOL;
            failure("Not enough disk space");
            exit;
        }
    }

    public function collectHistoricalData($asset, $counterAsset)
    {
        /*
        foreach (array_keys($this->intervals) as $interval) {
            $this->collectHistoricalData_certainInterval($asset, $counterAsset, $interval);
        }
        */

        Data::array($this->intervals)
            ->Keys()
            ->walk(function ($interval) use ($asset, $counterAsset) {
                // Check if zip exists
                $zipFilePath = $this->dataDirPath .
                    "Binance/historical-data/$counterAsset/$asset/Binance.$asset-$counterAsset.$interval.zip";

                if (file_exists($zipFilePath)) {
                    echo PHP_EOL;
                    success("$asset $counterAsset $interval", false);
                    echo "ZIP file exists";
                    return;
                }

                $this->checkAvailableSpace();
                $this->collectHistoricalData_certainInterval($asset, $counterAsset, $interval);

                echo "Packing JSON files into ZIP ...\n";

                // Create zip
                $dirPath = $this->dataDirPath . "Binance/historical-data/$counterAsset/$asset/$interval";
                exec("cd $dirPath; zip -9 -r $zipFilePath .");


                // Remove all ZIP files
                exec("rm -rf $dirPath");
            });
    }

    private function parseIntervalString($interval)
    {
        $amount = preg_replace("/[^0-9]+/", "", $interval);
        $measureUnit = preg_replace("/[^a-zA-Z]+/", "", $interval);

        switch ($measureUnit) {
            case "m":
                $measureUnit = "minutes";
                break;
            case "h":
                $measureUnit = "hours";
                break;
            case "d":
                $measureUnit = "days";
                break;
            case "w":
                $measureUnit = "weeks";
                break;
            case "M":
                $measureUnit = "months";
                break;
            default:
                failure("Unknown measure unit inside an interval string '$interval'");
                exit;
        }
        return [$amount, $measureUnit];
    }

    public function checkMissing(string $dataFilePath)
    {
        // @todo DRY
        $missingCandlesFilePath = dirname($dataFilePath) . "/missing." . basename($dataFilePath);

        $data = json_decode(file_get_contents($missingCandlesFilePath), true);

        if (empty($data)) {
            unlink($missingCandlesFilePath);
        } else {
            failure(count($data) . " candles missing");
        }
    }

    public function registerMissing(
        array     $indexedResponse,
        \DateTime $BunchFirstDateTime,
        \DateTime $BunchFinalDateTime,
        string    $interval,
        string    $filePath,
    )
    {
        // @todo DRY
        $missingCandlesFilePath = dirname($filePath) . "/missing." . basename($filePath);

        list($requestTimestamps, $actualTimestamp) = explode(".", basename($filePath));

        if ($actualTimestamp !== "json") {
            $BunchFirstDateTime = \DateTime::createFromFormat($this->timestampFormat, $actualTimestamp);
        }

        file_put_contents($missingCandlesFilePath, "[\n");

        list($amount, $measureUnit) = $this->parseIntervalString($interval);

        if (!array_key_exists($BunchFirstDateTime->format("Y-m-d H:i:s"), $indexedResponse)) {

            //failure($BunchFirstDateTime->format("Y-m-d H:i:s"));

            file_put_contents(
                $missingCandlesFilePath,
                // @todo DRY
                "\"" . $BunchFirstDateTime->format("Y-m-d H:i:s") . "\",\n",
                FILE_APPEND
            );
        }

        while ($BunchFirstDateTime < $BunchFinalDateTime) {

            $BunchFirstDateTime->modify("+ $amount $measureUnit");

            if ($BunchFirstDateTime == $BunchFinalDateTime) {
                continue;
            }


            $cloned = clone $BunchFirstDateTime;
            $cloned2 = clone $BunchFirstDateTime;

            if (
                !array_key_exists($BunchFirstDateTime->format("Y-m-d H:i:s"), $indexedResponse)
            ) {
                //failure($BunchFirstDateTime->format("Y-m-d H:i:s"));
                //showlnexit($indexedResponse);

                if(
                    $BunchFirstDateTime == $BunchFinalDateTime ||
                    $BunchFirstDateTime > $BunchFinalDateTime ||
                    $cloned->modify("+ $amount $measureUnit") > $BunchFinalDateTime ||
                    $cloned2->modify("+ $amount $measureUnit") == $BunchFinalDateTime
                ){
                    break;
                }

                file_put_contents(
                    $missingCandlesFilePath,
                    // @todo DRY
                    "\"" . $BunchFirstDateTime->format("Y-m-d H:i:s") . "\",\n",
                    FILE_APPEND
                );
            }

        }

        $contents = file_get_contents($missingCandlesFilePath);
        $contents = strlen($contents) > 5 ? substr($contents, 0, -2) . "\n]" : "[]";

        file_put_contents(
            $missingCandlesFilePath,
            $contents,
        );

        return json_decode(file_get_contents($missingCandlesFilePath), true);
    }

    private function addHistoricalDataIndexes($rawResponse)
    {
        $indexed = [];

        Data::array($rawResponse)
            // @todo Implement a ready method for this purpose!
            ->walk(function ($value) use (&$indexed) {
                list($timestamp) = $value;
                $formattedTimestamp = DateTimeAdvanced::getDateTimeFromTimestampInMilliseconds($timestamp)->format("Y-m-d H:i:s");
                $indexed[$formattedTimestamp] = $value;
            });
        // @todo File::json($filePath)->toArrayObject()->walk(...)->backToFile()->save();

        return $indexed;
    }

    private function historicalDataFileExists($asset, $counterAsset, $interval, \DateTime $BunchFirstDateTime)
    {
        $dirPath = $this->dataDirPath . "Binance/historical-data/" . "$counterAsset/$asset/$interval";

        if (!array_key_exists("$asset$counterAsset$interval", $this->glob)) {
            $this->glob["$asset$counterAsset$interval"] = array_reverse(array_filter(glob("$dirPath/*"), 'is_file'));
        }

        foreach ($this->glob["$asset$counterAsset$interval"] as $key => $file) {
            list($startTimestamp) = explode("__", basename($file));
            if ($startTimestamp === $BunchFirstDateTime->format($this->timestampFormat)) {
                unset($this->glob["$asset$counterAsset$interval"][$key]);
                return true;
            }
        }

        return false;
    }

    private function collectHistoricalData_certainInterval($asset, $counterAsset, $interval)
    {
        /*
        if($asset === 'BCH'){
            failure('BCH');exit;
        }
        */

        echo PHP_EOL;
        success("$asset $counterAsset $interval", false);

        $FinalDate = (new \DateTime())
            ->setTimezone(new \DateTimeZone('UTC'))
            ->modify('first day of this month')
            ->setTime(0, 0);


        $response = true;
        $BunchFinalDateTime = null;
        $BunchFirstDateTime = null;
        $exists = true;

        while (!empty($response)) {

            if ($response !== true) {

                $requestedStartTimestamp = $BunchFirstDateTime->format($this->timestampFormat);
                $endTimestamp = $BunchFinalDateTime->format($this->timestampFormat);
                $actualStartTimestamp = DateTimeAdvanced::getDateTimeFromTimestampInMilliseconds($response[0][0])->format($this->timestampFormat);

                if ($actualStartTimestamp === $requestedStartTimestamp) {
                    $actualStartTimestamp = null;
                }

                if ($actualStartTimestamp !== null) {
                    $actualStartTimestamp = ".$actualStartTimestamp";
                }


                // @todo File::json() that will automatically create folders that do not exist yet! Separate parameter for that
                // @todo ->save(true) // $forceCreateSubdirs
                $filePath = $this->dataDirPath . "Binance/historical-data/" .
                    "$counterAsset/$asset/$interval/{$requestedStartTimestamp}__$endTimestamp$actualStartTimestamp.json";
                exec("mkdir -p " . dirname($filePath)); // pure Linux solution

                if ($exists) {
                    echo PHP_EOL;
                    $exists = false;
                }
                echo $filePath . PHP_EOL;


                // @todo Data::array()->_get()->last() = ->_last()
                $reversedResponse = array_reverse($response);
                $latestTimestamp = $reversedResponse[0][6];

                if (DateTimeAdvanced::getDateTimeFromTimestampInMilliseconds($latestTimestamp) > $BunchFinalDateTime) {
                    unset($reversedResponse[0]);
                }
                $response = array_reverse($reversedResponse);

                if (count($response) > $this->candleAmounts[$interval]) {
                    // @todo DRY
                    failure("Too many candles");
                    showexit(
                        $this->candleAmounts[$interval],
                        $this->intervals[$interval],
                        DateTimeAdvanced::getDateTimeFromTimestampInMilliseconds($latestTimestamp) > $BunchFinalDateTime,
                        count($response),
                        $filePath
                    );
                }

                $indexedResponse = $this->addHistoricalDataIndexes($response);
                $missingCandles = $this->registerMissing($indexedResponse, $BunchFirstDateTime, $BunchFinalDateTime, $interval, $filePath);
                $this->checkMissing($filePath);


                if (
                    count($missingCandles) + count($indexedResponse) > calculateMaxCandleAmountFromJsonTitle($filePath, $interval)
                ){
                    failure('Too many candles');
                    showexit(
                        $filePath,
                        count($missingCandles),
                        count($indexedResponse),
                        calculateMaxCandleAmountFromJsonTitle($filePath, $interval)
                    );
                }

                if(count($missingCandles) > 0){

                    $diff = date_diff(
                        $BunchFinalDateTime->setTime($BunchFinalDateTime->format('H'), $BunchFinalDateTime->format('i'), 0),
                        \DateTime::createFromFormat("Ymd-Hi", ltrim($actualStartTimestamp ?? $requestedStartTimestamp, '.')),
                    );

                    // @todo DateTimeAdvanced::diffInDays || diffInMinutes || diffInHours || diffInSeconds
                    $diffInMinutes = $diff->days * 24 * 60;
                    $diffInMinutes += $diff->h * 60;
                    $diffInMinutes += $diff->i;

                    if( count($indexedResponse) + count($missingCandles) > $diffInMinutes){
                        failure("Too many candles");
                        showexit(
                            "TOTAL candles: " . count($indexedResponse) + count($missingCandles),
                            "MAX allowed candles: " . $diffInMinutes,
                            $filePath
                        );
                    }

                }


                file_put_contents(
                    $filePath,
                    json_encode(
                        $indexedResponse,
                        JSON_PRETTY_PRINT
                    )
                );
            }

            $BunchFinalDateTime = clone $FinalDate;
            $BunchFirstDateTime = clone $FinalDate;

            $hours = $this->intervals[$interval];

            $BunchFirstDateTime->modify("- $hours hours");

            $url = BinanceUrl::candleSticks(
                $asset, $counterAsset, $FinalDate->modify("- $hours hours"), $hours, $interval
            );

            if (!$url) {
                $response = false;
                continue;
            }

            $historicalDataExists = $this->historicalDataFileExists($asset, $counterAsset, $interval, $BunchFirstDateTime);

            if (!$historicalDataExists) {
                $response = json_decode(file_get_contents($url), true);
            } else {
                echo ".";
                $exists = true;
                $response = true;
            }
        }

    }
}