<?php

namespace HistoricalData;

class DateTimeAdvanced
{
    public static function getTimestampInMilliseconds(\DateTime $DateTime)
    {
        return $DateTime->getTimestamp() . substr($DateTime->format('u'), 0, 3);
    }

    public static function getDateTimeFromTimestampInMilliseconds(string $timestamp, bool $UTC = true)
    {
        $DateTime =  \DateTime::createFromFormat("Y-m-d H:i:s", date("Y-m-d H:i:s", floor($timestamp / 1000)));

        if(!$UTC){
            return $DateTime;
        }

        return $DateTime->setTimezone(new \DateTimeZone('UTC'));
    }
}