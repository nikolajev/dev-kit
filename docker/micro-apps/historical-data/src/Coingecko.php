<?php

namespace HistoricalData;

use Nikolajev\DataObject\ArrayObject;
use Nikolajev\DataObject\Data;

final class Coingecko
{
    //private $dataDir;
    private string $cacheDirPath;
    private string $cachePath_topAssets;

    public ArrayObject $TopAssets;

    public function __construct()
    {
        $baseDir = dirname(__DIR__);
        $this->cacheDirPath = "$baseDir/tmp/cache/json/Coingecko/";
        $this->cachePath_topAssets = $this->cacheDirPath . "top-assets";
    }

    public function collectTopAssets(
        string $counterAsset = 'eur', int $perPage = 1000, int $page = 1,

        // @todo Use a separate class for these parameters below
        bool   $readCache = true,
        bool   $writeCache = true,
        bool   $saveToObject = true, // Always as a ready ArrayObject:  Data::array(...)
        bool   $returnArrayObject = true,
    )
    {
        $topAssets = null;
        $cacheFilePath = $this->cachePath_topAssets . "__{$counterAsset}_per-page{$perPage}_page{$page}.json";
        $cacheFilePath_symbolsOnly = $this->cachePath_topAssets . "-symbols-only__{$counterAsset}_per-page{$perPage}_page{$page}.json";

        if ($readCache && file_exists($cacheFilePath)) {
            $topAssets = json_decode(file_get_contents($cacheFilePath), true);
        }

        if ($topAssets === null) {
            $topAssets = json_decode(file_get_contents(CoingeckoUrl::collectTopAssets($counterAsset, $perPage, $page)), true);
        }


        $TopAssetsSymbolsOnly =
            Data::array($topAssets ?? [])
                ->Column('symbol'); // @todo ->Columns() instead of ->leaveValuesByKeys()


        if ($writeCache) {
            // @todo A separate param JSON_PRETTY_PRINT
            file_put_contents($cacheFilePath, json_encode($topAssets, JSON_PRETTY_PRINT));

            // @todo $TopAssetsSymbolsOnly->toJsonString($prettyPrint = true || set from global config)
            // @todo + $TopAssetsSymbolsOnly->toJsonFile($cacheFilePath_symbolsOnly, $prettyPrint = true || set from global config)->save()
            file_put_contents($cacheFilePath_symbolsOnly, json_encode($TopAssetsSymbolsOnly->return(), JSON_PRETTY_PRINT));
        }

        if ($saveToObject) {
            $this->TopAssets = $TopAssetsSymbolsOnly; // Data::array($topAssets)
        }

        return $returnArrayObject ? $TopAssetsSymbolsOnly /* Data::array($topAssets) */ : $topAssets;
    }
}