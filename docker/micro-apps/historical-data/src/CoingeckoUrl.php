<?php

namespace HistoricalData;

final class CoingeckoUrl
{
    static $apiBaseurl = "https://api.coingecko.com/api/v3/";

    // @link https://stackoverflow.com/questions/63075151/how-can-i-get-a-list-of-the-300-first-coins-of-coingecko-api-by-marketcap
    // @link https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=3

    public static function collectTopAssets(string $counterAsset = 'eur', int $perPage = 1000, int $page = 1)
    {
        return self::$apiBaseurl . "coins/markets?vs_currency=$counterAsset&order=market_cap_desc&per_page=$perPage&page=$page";
    }

}