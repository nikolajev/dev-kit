<?php

function fileGetZippedJson($dir, $basenameWithoutZip)
{
    exec("cd $dir && unzip -q $basenameWithoutZip.zip");

    $array = json_decode(file_get_contents("$dir/$basenameWithoutZip"), true);

    exec("rm $dir/$basenameWithoutZip");

    return $array;
}

function jsonZip($array, $dir, $basenameWithoutExtension)
{
    file_put_contents("$dir/$basenameWithoutExtension.json", json_encode($array));
    exec("cd $dir; zip -9 $basenameWithoutExtension.json.zip $basenameWithoutExtension.json"); // @todo ? -9
    exec("rm $dir/$basenameWithoutExtension.json");
}

function fileZip($dir, $basenameWithoutExtension){
    exec("cd $dir; zip -9 $basenameWithoutExtension.zip $basenameWithoutExtension/"); // @todo ? -9
}

function parseFilePath($filePath)
{
    // @todo Move to File::fromTitle($filePath), this very method will be private
    $extensions = explode(",", "html,css,js,php,inc,json,yml,yaml,twig,tpl,xml,csv,zip,txt,log,sh");
    // @todo ...json.log -> 'txt type' file where every line is a separate json string
    $dirPath = dirname($filePath);
    $basename = basename($filePath);

    $extension = pathinfo($basename, PATHINFO_EXTENSION);
    $explodedExtension = [];
    $extensionIsValid = in_array($extension, $extensions);

    while ($extensionIsValid) {
        $explodedExtension[] = $extension;
        $strlenExtension = strlen($extension);
        $removedStrLen = $strlenExtension + 1; // '.'
        $extension = pathinfo(substr($basename, 0, -$removedStrLen), PATHINFO_EXTENSION);
        $extensionIsValid = in_array($extension, $extensions);
    }

    $extension = implode(".", array_reverse($explodedExtension));

    $strlenExtension = strlen($extension);
    $removedStrLen = $strlenExtension + 1; // '.'

    $basenameWithoutExtension = substr($basename, 0, -$removedStrLen);

    return [$dirPath, $basenameWithoutExtension, $extension];
}

// @link https://unix.stackexchange.com/questions/489445/unzip-to-a-folder-with-the-same-name-as-the-file-without-the-zip-extension
function unzipHere($dirPath, $basenameWithoutExtension, $createFolder = true)
{
    if ($createFolder) {
        exec("cd $dirPath && unzip -q -d $basenameWithoutExtension $basenameWithoutExtension.zip");
        return;
    }

    exec("cd $dirPath && unzip -q $basenameWithoutExtension.zip");
}

function remove($dirPath, $basenameWithoutExtension, $extension = null)
{
    $basename = $basenameWithoutExtension;
    if ($extension !== null) {
        $basename .= ".$extension";
    }

    if (is_dir("$dirPath/$basename")) {
        exec("rm -rf $dirPath/$basename");
        return;
    }

    exec("rm $dirPath/$basename");
}

// @todo function fileGetZip($dir, $basenameWithoutZip)

// @todo DateTimeAdvanced()
function getDiffInMinutes($DateStart, $DateEnd)
{
    $diff = date_diff($DateStart, $DateEnd);
    $diffInMinutes = $diff->days * 24 * 60;
    $diffInMinutes += $diff->h * 60;
    $diffInMinutes += $diff->i;

    return $diffInMinutes;
}

// @todo DateTimeAdvanced()
function getDiffInHours($DateStart, $DateEnd)
{
    $diff = date_diff($DateStart, $DateEnd);
    $diffInHours = $diff->days * 24;
    $diffInHours += $diff->h;

    return $diffInHours;
}

// @todo DateTimeAdvanced()
function getDiffInDays($DateStart, $DateEnd)
{
    $diff = date_diff($DateStart, $DateEnd);
    return $diff->days;
}

// @todo DateTimeAdvanced()
function getDiffInWeeks($DateStart, $DateEnd)
{
    return floor(getDiffInDays($DateStart, $DateEnd) / 7);
}


// @todo Might be DateTimeAdvanced or DateTimeHelper
function parseIntervalString($interval)
{
    $amount = preg_replace("/[^0-9]+/", "", $interval);
    $measureUnit = preg_replace("/[^a-zA-Z]+/", "", $interval);

    switch ($measureUnit) {
        case "m":
            $measureUnit = "minutes";
            break;
        case "h":
            $measureUnit = "hours";
            break;
        case "d":
            $measureUnit = "days";
            break;
        case "w":
            $measureUnit = "weeks";
            break;
        case "M":
            $measureUnit = "months";
            break;
        default:
            failure("Unknown measure unit inside an interval string '$interval'");
            exit;
    }
    return [$amount, $measureUnit];
}

// @todo DRY
function parseStartAndEndFromFilePath($filePath)
{
    $basename = basename($filePath);
    $dirname = dirname($filePath);

    $exploded = explode(".", $basename);
    list($requestedTimestamps, $actualStartTimestamp) = $exploded;
    list($startTimestamp, $endTimestamp) = explode("__", $requestedTimestamps);

    if ($actualStartTimestamp !== 'json') {
        $startTimestamp = $actualStartTimestamp;
    }

    return [
        DateTime::createFromFormat("Ymd-Hi", $startTimestamp),
        DateTime::createFromFormat("Ymd-Hi", $endTimestamp),
    ];
}

// @todo DRY
function calculateMaxCandleAmountFromJsonTitle($filePath, $interval = null)
{
    $basename = basename($filePath);
    $dirname = dirname($filePath);

    $interval = $interval ?? array_reverse(explode(".", $dirname))[0];

    // @todo DateTimeAdvanced::divide($FromDateTime, $ToDateTime, "15 minutes")
    // @todo _d() = Data::datetime(); _d("20220202", "Ymd")->modify()->_get()->initValue(); || ->modify()->_initValue()

    // @todo DRY
    $exploded = explode(".", $basename);
    list($requestedTimestamps, $actualStartTimestamp) = $exploded;
    list($startTimestamp, $endTimestamp) = explode("__", $requestedTimestamps);


    if ($actualStartTimestamp !== 'json') {
        $startTimestamp = $actualStartTimestamp;
    }

    list($intervalAmount, $intervalMeasureUnit) = parseIntervalString($interval);

    if ($intervalMeasureUnit === "minutes") {
        return floor(getDiffInMinutes(
                DateTime::createFromFormat("Ymd-Hi", $startTimestamp),
                DateTime::createFromFormat("Ymd-Hi", $endTimestamp)
            ) / $intervalAmount);
    } elseif ($intervalMeasureUnit === "hours") {
        return floor(getDiffInHours(
                DateTime::createFromFormat("Ymd-Hi", $startTimestamp),
                DateTime::createFromFormat("Ymd-Hi", $endTimestamp)
            ) / $intervalAmount);
    } elseif ($intervalMeasureUnit === "weeks") {
        return floor(getDiffInWeeks(
                DateTime::createFromFormat("Ymd-Hi", $startTimestamp),
                DateTime::createFromFormat("Ymd-Hi", $endTimestamp)
            ) / $intervalAmount);
    } elseif ($intervalMeasureUnit === "days") {
        return floor(getDiffInDays(
                DateTime::createFromFormat("Ymd-Hi", $startTimestamp),
                DateTime::createFromFormat("Ymd-Hi", $endTimestamp)
            ) / $intervalAmount);
    } else {
        failure($intervalMeasureUnit);
        exit;
    }
}


# PHP-DATA

function Arr(array $array){
    return \Nikolajev\DataObject\Data::array($array);
}
/*
function Date(){
    return 1;
}

function fJson(){
    return 1;
}
*/