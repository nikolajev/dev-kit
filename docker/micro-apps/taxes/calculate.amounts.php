<?php
// @todo Use bootstrap
require_once "vendor/autoload.php";

require_once __DIR__ . "/Lib/Math.php";

use Nikolajev\DataObject\Data;

// @todo Use File::json()
$Data = Data::array(json_decode(file_get_contents(__DIR__ . "/result/combined.json"), true));


/*
$assets = [];

$Data
    ->walk(function ($value) use (&$assets) {
        foreach ($value['balanceChanges'] ?? [] as $asset => $amount) {
            $assets[] = $asset;
        }

        foreach ($value['fees'] ?? [] as $asset => $amount) {
            $assets[] = $asset;
        }
    });

$assets = Data::array($assets)->unique();

show($assets->return(), $assets->_get()->count());
*/

$amounts = [];

        $Data
            ->walk(function($value) use (&$amounts){
                foreach ($value['balanceChanges'] ?? [] as $asset => $amount) {
                    if(!array_key_exists($asset, $amounts)){
                        $amounts[$asset] = $amount;
                        continue;
                    }

                    $amountBefore = $amounts[$asset];

                    //$amounts[$asset] += $amount;
                    $amounts[$asset] = Math::add($amounts[$asset], $amount);

                    if($asset !== 'EUR' && $amounts[$asset] < 0){
                        debug(
                            $amountBefore,
                            $amount,
                            "$asset: $amountBefore -> {$amounts[$asset]} " . json_encode($value),
                            Math::add($amountBefore, $amount)
                        );
                    }
                }

                foreach ($value['fees'] ?? [] as $asset => $amount) {
                    if(!array_key_exists($asset, $amounts)){
                        $amounts[$asset] = $amount;
                        continue;
                    }

                    $amountBefore = $amounts[$asset];

                    //$amounts[$asset] += $amount;
                    //$amounts[$asset] = bcadd($amounts[$asset], $amount);
                    $amounts[$asset] = Math::add($amounts[$asset], $amount);

                    if($asset !== 'EUR' && $amounts[$asset] < 0){
                        show(
                            $amountBefore,
                            $amount,
                            "$asset: $amountBefore -> {$amounts[$asset]} " . json_encode($value),
                            $amountBefore + $amount,
                            Math::add($amountBefore, $amount)
                        );
                    }
                }
            });



file_put_contents(
    __DIR__ . '/result/calculated.json',
    json_encode($amounts)
);