<?php

require_once "vendor/autoload.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Debugger\Debugger;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;


$total = 0;

// @todo Defaults to comma!
File::csvDelimiter(',');

// @todo Use Filesystem with wildcard
$roughData = Data::array()
    ->merge([
        File::csv('./data/2021.01-03.csv'),
        File::csv('./data/2021.04-06.csv'),
        File::csv('./data/2021.07-09.csv'),
        File::csv('./data/2021.10-12.csv'),
    ]);


// Group report entries by UTC_Time field + remove obsolete HEADINGS report entries
$groupedByTimestamp = $roughData->GroupByField(1)->unset('UTC_Time');


//Debugger::silent(); // Throw exceptions if required


$logEntryTypes = [];

$groupedByTimestamp
    ->walk(function (&$logEntries) use (&$logEntryTypes) {

        $logEntriesArray = Data::array($logEntries);

        /*
        $logEntries = Data::array($logEntries)
            ->walk(function (&$logEntry) use (&$logEntryTypes) {

                $logEntry = Data::array($logEntry)
                    // @todo ->unsetMultiple([[0], [1], [5]]) - Looks too complicated
                    //->unset(0)
                    //->unset(0)
                    //->unset(4)
                    ->return();
                // @todo ->unsetMultiple(0, 1, 5) - Do not use compound references
            })
            ->return();
        */

        if ($logEntriesArray->_get()->count() > 1) {

            // Report entries grouped by one timestamp must belong to one Binance account type only

            $extractedAccountTypes = $logEntriesArray->Column(0);


            if (!$extractedAccountTypes->_validate()->valuesAreSame()) {

                // The only exception are Card account transfers. Just ignored (removed)
                if ($extractedAccountTypes->_validate()->isSimilar(['Card', 'Spot'])) { // Items' order is not important
                    return ArrayObject::WALK__UNSET;
                } else {
                    if (Debugger::isSilent()) {
                        throw new Exception("Unexpected account types: " . $extractedAccountTypes->var_export());
                    } else {
                        trace(-3);
                        failure('Unexpected account types');
                        showexit($extractedAccountTypes->return());
                    }
                }

            }




            $validator = $logEntriesArray->Column(3)->unique()->_validate();

            // All possible combinations
            if (
                !$validator->isSimilar(['Launchpool Interest']) &&
                !$validator->isSimilar(['Buy', 'Fee']) &&
                !$validator->isSimilar(['Sell', 'Fee']) &&
                !$validator->isSimilar(['Transaction Related']) &&
                !$validator->isSimilar(['Buy', 'Fee', 'Transaction Related']) &&
                !$validator->isSimilar(['Buy', 'Transaction Related']) &&
                !$validator->isSimilar(['Commission History']) &&
                !$validator->isSimilar(['Commission Rebate']) &&
                !$validator->isSimilar(['transfer_in', 'transfer_out'])
            ) {
                show($logEntriesArray->Column(3)->unique()->return());
            };


            // @todo Logics here


            $logEntries = $logEntriesArray->return();

        }


        /** @var ArrayObject $firstLogEntry */
        $firstLogEntry = $logEntriesArray->_get()->first();

        $logEntryType = $firstLogEntry->_get()->byKeyOrKeys(3);


        if (in_array($logEntryType, ['Binance Card Spending', 'transfer_in', 'transfer_out', 'Withdraw'])) {
            return ArrayObject::WALK__UNSET;
        }


        if (in_array($logEntryType, ['Commission Rebate'])) {
            //show($logEntries/*, $firstLogEntry->return()*/);
        }

        $logEntryTypes[] = $logEntryType;

    });


show(
    $groupedByTimestamp->_get()->count(),
    $groupedByTimestamp->_get()->first(false),
    Data::array($logEntryTypes)->unique()->return()
);

