<?php
// @todo Use bootstrap
require_once "vendor/autoload.php";

require_once __DIR__ . "/Lib/Math.php";
require_once __DIR__ . "/Lib/BinanceHistoricalData.php";

use Nikolajev\DataObject\Data;

// @todo Use File::json()
$Data = Data::array(json_decode(file_get_contents(__DIR__ . "/result/combined.json"), true));

$historicalData = Data::array(
    json_decode(file_get_contents(__DIR__ . "/result/historical-data.json"), true)
)->return();


$calculationLog = [];

$Data
    ->walk(function ($value) use (&$calculationLog, $historicalData) {

        $assets = Data::array($value['balanceChanges'])->Keys()
            ->merge([
                Data::array($value['fees'] ?? [])->Keys()
            ])
            ->unique();

        if ($value['type'] === 'Staking') {
            return;
        }

        if ($value['type'] === 'Get for free') {
            // @todo
            return;
        }

        if (
            ($assets->_get()->count() === 2 && !$assets->_validate()->includes('EUR'))/* ||
            $assets->_get()->count() > 2*/
        ) {
            // @todo
            failure('No EUR');
            return;
            //showexit($value);
        }

        $euros = $value['balanceChanges']['EUR'];

        // Crypto bought
        if ($euros < 0) {

            $balanceChanges = Data::array($value['balanceChanges'])->unset('EUR');

            $euros = Math::add($euros, $value['fees']['EUR'] ?? 0);

            // @todo DRY
            if (!array_key_exists('fees', $value)) {
                failure('No fees');
                showexit($value);
            }

            $fees = Data::array($value['fees'])->unset('EUR');

            if ($balanceChanges->_get()->count() > 1) {
                failure("Too many assets");
                showexit($value);
            }

            $assetTitle = $balanceChanges->Keys()->_get()->first();

            if ($fees->_get()->count() > 0) {

                //$assetTitle = $balanceChanges->Keys()->_get()->first();
                $assetAmount = Math::add(
                    $balanceChanges->_get()->byKeyOrKeys($assetTitle),
                    $fees->_get()->byKeyOrKeys($assetTitle),
                );

                showexit(
                    $value, $euros, $balanceChanges->Keys()->_get()->first(), $assetAmount
                );

                // $spentInEuros = 0;

                $pricePerEur = $historicalData[$assetTitle][$value['timestamp']] ?? 0;


                if ($pricePerEur === 0) {
                    $response = (new BinanceHistoricalData())->requestPriceHistoricalData(
                        $assetTitle, new DateTime($value['timestamp']), new DateTime($value['timestamp'])
                    );

                    $pricePerEur = $response[0][1] ?? 0;
                }

                if ($pricePerEur === 0 || $pricePerEur === null) {

                    if ($assetTitle === 'BTT' && $value['timestamp'] === '2021-04-09 09:18:55') {
                        $pricePerEur = "0.007788";
                    } else {
                        // @todo
                        failure("No historical data: $assetTitle {$value['timestamp']}");
                        show($value);
                    }

                }

                $fees->unset($assetTitle);

                if ($fees->_get()->count() > 0) {
                    failure('Fees still there');
                    showexit($value);
                }
                /*
                                showexit($historicalData[$assetTitle], $value['timestamp'], $assetTitle, $value['broker']);
                                showexit($historicalData[$assetTitle][$value['timestamp']]);
                */

            }
            // @todo DRY END

            //showexit($balanceChanges->Keys()->_get()->first(), $balanceChanges->_get()->first());

            // @todo
            $assetAmount = $balanceChanges->_get()->first();

            $ongoingTotalAmount = 0;
            $ongoingTotalSpentInEur = 0;

            if (array_key_exists($assetTitle, $calculationLog)) {
                $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

                $ongoingTotalAmount = $latestLogEntry['total amount'];
                $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
            }

            $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());
            $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
            $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);


            $calculationLog[$assetTitle][] = [
                'action' => 'Buy',
                'broker' => $value['broker'],
                'timestamp' => $value['timestamp'],
                'counter asset' => 'EUR',
                'received' => $assetAmount,
                'total amount' => $totalAmount,
                'spent' => $euros,
                'spent in euros' => $euros,
                'total spent in euros' => $totalSpentInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, $totalAmount),
            ];

            //success();
            //showlnexit($calculationLog);

            // Crypto sold
        } else {
            $balanceChanges = Data::array($value['balanceChanges'])->unset('EUR');

            $euros = Math::add($euros, $value['fees']['EUR'] ?? 0);

            // @todo DRY
            if (!array_key_exists('fees', $value)) {
                failure('No fees');
                showexit($value);
            }

            $fees = Data::array($value['fees'])->unset('EUR');

            if ($balanceChanges->_get()->count() > 1) {
                failure("Too many assets");
                showexit($value);
            }

            if ($fees->_get()->count() > 0) {
                failure('Fees still there');
                showexit($value);
            }
            // @todo DRY END


            $assetTitle = $balanceChanges->Keys()->_get()->first();
            $assetAmount = $balanceChanges->_get()->first();

            $ongoingTotalAmount = 0;
            $ongoingTotalSpentInEur = 0;
            $ongoingAveragePrice = 0;

            if (array_key_exists($assetTitle, $calculationLog)) {
                $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

                $ongoingTotalAmount = $latestLogEntry['total amount'];
                $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
                $ongoingAveragePrice = $latestLogEntry['total price per eur'];
            }

            $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());
            $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
            $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);


            $calculationLog[$assetTitle][] = [
                'action' => 'Sell',
                'broker' => $value['broker'],
                'timestamp' => $value['timestamp'],
                'counter asset' => 'EUR',
                'spent' => $assetAmount,
                'total amount' => $totalAmount,
                'received' => $euros,
                'received in euros' => $euros,
                'total spent in euros' => $totalSpentInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => $ongoingAveragePrice,
            ];

            /*
            showexit(
                $value, $currentTotalAmount, $currentTotalSpentInEur, $assetTitle,
                $currentPricePerEur, $totalAmount, $totalSpentInEur,
                Data::array($calculationLog[$assetTitle])->_get()->last()->return()
            );
*/

            /*
            file_put_contents(
                __DIR__ . '/result/calculated.tmp.json',
                json_encode($calculationLog)
            );

            failure($euros);
            showexit($value);
            */
        }


        //success($euros);
        //showexit($value);
    });


file_put_contents(
    __DIR__ . '/result/calculated.json',
    json_encode($Data->return())
);