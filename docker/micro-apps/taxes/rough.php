<?php

require_once "vendor/autoload.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;


$total = 0;

// @todo Defaults to comma!
File::csvDelimiter(',');

$a = Data::array()
    ->merge([
        File::csv('./data/binance.2021.01-03.csv'),
        File::csv('./data/binance.2021.04-06.csv'),
        File::csv('./data/binance.2021.07-09.csv'),
        File::csv('./data/binance.2021.10-12.csv'),
    ])
    ->walk(function ($value, $key) use (&$total, &$i, &$i1) {
        list($userId, $utcTime, $account, $operation, $coin, $change, $remark) = $value;

        if (
            $userId === 'User_ID' ||
            $coin !== 'EUR' ||
            $change < 0 ||
            !in_array($operation, ['Sell', 'Buy', 'Transaction Related'])
        ) {
            return ArrayObject::WALK__UNSET;
        }

        $total += $change;
    });


show("BINANCE", "Total transactions: " . $a->_get()->count(), "Total fiat received: " . $total);


$total = 0;

$a = File::csv('./data/kriptomat.2021.csv')
    ->toArrayObject()
    //->slice(4)
    ->walk(function ($value) use (&$total) {
        list($timestamp, $type, $asset, $a1, $a2, $eurAmount, $a3, $address, $notes) = $value;
        if (
            $type !== 'Sell'
        ) {
            return ArrayObject::WALK__UNSET;
        }
        $total += $eurAmount;
    });

show("KRIPTOMAT", "Total transactions: " . $a->_get()->count(), "Total fiat received: " . $total);


$total = 0;

$a = File::csv('./data/coinbasepro.2021.csv')
    ->toArrayObject()
    ->walk(function ($value) use (&$total) {
        list($portfolio, $tradeId, $product, $side, $createdAt, $size, $sizeUnit, $price, $fee, $_total, $totalUnit) = $value;
        if (
            $totalUnit !== 'EUR' || $_total < 0
        ) {
            return ArrayObject::WALK__UNSET;
        }
        $total += $_total;
    });

show("Coinbase Pro", "Total transactions: " . $a->_get()->count(), "Total fiat received: " . $total);