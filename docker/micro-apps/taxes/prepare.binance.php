<?php

require_once "vendor/autoload.php";

require_once __DIR__ . "/Lib/Math.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Debugger\Debugger;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;
use Nikolajev\Filesystem\FilesList;
use Nikolajev\Filesystem\FilesListParams;


// @todo Works with full paths. Implement basenames separately!
$filePaths = FilesList::list(
    __DIR__ . '/data', // @todo Implement same path processing as in File - './data'
    (new FilesListParams())->includedFilenamePatterns(['*data/binance.*']) // @todo FilenamePatterns with basenames, FilepathPatters with full paths
);

$csvFiles = [];

Data::array($filePaths)
    ->walk(function ($value) use (&$csvFiles) {
        $csvFiles[] = File::csv($value);
    });


// @todo Throw exception if file does not exist!
$roughData = Data::array()
    ->merge(
        $csvFiles
    /*[
        File::csv('./data/binance.2021.01-03.csv'),
        File::csv('./data/binance.2021.04-06.csv'),
        File::csv('./data/binance.2021.07-09.csv'),
        File::csv('./data/binance.2021.10-12.csv'),
        File::csv('./data/binance.2022.01-03.csv'),
    ]*/
    );


// Remove duplicates + Group report entries by UTC_Time field + remove obsolete HEADINGS report entries

// NB!!! Cannot use ->unique() as it ruins 2022-01-06 13:18:47 data!!!
$groupedByTimestamp = $roughData/*->unique()*/->GroupByField(1)->unset('UTC_Time');

//Debugger::silent(); // Throw exceptions if required


$groupedByTimestamp
    // @todo Why this 'use (&$logEntryTypes)' is required?
    ->walk(function (&$logEntries, $timestampKey) use (&$logEntryTypes) {

        $logEntriesArray = Data::array($logEntries);


        # GROUPS WITH MULTIPLE REPORT ENTRIES

        if ($logEntriesArray->_get()->count() > 1) {

            // Report entries grouped by one timestamp must belong to one Binance account type only

            $extractedAccountTypes = $logEntriesArray->Column(0);

            if (!$extractedAccountTypes->_validate()->valuesAreSame()) {

                // The only exception are Card account transfers. Just ignored (removed)
                if ($extractedAccountTypes->_validate()->isSimilar(['Card', 'Spot'])) { // Items' order is not important
                    return ArrayObject::WALK__UNSET;
                } else {
                    if (Debugger::isSilent()) {
                        throw new Exception("Unexpected account types: " . $extractedAccountTypes->var_export());
                    } else {
                        trace(-3);
                        failure('Unexpected account types');
                        showexit($extractedAccountTypes->return());
                    }
                }

            }


            $Actions = $logEntriesArray->Column(3)->unique();

            $validator = $Actions->_validate();

            # 'GET FO FREE' TYPE REPORT ENTRIES

            if (
                $validator->isSimilar(['Launchpool Interest']) ||
                $validator->isSimilar(['Commission History']) ||
                $validator->isSimilar(['Commission Rebate']) ||
                $validator->isSimilar(['POS savings redemption']) ||
                $validator->isSimilar(['POS savings interest'])
            ) {
                $balanceChanges = [];

                $logEntriesArray
                    ->walk(function ($value) use (&$balanceChanges) {
                        list($userId, $utcTimestamp, $accountType, $info, $asset, $balanceDiff, $comment) = $value;

                        // @todo Use Math
                        if ($balanceDiff < 0) {
                            throw new Exception("Balance difference for 'Launchpool Interest' cannot be negative");
                        }

                        $balanceChanges[] = [
                            $asset, $balanceDiff
                        ];
                    });

                $balanceChangesSummary = Data::array($balanceChanges)
                    ->SumByReferences(1, 0, true);


                $finalReportEntries = [];

                $balanceChangesSummary
                    ->walk(function ($value, $key) use (&$finalReportEntries, $timestampKey, $Actions) {
                        $finalReportEntries[] = [
                            'type' => 'Get for free',
                            'timestamp' => $timestampKey,
                            'balanceChanges' => [
                                $key => $value,
                            ],
                            'comment' => json_encode($Actions->return()),
                        ];
                    });

                return [ArrayObject::WALK__REPLACE_WITH_MULTIPLE, $finalReportEntries];
            }


            # EXCHANGE CRYPTO

            if (
                $validator->isSimilar(['Buy', 'Fee']) ||
                $validator->isSimilar(['Sell', 'Fee']) ||
                $validator->isSimilar(['Transaction Related']) ||
                $validator->isSimilar(['Buy', 'Fee', 'Transaction Related']) ||
                $validator->isSimilar(['Buy', 'Transaction Related'])
            ) {

                /*
                // @todo showln() does not display location!
                showln(142, $logEntriesArray->return());
                */

                $usedAssetsAmount = $logEntriesArray->Column(4)->unique()->_get()->count();

                if ($usedAssetsAmount < 2 || $usedAssetsAmount > 3) {
                    if (Debugger::isSilent()) {
                        throw new Exception("Too few/many assets inside one group");
                    } else {
                        trace(-3);
                        failure('Too few/many assets inside one group');
                        showexit($logEntriesArray->return());
                    }
                }

                if ($usedAssetsAmount === 3) {
                    if (!$logEntriesArray->Column(4)->unique()->_validate()->includes('BNB')) {
                        if (Debugger::isSilent()) {
                            throw new Exception("3 asset group does not contain BNB");
                        } else {
                            trace(-3);
                            failure('3 asset group does not contain BNB');
                            showexit($logEntriesArray->return());
                        }
                    }
                }


                # Fees

                if (!$logEntriesArray->GroupByField(3)->_validate()->keyExists('Fee')) {

                    $feesSummary = Data::array([]);

                } else {

                    if (
                        $logEntriesArray->GroupByField(3)->_get()->byKeyOrKeys('Fee')->Column(4)->unique()->_get()->count() > 1 &&
                        $timestampKey !== "2021-12-28 09:38:17" && // Checked exception with BNB & PYR
                        $timestampKey !== "2022-01-06 13:05:42" && // Checked exception with BNB & MC
                        $timestampKey !== "2022-01-21 09:38:52" // Checked exception with BNB & BTC
                    ) {
                        if (Debugger::isSilent()) {
                            throw new Exception("Fee can be in one asset only");
                        } else {
                            trace(-3);
                            failure('Fee can be in one asset only');
                            showexit($logEntriesArray->return(), $timestampKey);
                        }
                    }


                    $fees = [];

                    $logEntriesArray->GroupByField(3)->_get()->byKeyOrKeys('Fee')
                        ->walk(function ($value) use (&$fees) {
                            list($userId, $utcTimestamp, $accountType, $info, $asset, $balanceDiff, $comment) = $value;

                            // @todo Use Math
                            if ($balanceDiff > 0) {
                                throw new Exception("Balance difference for 'Fees' cannot be positive");
                            }

                            $fees[] = [
                                $asset, $balanceDiff
                            ];
                        });

                    $feesSummary = Data::array($fees)
                        ->SumByReferences(1, 0);

                }


                $balanceChanges = [];

                $logEntriesArray->GroupByField(3)->unset('Fee')
                    // @todo DRY
                    ->walk(function ($reportEntries) use (&$balanceChanges) {

                        // @todo Create functionality to avoid extra nesting
                        Data::array($reportEntries)
                            ->walk(function ($value) use (&$balanceChanges) {

                                list($userId, $utcTimestamp, $accountType, $info, $asset, $balanceDiff, $comment) = $value;

                                $balanceChanges[] = [
                                    $asset, $balanceDiff
                                ];
                            });
                    });

                $balanceChangesSummary = Data::array($balanceChanges)
                    ->SumByReferences(1, 0);

/*
                // @todo ->_validate()->includes()
                if (in_array("DAR", $logEntriesArray->Column(4)->unique()->return())) {
                    showln(
                        $logEntriesArray->return(),
                        $balanceChangesSummary->return()
                    );
                    exit;
                }
*/
                return [ArrayObject::WALK__REPLACE_WITH, [
                    'type' => 'Crypto exchange',
                    'timestamp' => $timestampKey,
                    'balanceChanges' => $balanceChangesSummary->return(),
                    'fees' => $feesSummary->return(),
                    'comment' => json_encode($Actions->return()),
                ]
                ];
            }


            if (
                !$validator->isSimilar(['transfer_in', 'transfer_out']) // Processed later
            ) {
                failure();
                showexit($logEntriesArray->Column(3)->unique()->return(), $logEntriesArray->return());
            }
        }


        # GROUPS WITH SINGLE REPORT ENTRY ONLY, CARD TRANSFERS ARE THE ONLY EXCEPTION

        /** @var ArrayObject $firstLogEntry */
        $firstLogEntry = $logEntriesArray->_get()->first();
        $logEntryType = $firstLogEntry->_get()->byKeyOrKeys(3);

        // These report entries are obsolete for TAX calculation
        if (in_array($logEntryType, ['Binance Card Spending', 'transfer_in', 'transfer_out', 'Deposit', 'Withdraw'])) {
            return ArrayObject::WALK__UNSET;
        }

        # 'GET FO FREE' TYPE REPORT ENTRIES

        if (in_array($logEntryType, [
            'Distribution',
            'Cash Voucher distribution',
            'Commission History',
            'Card Cashback',
            'Launchpool Interest',
            'POS savings interest',
            'Launchpad token distribution',
        ])) {
            if ($logEntriesArray->_get()->count() !== 1) {
                throw new Exception("Invalid grouped (by timestamp) report entries amount. (Distribution)");
            }

            list($userId, $utcTimestamp, $accountType, $info, $asset, $balanceDiff, $comment) =
                $logEntriesArray->_get()->first(false);

            // @todo Use Math
            if (
                $balanceDiff < 0 &&
                $timestampKey !== "2022-01-24 05:31:14" // Checked BTT conversion to BTTC
            ) {
                throw new Exception("Balance difference for 'Get for free' cannot be negative");
            }

            return [ArrayObject::WALK__REPLACE_WITH, [
                'type' => 'Get for free',
                'timestamp' => $timestampKey,
                'balanceChanges' => [
                    $asset => $balanceDiff,
                ],
                'comment' => $logEntryType,
            ]
            ];
        }


        # 'BUY EXCHANGE' TYPE REPORT ENTRIES
        if (in_array($logEntryType, ['NFT transaction'])) {

            // @todo DRY
            list($userId, $utcTimestamp, $accountType, $info, $asset, $balanceDiff, $comment) =
                $logEntriesArray->_get()->first(false);

            // @todo Use Math
            if ($balanceDiff > 0) {
                throw new Exception("Balance difference for 'NFT Buy' cannot be positive");
            }

            return [ArrayObject::WALK__REPLACE_WITH, [
                'type' => 'NFT exchange',
                'timestamp' => $timestampKey,
                'balanceChanges' => [
                    $asset => $balanceDiff,
                ],
                'comment' => $logEntryType,
            ]
            ];
        }


        # STAKING
        if (in_array($logEntryType, [
            'POS savings purchase',
            'POS savings redemption',
            'Launchpad subscribe', // @todo Check how asset gets back (avoid errors while calculating taxes)
        ])) {

            // @todo DRY
            list($userId, $utcTimestamp, $accountType, $info, $asset, $balanceDiff, $comment) =
                $logEntriesArray->_get()->first(false);

            if ($logEntryType === 'POS savings purchase') {
                // @todo Use Math
                if ($balanceDiff > 0) {
                    throw new Exception("Balance difference for 'POS savings purchase' cannot be positive");
                }
            }

            if ($logEntryType === 'POS savings redemption') {
                // @todo Use Math
                if ($balanceDiff < 0) {
                    throw new Exception("Balance difference for 'POS savings redemption' cannot be negative");
                }
            }

            return [ArrayObject::WALK__REPLACE_WITH, [
                'type' => 'Staking',
                'timestamp' => $timestampKey,
                'balanceChanges' => [
                    $asset => $balanceDiff,
                ],
                'comment' => $logEntryType,
            ]
            ];
        }


        throw new Exception(json_encode($logEntries));
    });

$groupedByTimestamp
    ->walk(function ($value) {
        if (!Data::array($value)->_validate()->keyExists('type')) {
            throw new Exception("Unprocessed report entry");
        }
    });


show($groupedByTimestamp->_get()->count());

// @todo Use filesystem
file_put_contents(__DIR__ . '/result/binance.json', json_encode($groupedByTimestamp->reindex()->return()));


// Check amounts

$assets = ["BTC", "ETH", "XRP", "SOL", "ENJ", "DAR"];

foreach ($assets as $asset) {
    $amount = 0;

    $groupedByTimestamp
        ->walk(function ($reportEntry, $key) use ($asset, &$amount) {
            $ReportEntry = Data::array($reportEntry);

            $ReportEntry->_get()->byKeyOrKeys('balanceChanges')
                ->walk(function ($_amount, $_asset) use ($asset, &$amount) {
                    if ($asset === $_asset) {
                        $amount += $_amount;
                    }
                });

            if ($ReportEntry->_validate()->keyExists('fees')) {
                $ReportEntry->_get()->byKeyOrKeys('fees')
                    ->walk(function ($_amount, $_asset) use ($asset, &$amount) {
                        if ($asset === $_asset) {
                            $amount += $_amount;
                        }
                    });
            }
        });

    show($asset, $amount);
}
