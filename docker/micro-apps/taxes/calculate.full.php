<?php
// @todo Use bootstrap
require_once "vendor/autoload.php";

require_once __DIR__ . "/Lib/Math.php";
require_once __DIR__ . "/Lib/BinanceHistoricalData.php";

use Nikolajev\DataObject\Data;

// @todo $Data->_get('fieldTitle') = $Data->_get()->byKeyOrKeys('fieldTitle')

$staking = [];
$nftExchange = [];
$getForFree = []; // OK

$_2withoutEur = []; // OK
$_2withEur_buy_feeInEur = []; // OK
$_2withEur_sell_feeInEur = []; // OK
$_2withEur_buy_noFee = []; // OK
$_2withEur_sell_noFee = []; // OK
$_2withEur_buy_feeNotInEur = []; // OK
$_2withEur_sell_feeNotInEur = []; // OK
$_3withEur_buy_multipleFeesNotInEur = []; // OK
$_3withEur_buy_feesInBnb = []; // OK
$_3withEur_sell_multipleFeesNotInEur = []; // @todo No occurrences yet
$_3withEur_sell_feesInBnb = []; // OK
$_3withoutEur = []; // @todo

class Calc
{
    public static function _2withEur_buy_feeInEur(array $logEntry, array &$calculationLog)
    {
        if (!array_key_exists('fees', $logEntry) || count($logEntry['fees']) === 0) {
            failure('No fees');
            showexit($logEntry);
        }

        $euros = Math::add($logEntry['balanceChanges']['EUR'], $logEntry['fees']['EUR'] ?? 0);
        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees'])->unset('EUR');

        if ($fees->_get()->count() !== 0) {
            failure("Too many assets in fees");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);


        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;


        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0; // @todo

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros']; // @todo
        }

        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = Math::add($ongoingTotalAmountBought, $assetAmount);
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountFees = $ongoingTotalAmountFees;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros); // @todo


        $calculationLog[$assetTitle][] = [
            'action' => 'Buy',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            'received' => "$assetAmount $assetTitle",
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'spent' => "$euros EUR",
            'spent in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur, // @todo
            'total received in euros' => $ongoingTotalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '2withEur_buy_feeInEur',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        // @todo
        /*
                 $calculationLog[$assetTitle][] = [
            'action'  => 'Buy',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            str_pad('received', 25, ' ') => "$assetAmount $assetTitle",
            str_pad('total amount', 25, ' ') => $totalAmount,
            str_pad('total amount bought', 25, ' ') => $totalAmountBought,
            str_pad('total amount sold', 25, ' ') => $totalAmountSold,
            str_pad('spent', 25, ' ') => "$euros EUR",
            str_pad('spent in euros', 25, ' ') => $euros,
            str_pad('total balance in euros', 25, ' ') => $totalBalanceInEur,
            str_pad('total spent in euros', 25, ' ') => $totalSpentInEur, // @todo
            str_pad('total received in euros', 25, ' ') => $ongoingTotalReceivedInEur,
            str_pad('current price per eur', 25, ' ') => $currentPricePerEur,
            str_pad('total price per eur', 25, ' ') => Math::divide(-$totalSpentInEur, $totalAmountBought),
            'label' => '2withEur_buy_feeInEur',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT),
        ];
         */

        /*
        if ($logEntry['timestamp'] === "2021-03-12 08:30:32") {
            showexit(
                $logEntry,
                $ongoingTotalAmount,
                $ongoingTotalSpentInEur,
                $euros,
                $balanceChanges->Keys()->_get()->first(),
                $assetAmount//, $calculationLog
            );
        }
        */

    }

    public static function _2withEur_sell_feeInEur(array $logEntry, array &$calculationLog)
    {
        if (!array_key_exists('fees', $logEntry) || count($logEntry['fees']) === 0) {
            failure('No fees');
            showexit($logEntry);
        }

        $euros = Math::add($logEntry['balanceChanges']['EUR'], $logEntry['fees']['EUR'] ?? 0);
        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees'])->unset('EUR');

        if ($fees->_get()->count() !== 0) {
            failure("Too many assets in fees");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);


        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0; // @todo

        //$ongoingAveragePrice = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = Math::add($ongoingTotalAmountSold, $assetAmount);
        $totalAmountFees = $ongoingTotalAmountFees;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = $ongoingTotalSpentInEur;
        $totalReceivedInEur = Math::add($ongoingTotalReceivedInEur, $euros);


        $calculationLog[$assetTitle][] = [
            'action' => 'Sell',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            'spent' => "$assetAmount $assetTitle",
            'spent clean' => $assetAmount,
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'received' => "$euros EUR",
            'received in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '2withEur_sell_feeInEur',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];
    }

    public static function _2withEur_buy_noFee(array $logEntry, array &$calculationLog)
    {
        $euros = $logEntry['balanceChanges']['EUR'];

        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');
        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        if (count($logEntry['fees']) !== 0) {
            failure("Fees are not allowed");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }


        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = Math::add($ongoingTotalAmountBought, $assetAmount);
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountFees = $ongoingTotalAmountFees;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);
        $totalReceivedInEur = $ongoingTotalReceivedInEur;


        $calculationLog[$assetTitle][] = [
            'action' => 'Buy',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            'received' => "$assetAmount $assetTitle",
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'spent' => "$euros EUR",
            'spent in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '_2withEur_buy_noFee',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        //showexit($logEntry, $assetTitle, $assetAmount, $euros, $ongoingTotalAmount, $ongoingTotalSpentInEur);
    }

    public static function _2withEur_sell_noFee(array $logEntry, array &$calculationLog)
    {
        $euros = $logEntry['balanceChanges']['EUR'];

        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');
        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        if (count($logEntry['fees']) !== 0) {
            failure("Fees are not allowed");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = Math::add($ongoingTotalAmountSold, $assetAmount);
        $totalAmountFees = $ongoingTotalAmountFees;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = $ongoingTotalSpentInEur;
        $totalReceivedInEur = Math::add($ongoingTotalReceivedInEur, $euros);


        $calculationLog[$assetTitle][] = [
            'action' => 'Sell',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            'spent' => "$assetAmount $assetTitle",
            'spent clean' => $assetAmount,
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'received' => "$euros EUR",
            'received in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            // @todo Remove this check when everything is ready
            'total price per eur' => (int)Math::add($totalAmountBought, $totalAmountForFree) === 0 ? 0 :
                Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '2withEur_sell_noFee',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        //showexit($logEntry);
    }

    public static function _2withEur_buy_feeNotInEur(array $logEntry, array &$calculationLog)
    {
        $euros = $logEntry['balanceChanges']['EUR'];

        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees']);

        if ($fees->_validate()->includes('EUR')) {
            failure("Fees include EUR");
            showexit($logEntry);
        }

        if ($fees->_get()->count() !== 1) {
            failure("Too many assets in fees");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = Math::add(
            $balanceChanges->_get()->byKeyOrKeys($assetTitle),
            $fees->_get()->byKeyOrKeys($assetTitle),
        );

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = Math::add($ongoingTotalAmountBought, $assetAmount);
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountFees = $ongoingTotalAmountFees;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);
        $totalReceivedInEur = $ongoingTotalReceivedInEur;

        $calculationLog[$assetTitle][] = [
            'action' => 'Buy',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            'received' => "$assetAmount $assetTitle",
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'spent' => "$euros EUR",
            'spent in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '2withEur_buy_feeNotInEur',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        //showexit($logEntry, $assetTitle, $assetAmount, $euros, $ongoingTotalAmount, $ongoingTotalSpentInEur);
    }

    public static function _2withEur_sell_feeNotInEur(array $logEntry, array &$calculationLog)
    {
        $euros = $logEntry['balanceChanges']['EUR'];

        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees']);

        if ($fees->_validate()->includes('EUR')) {
            failure("Fees include EUR");
            showexit($logEntry);
        }

        if ($fees->_get()->count() !== 1) {
            failure("Too many assets in fees");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = Math::add(
            $balanceChanges->_get()->byKeyOrKeys($assetTitle),
            $fees->_get()->byKeyOrKeys($assetTitle),
        );


        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;


        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = Math::add($ongoingTotalAmountSold, $assetAmount);
        $totalAmountFees = $ongoingTotalAmountFees;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = $ongoingTotalSpentInEur;
        $totalReceivedInEur = Math::add($ongoingTotalReceivedInEur, $euros);


        $calculationLog[$assetTitle][] = [
            'action' => 'Sell',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            'spent' => "$assetAmount $assetTitle",
            'spent clean' => $assetAmount,
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'received' => "$euros EUR",
            'received in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => (int)Math::add($totalAmountBought, $totalAmountForFree) === 0 ? 0 :
                Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '2withEur_sell_feeNotInEur',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        //showexit($logEntry);
    }

    public static function _3withEur_buy_feesInBnb(array $logEntry, array &$calculationLog, array $historicalData)
    {
        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees']);

        if ($fees->_validate()->includes('EUR')) {
            failure("Fees include EUR");
            showexit($logEntry);
        }

        if ($fees->_get()->count() !== 1) {
            failure("Too many assets in fees");
            showexit($logEntry);
        }

        if ($fees->Keys()->_get()->first() !== 'BNB') {
            failure("Fees are not in BNB");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);

        if (!array_key_exists($logEntry['timestamp'], $historicalData['BNB'])) {
            failure('No historical data for BNB');
            showexit($logEntry);
        }

        $bnbPricePerEur = $historicalData['BNB'][$logEntry['timestamp']];

        $feeInEur = Math::multiply($fees->_get()->byKeyOrKeys('BNB'), $bnbPricePerEur);

        $euros = Math::add($logEntry['balanceChanges']['EUR'], $feeInEur);
        /*
                showexit(
                    $logEntry,
                    $fees->_get()->byKeyOrKeys('BNB'),
                    $bnbPrice,
                    $feeInEur,
                    $logEntry['balanceChanges']['EUR'],
                    $euros
                );
        */

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }


        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = Math::add($ongoingTotalAmountBought, $assetAmount);
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;


        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);
        $totalReceivedInEur = $ongoingTotalReceivedInEur;
        $totalAmountFees = $ongoingTotalAmountFees;

        $calculationLog[$assetTitle][] = [
            'action' => 'Buy',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            'received' => "$assetAmount $assetTitle",
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'spent' => "EUR: {$logEntry['balanceChanges']['EUR']}, BNB: {$fees->_get()->byKeyOrKeys('BNB')}",
            'spent in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '3withEur_buy_feesInBnb',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        # BNB


        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog['BNB'])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $totalAmount = Math::add($ongoingTotalAmount, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountFees = Math::add($ongoingTotalAmountFees, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $calculationLog['BNB'][] = [
            'action' => 'Fee',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => null,
            'spent' => $fees->_get()->byKeyOrKeys('BNB') . " BNB",
            'spent clean' => $fees->_get()->byKeyOrKeys('BNB'),
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'received' => 0,
            'received in euros' => 0,
            'total balance in euros' => $ongoingTotalBalanceInEur,
            'total spent in euros' => $ongoingTotalSpentInEur,
            'total received in euros' => $ongoingTotalReceivedInEur,
            'current price per eur' => $bnbPricePerEur,
            'total price per eur' => Math::divide(-$ongoingTotalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '3withEur_buy_feesInBnb Fee',
            'asset' => 'BNB',
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        /*showexit(
            $latestLogEntry,
            [
                'action' => 'Fee',
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'counter asset' => null,
                'spent' => $fees->_get()->byKeyOrKeys('BNB'),
                'total amount' => $totalAmount,
                'received' => 0,
                'received in euros' => 0,
                'total spent in euros' => $ongoingTotalSpentInEur,
                'current price per eur' => $bnbPrice,
                'total price per eur' => Math::divide(-$ongoingTotalSpentInEur, $totalAmount),
                'label' => '3withEur_buy_feesInBnb Fee',
                'asset' => 'BNB',
            ]
        );*/
        //showexit($logEntry, $assetTitle, $assetAmount);
    }

    public static function _3withEur_sell_feesInBnb(array $logEntry, array &$calculationLog, array $historicalData)
    {
        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees']);

        if ($fees->_validate()->includes('EUR')) {
            failure("Fees include EUR");
            showexit($logEntry);
        }

        if ($fees->_get()->count() !== 1) {
            failure("Too many assets in fees");
            showexit($logEntry);
        }

        if ($fees->Keys()->_get()->first() !== 'BNB') {
            failure("Fees are not in BNB");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);

        if (!array_key_exists($logEntry['timestamp'], $historicalData['BNB'])) {
            failure('No historical data for BNB');
            showexit($logEntry);
        }

        $bnbPricePerEur = $historicalData['BNB'][$logEntry['timestamp']];

        // @todo Implement both ->byKeyOrKeys('BNB', 'price') + ->byKeyOrKeys(['BNB', 'price'])
        // @todo + ->byKeyOrKeys(['BNB', 'price'], [2020, 'Jan', 31]) =  ->byKeyOrKeys('BNB', 'price', 2020, 'Jan', 31)
        // Use as it is most convenient. No other parameters, absolutely safe.
        $feeInEur = Math::multiply($fees->_get()->byKeyOrKeys('BNB'), $bnbPricePerEur);

        $euros = Math::add($logEntry['balanceChanges']['EUR'], $feeInEur);
        /*
                showexit(
                    $logEntry,
                    $fees->_get()->byKeyOrKeys('BNB'),
                    $bnbPrice,
                    $feeInEur,
                    $logEntry['balanceChanges']['EUR'],
                    $euros
                );
        */


        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());
        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = Math::add($ongoingTotalAmountSold, $assetAmount);
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = $ongoingTotalSpentInEur;
        $totalReceivedInEur = Math::add($ongoingTotalReceivedInEur, $euros);
        $totalAmountFees = $ongoingTotalAmountFees;

        // @todo Use OOP instead of arrays
        $calculationLog[$assetTitle][] = [
            'action' => 'Sell',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => 'EUR',
            'spent' => "$assetAmount $assetTitle, {$fees->_get()->byKeyOrKeys('BNB')} BNB",
            'spent clean' => $assetAmount,
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'received' => "$euros EUR",
            'received in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '3withEur_sell_feesInBnb', // @todo Use defined class constants
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];
        /*
                showexit($logEntry, $latestLogEntry, [
                    'action' => 'Sell',
                    'broker' => $logEntry['broker'],
                    'timestamp' => $logEntry['timestamp'],
                    'counter asset' => 'EUR',
                    'spent' => $assetAmount,
                    'total amount' => $totalAmount,
                    'received' => $euros,
                    'received in euros' => $euros,
                    'total spent in euros' => $totalSpentInEur,
                    'current price per eur' => $currentPricePerEur,
                    'total price per eur' => $ongoingAveragePrice,
                    'label' => '3withEur_sell_feesInBnb',
                    'asset' => $assetTitle,
                ]);
        */

        // Add to BNB calculations as well


# BNB


        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog['BNB'])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }


        $totalAmount = Math::add($ongoingTotalAmount, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountFees = Math::add($ongoingTotalAmountFees, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $calculationLog['BNB'][] = [
            'action' => 'Fee',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            'spent' => $fees->_get()->byKeyOrKeys('BNB') . " BNB",
            'spent clean' => $fees->_get()->byKeyOrKeys('BNB'),
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            // @todo "total amount fees in eur real" + "total amount fees in eur actual" (later)
            'received' => 0,
            'received in euros' => 0,
            'total balance in euros' => $ongoingTotalBalanceInEur,
            'total spent in euros' => $ongoingTotalSpentInEur,
            'total received in euros' => $ongoingTotalReceivedInEur,
            'current price per eur' => $bnbPricePerEur,
            'total price per eur' => Math::divide(-$ongoingTotalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '3withEur_buy_feesInBnb Fee',
            'asset' => 'BNB',
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

        //showexit($logEntry, $assetTitle, $assetAmount);
    }

    public static function _getForFree(array $logEntry, array &$calculationLog)
    {
        $balanceChanges = Data::array($logEntry['balanceChanges']);

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees'] ?? []);

        if ($fees->_get()->count() > 0) {
            failure("Fees included in 'Get for free' event");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);


        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $currentPricePerEur = null;
        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = Math::add($ongoingTotalAmountForFree, $assetAmount);
        $totalAmountFees = $ongoingTotalAmountFees;

        $totalBalanceInEur = $ongoingTotalBalanceInEur;
        $totalSpentInEur = $ongoingTotalSpentInEur;
        $totalReceivedInEur = $ongoingTotalReceivedInEur;


        $calculationLog[$assetTitle][] = [
            'action' => 'Get for free',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            'received' => "$assetAmount $assetTitle",
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'spent' => null,
            'spent in euros' => null,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => (int)Math::add($totalAmountBought, $totalAmountForFree) === 0 ? 0 :
                Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => 'getForFree',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];
    }

    public static function _2withoutEur(array $logEntry, array &$calculationLog, array $historicalData)
    {
        $balanceChanges = Data::array($logEntry['balanceChanges']);
        $fees = Data::array($logEntry['fees']);

        if ($fees->_get()->count() !== 1) {
            failure("Something wrong with fees");
            showexit($logEntry);
        }

        if ($balanceChanges->Keys()->_validate()->includes('BTC')) {
            $exchangeAsset = 'BTC';
            $exchangeAssetAmount = $balanceChanges->_get()->byKeyOrKeys($exchangeAsset);
            $balanceChanges->unset('BTC');
        } elseif ($balanceChanges->Keys()->_validate()->includes('ETH')) {
            $exchangeAsset = 'ETH';
            $exchangeAssetAmount = $balanceChanges->_get()->byKeyOrKeys($exchangeAsset);
            $balanceChanges->unset('ETH');
        } elseif ($balanceChanges->Keys()->_validate()->includes('BNB')) {
            $exchangeAsset = 'BNB';
            $exchangeAssetAmount = $balanceChanges->_get()->byKeyOrKeys($exchangeAsset);
            $balanceChanges->unset('BNB');
        } else {
            failure("Cannot find an exchange asset");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetEventType = $balanceChanges->_get()->byKeyOrKeys($assetTitle) > 0 ?
            'Buy' : 'Sell';

        $exchangeAssetEventType = $exchangeAssetAmount > 0 ?
            'Exchange receive' : 'Exchange spend';


        $feeAsset = $fees->Keys()->_get()->first();
        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);

        if ($feeAsset === $exchangeAsset) {
            $exchangeAssetAmount = Math::add($exchangeAssetAmount, $fees->_get()->byKeyOrKeys($feeAsset));
        } elseif ($feeAsset === $assetTitle) {
            $assetAmount = Math::add(
                $assetAmount,
                $fees->_get()->byKeyOrKeys($feeAsset)
            );
        }


        if (!array_key_exists($logEntry['timestamp'], $historicalData[$exchangeAsset])) {
            failure("Missing history data");
            showexit($logEntry);
        }

        $exchangeAssetPrice = $historicalData[$exchangeAsset][$logEntry['timestamp']];


        # MAIN ASSET

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;


        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }


        if ($assetEventType === 'Buy') {

            $euros = Math::multiply($exchangeAssetPrice, $exchangeAssetAmount);

            $currentPricePerEur = Math::divide(-$euros, $assetAmount);

            //showexit($exchangeAssetPrice, $exchangeAssetAmount, $euros, $currentPricePerEur, $assetAmount, $logEntry);

            $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
            $totalAmountBought = Math::add($ongoingTotalAmountBought, $assetAmount);
            $totalAmountSold = $ongoingTotalAmountSold;
            $totalAmountSpent = $ongoingTotalAmountSpent;
            $totalAmountReceived = $ongoingTotalAmountReceived;
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
            $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);
            $totalReceivedInEur = $ongoingTotalReceivedInEur;
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$assetTitle][] = [
                'action' => $assetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'received' => "$assetAmount $assetTitle",
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'spent' => "$exchangeAssetAmount $exchangeAsset",
                'spent in euros' => $euros,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '2withoutEur buy',
                'asset' => $assetTitle,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } elseif ($assetEventType === 'Sell') {

            $euros = Math::multiply($exchangeAssetPrice, $exchangeAssetAmount);

            $currentPricePerEur = Math::divide(-$euros, $assetAmount);

            //showexit($exchangeAssetPrice, $exchangeAssetAmount, $euros, $currentPricePerEur, $assetAmount, $logEntry);

            $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
            $totalAmountBought = $ongoingTotalAmountBought;
            $totalAmountSold = Math::add($ongoingTotalAmountSold, $assetAmount);
            $totalAmountSpent = $ongoingTotalAmountSpent;
            $totalAmountReceived = $ongoingTotalAmountReceived;
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
            $totalSpentInEur = $ongoingTotalSpentInEur;
            $totalReceivedInEur = Math::add($ongoingTotalReceivedInEur, $euros);
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$assetTitle][] = [
                'action' => $assetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'spent' => "$assetAmount $assetTitle",
                'spent clean' => $assetAmount,
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'received' => "$exchangeAssetAmount $exchangeAsset",
                'received in euros' => $euros,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '2withoutEur sell',
                'asset' => $assetTitle,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } else {
            failure("Unknown main asset event type");
            showexit($logEntry);
        }


        # EXCHANGE ASSET

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($exchangeAsset, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$exchangeAsset])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        if ($exchangeAssetEventType === 'Exchange receive') {

            $currentPricePerEur = null;

            $totalAmount = Math::add($ongoingTotalAmount, $exchangeAssetAmount);
            $totalAmountBought = $ongoingTotalAmountBought;
            $totalAmountSold = $ongoingTotalAmountSold;
            $totalAmountSpent = $ongoingTotalAmountSpent;
            $totalAmountReceived = Math::add($ongoingTotalAmountReceived, $exchangeAssetAmount);
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = $ongoingTotalBalanceInEur;
            $totalSpentInEur = $ongoingTotalSpentInEur;
            $totalReceivedInEur = $ongoingTotalReceivedInEur;
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$exchangeAsset][] = [
                'action' => $exchangeAssetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'received' => "$exchangeAssetAmount $exchangeAsset",
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'spent' => null,
                'spent in euros' => null,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '2withoutEur exchange receive',
                'asset' => $exchangeAsset,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } elseif ($exchangeAssetEventType === 'Exchange spend') {

            $currentPricePerEur = null;

            $totalAmount = Math::add($ongoingTotalAmount, $exchangeAssetAmount);
            $totalAmountBought = $ongoingTotalAmountBought;
            $totalAmountSold = $ongoingTotalAmountSold;
            $totalAmountSpent = Math::add($ongoingTotalAmountSpent, $exchangeAssetAmount);
            $totalAmountReceived = $ongoingTotalAmountReceived;
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = $ongoingTotalBalanceInEur;
            $totalSpentInEur = $ongoingTotalSpentInEur;
            $totalReceivedInEur = $ongoingTotalReceivedInEur;
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$exchangeAsset][] = [
                'action' => $exchangeAssetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'spent' => "$exchangeAssetAmount $exchangeAsset",
                'spent clean' => $exchangeAssetAmount,
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'received' => null,
                'received in euros' => null,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '2withoutEur exchange spend',
                'asset' => $exchangeAsset,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } else {
            failure("Unknown exchange asset event type");
            showexit($logEntry);
        }


        //show($exchangeAssetAmount, $logEntry, $assetAmount);
        //show($exchangeAsset, $assetEventType, $balanceChanges->_get()->byKeyOrKeys($exchangeAsset));
        //show($assetEventType, $logEntry, $exchangeAssetEventType);
        //showln($balanceChanges->Keys()->return(), $feeAsset);
        //showexit($logEntry['timestamp'],$balanceChanges->return(), $fees->return());
    }

    public static function _3withEur_buy_multipleFeesNotInEur(array $logEntry, array &$calculationLog, array $historicalData)
    {
        $balanceChanges = Data::array($logEntry['balanceChanges'])->unset('EUR');

        if ($balanceChanges->_get()->count() !== 1) {
            failure("Too many assets in balance changes");
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees']);

        if ($fees->_validate()->includes('EUR')) {
            failure("Fees include EUR");
            showexit($logEntry);
        }

        if ($fees->_get()->count() > 2) {
            failure("Too many assets in fees");
            showexit($logEntry);
        }

        if (!$fees->Keys()->_validate()->includes('BNB')) {
            failure("Fees are not in BNB");
            showexit($logEntry);
        }

        $assetTitle = $balanceChanges->Keys()->_get()->first();

        if (!$fees->Keys()->_validate()->includes($assetTitle)) {
            failure("Problems with fees");
            showexit($logEntry);
        }

        $assetAmount = Math::add(
            $balanceChanges->_get()->byKeyOrKeys($assetTitle),
            $fees->_get()->byKeyOrKeys($assetTitle),
        );

        $fees->unset($assetTitle);

        if (!array_key_exists($logEntry['timestamp'], $historicalData['BNB'])) {
            failure('No historical data for BNB');
            showexit($logEntry);
        }

        $bnbPricePerEur = $historicalData['BNB'][$logEntry['timestamp']];

        $feeInEur = Math::multiply($fees->_get()->byKeyOrKeys('BNB'), $bnbPricePerEur);

        $fees->unset('BNB');

        if ($fees->_get()->count() !== 0) {
            failure('Too many different fees, one of fees is not in BNB');
            showexit($logEntry);
        }

        $fees = Data::array($logEntry['fees']);

        $euros = Math::add($logEntry['balanceChanges']['EUR'], $feeInEur);

        //showexit($logEntry, $euros);


        # MAIN ASSET

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $currentPricePerEur = Math::divide(-$euros, $balanceChanges->_get()->first());

        //showexit($logEntry, $currentPricePerEur);

        $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
        $totalAmountBought = Math::add($ongoingTotalAmountBought, $assetAmount);
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;


        $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
        $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);
        $totalReceivedInEur = $ongoingTotalReceivedInEur;
        $totalAmountFees = $ongoingTotalAmountFees;

        $calculationLog[$assetTitle][] = [
            'action' => 'Buy',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            'received' => "$assetAmount $assetTitle",
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'spent' => "EUR: {$logEntry['balanceChanges']['EUR']}, BNB: {$fees->_get()->byKeyOrKeys('BNB')}",
            'spent in euros' => $euros,
            'total balance in euros' => $totalBalanceInEur,
            'total spent in euros' => $totalSpentInEur,
            'total received in euros' => $totalReceivedInEur,
            'current price per eur' => $currentPricePerEur,
            'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '3withEur_buy_multipleFeesNotInEur',
            'asset' => $assetTitle,
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT),
        ];


        # BNB FEE

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog['BNB'])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $totalAmount = Math::add($ongoingTotalAmount, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountFees = Math::add($ongoingTotalAmountFees, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $calculationLog['BNB'][] = [
            'action' => 'Fee',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => null,
            'spent' => $fees->_get()->byKeyOrKeys('BNB') . " BNB",
            'spent clean' => $fees->_get()->byKeyOrKeys('BNB'),
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'received' => 0,
            'received in euros' => 0,
            'total balance in euros' => $ongoingTotalBalanceInEur,
            'total spent in euros' => $ongoingTotalSpentInEur,
            'total received in euros' => $ongoingTotalReceivedInEur,
            'current price per eur' => $bnbPricePerEur,
            'total price per eur' => Math::divide(-$ongoingTotalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '3withEur_buy_multipleFeesNotInEur Fee',
            'asset' => 'BNB',
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];

    }

    public static function _3withEur_sell_multipleFeesNotInEur(array $logEntry, array &$calculationLog, array $historicalData)
    {
        // @todo No occurrences yet
    }

    public static function _3withoutEur(array $logEntry, array &$calculationLog, array $historicalData)
    {
        $balanceChanges = Data::array($logEntry['balanceChanges']);
        $fees = Data::array($logEntry['fees']);

        if (!$fees->Keys()->_validate()->includes('BNB')) {
            failure("Fees not in BNB");
            showexit($logEntry);
        }


        if ($fees->_get()->count() === 0 || $fees->_get()->count() > 2) {
            failure("Something wrong with fees");
            showexit($logEntry);
        }

        if ($balanceChanges->Keys()->_validate()->includes('BTC')) {
            $exchangeAsset = 'BTC';
            $exchangeAssetAmount = $balanceChanges->_get()->byKeyOrKeys($exchangeAsset);
            $balanceChanges->unset('BTC');
        } elseif ($balanceChanges->Keys()->_validate()->includes('ETH')) {
            $exchangeAsset = 'ETH';
            $exchangeAssetAmount = $balanceChanges->_get()->byKeyOrKeys($exchangeAsset);
            $balanceChanges->unset('ETH');
        } elseif ($balanceChanges->Keys()->_validate()->includes('BNB')) {
            $exchangeAsset = 'BNB';
            $exchangeAssetAmount = $balanceChanges->_get()->byKeyOrKeys($exchangeAsset);
            $balanceChanges->unset('BNB');
        } else {
            failure("Cannot find an exchange asset");
            showexit($logEntry);
        }


        $assetTitle = $balanceChanges->Keys()->_get()->first();

        $assetEventType = $balanceChanges->_get()->byKeyOrKeys($assetTitle) > 0 ?
            'Buy' : 'Sell';

        $exchangeAssetEventType = $exchangeAssetAmount > 0 ?
            'Exchange receive' : 'Exchange spend';


        $feeAsset = $fees->Keys()->_get()->first();
        $assetAmount = $balanceChanges->_get()->byKeyOrKeys($assetTitle);

        if ($feeAsset === $exchangeAsset) {
            $exchangeAssetAmount = Math::add($exchangeAssetAmount, $fees->_get()->byKeyOrKeys($feeAsset));
        } elseif ($feeAsset === $assetTitle) {
            $assetAmount = Math::add(
                $assetAmount,
                $fees->_get()->byKeyOrKeys($feeAsset)
            );
        }


        if (!array_key_exists($logEntry['timestamp'], $historicalData[$exchangeAsset])) {
            failure("Missing history data");
            showexit($logEntry);
        }

        $exchangeAssetPrice = $historicalData[$exchangeAsset][$logEntry['timestamp']];
        $bnbPricePerEur = $historicalData['BNB'][$logEntry['timestamp']];
        $feeInEur = Math::multiply($fees->_get()->byKeyOrKeys('BNB'), $bnbPricePerEur);


        # MAIN ASSET

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;


        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$assetTitle])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }


        if ($assetEventType === 'Buy') {

            $euros = Math::multiply($exchangeAssetPrice, $exchangeAssetAmount);

            $euros = Math::add($euros, $feeInEur);

            $currentPricePerEur = Math::divide(-$euros, $assetAmount);

            //showexit($exchangeAssetPrice, $exchangeAssetAmount, $euros, $currentPricePerEur, $assetAmount, $logEntry);

            $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
            $totalAmountBought = Math::add($ongoingTotalAmountBought, $assetAmount);
            $totalAmountSold = $ongoingTotalAmountSold;
            $totalAmountSpent = $ongoingTotalAmountSpent;
            $totalAmountReceived = $ongoingTotalAmountReceived;
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
            $totalSpentInEur = Math::add($ongoingTotalSpentInEur, $euros);
            $totalReceivedInEur = $ongoingTotalReceivedInEur;
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$assetTitle][] = [
                'action' => $assetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'received' => "$assetAmount $assetTitle",
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'spent' => "$exchangeAssetAmount $exchangeAsset, {$fees->_get()->byKeyOrKeys($feeAsset)} $feeAsset",
                'spent in euros' => $euros,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,  // @todo Rename field: current price per eur (fee inclusive)
                // @todo Rename field: total price per eur (fee inclusive)
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '3withoutEur buy',
                'asset' => $assetTitle,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } elseif ($assetEventType === 'Sell') {

            // @todo No occurrences yet! BNB fee not included in euros (total)
            $euros = Math::multiply($exchangeAssetPrice, $exchangeAssetAmount);

            $currentPricePerEur = Math::divide(-$euros, $assetAmount);

            //showexit($exchangeAssetPrice, $exchangeAssetAmount, $euros, $currentPricePerEur, $assetAmount, $logEntry);

            $totalAmount = Math::add($ongoingTotalAmount, $assetAmount);
            $totalAmountBought = $ongoingTotalAmountBought;
            $totalAmountSold = Math::add($ongoingTotalAmountSold, $assetAmount);
            $totalAmountSpent = $ongoingTotalAmountSpent;
            $totalAmountReceived = $ongoingTotalAmountReceived;
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = Math::add($ongoingTotalBalanceInEur, $euros);
            $totalSpentInEur = $ongoingTotalSpentInEur;
            $totalReceivedInEur = Math::add($ongoingTotalReceivedInEur, $euros);
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$assetTitle][] = [
                'action' => $assetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'spent' => "$assetAmount $assetTitle", // @todo No occurrences yet! Test later (Most probably BNB fee missing)
                'spent clean' => $assetAmount,
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'received' => "$exchangeAssetAmount $exchangeAsset",
                'received in euros' => $euros,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '3withoutEur sell', // @todo No occurrences yet! Test later
                'asset' => $assetTitle,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } else {
            failure("Unknown main asset event type");
            showexit($logEntry);
        }

        # EXCHANGE ASSET

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($exchangeAsset, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog[$exchangeAsset])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        if ($exchangeAssetEventType === 'Exchange receive') {

            $currentPricePerEur = null;

            $totalAmount = Math::add($ongoingTotalAmount, $exchangeAssetAmount);
            $totalAmountBought = $ongoingTotalAmountBought;
            $totalAmountSold = $ongoingTotalAmountSold;
            $totalAmountSpent = $ongoingTotalAmountSpent;
            $totalAmountReceived = Math::add($ongoingTotalAmountReceived, $exchangeAssetAmount);
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = $ongoingTotalBalanceInEur;
            $totalSpentInEur = $ongoingTotalSpentInEur;
            $totalReceivedInEur = $ongoingTotalReceivedInEur;
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$exchangeAsset][] = [
                'action' => $exchangeAssetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'received' => "$exchangeAssetAmount $exchangeAsset",
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'spent' => null,
                'spent in euros' => null,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '3withoutEur exchange receive',
                'asset' => $exchangeAsset,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } elseif ($exchangeAssetEventType === 'Exchange spend') {

            $currentPricePerEur = null;

            $totalAmount = Math::add($ongoingTotalAmount, $exchangeAssetAmount);
            $totalAmountBought = $ongoingTotalAmountBought;
            $totalAmountSold = $ongoingTotalAmountSold;
            $totalAmountSpent = Math::add($ongoingTotalAmountSpent, $exchangeAssetAmount);
            $totalAmountReceived = $ongoingTotalAmountReceived;
            $totalAmountForFree = $ongoingTotalAmountForFree;


            $totalBalanceInEur = $ongoingTotalBalanceInEur;
            $totalSpentInEur = $ongoingTotalSpentInEur;
            $totalReceivedInEur = $ongoingTotalReceivedInEur;
            $totalAmountFees = $ongoingTotalAmountFees;

            $calculationLog[$exchangeAsset][] = [
                'action' => $exchangeAssetEventType,
                'broker' => $logEntry['broker'],
                'timestamp' => $logEntry['timestamp'],
                'spent' => "$exchangeAssetAmount $exchangeAsset",
                'spent clean' => $exchangeAssetAmount,
                'total amount' => $totalAmount,
                'total amount bought' => $totalAmountBought,
                'total amount sold' => $totalAmountSold,
                'total amount fees' => $totalAmountFees,
                'total amount spent' => $totalAmountSpent,
                'total amount received' => $totalAmountReceived,
                'total amount for free' => $totalAmountForFree,
                'received' => null,
                'received in euros' => null,
                'total balance in euros' => $totalBalanceInEur,
                'total spent in euros' => $totalSpentInEur,
                'total received in euros' => $totalReceivedInEur,
                'current price per eur' => $currentPricePerEur,
                'total price per eur' => Math::divide(-$totalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
                'label' => '3withoutEur exchange spend',
                'asset' => $exchangeAsset,
                'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
            ];

        } else {
            failure("Unknown exchange asset event type");
            showexit($logEntry);
        }


        # BNB FEES

        $ongoingTotalAmount = 0;
        $ongoingTotalAmountBought = 0;
        $ongoingTotalAmountSold = 0;
        $ongoingTotalAmountFees = 0;
        $ongoingTotalAmountSpent = 0;
        $ongoingTotalAmountReceived = 0;
        $ongoingTotalAmountForFree = 0;

        $ongoingTotalBalanceInEur = 0;
        $ongoingTotalReceivedInEur = 0;
        $ongoingTotalSpentInEur = 0;

        if (array_key_exists($assetTitle, $calculationLog)) {
            $latestLogEntry = Data::array($calculationLog['BNB'])->_get()->last()->return();

            $ongoingTotalAmount = $latestLogEntry['total amount'];
            $ongoingTotalAmountBought = $latestLogEntry['total amount bought'];
            $ongoingTotalAmountSold = $latestLogEntry['total amount sold'];
            $ongoingTotalAmountFees = $latestLogEntry['total amount fees'];
            $ongoingTotalAmountSpent = $latestLogEntry['total amount spent'];
            $ongoingTotalAmountReceived = $latestLogEntry['total amount received'];
            $ongoingTotalAmountForFree = $latestLogEntry['total amount for free'];

            $ongoingTotalBalanceInEur = $latestLogEntry['total balance in euros'];
            $ongoingTotalReceivedInEur = $latestLogEntry['total received in euros'];
            $ongoingTotalSpentInEur = $latestLogEntry['total spent in euros'];
        }

        $totalAmount = Math::add($ongoingTotalAmount, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountBought = $ongoingTotalAmountBought;
        $totalAmountSold = $ongoingTotalAmountSold;
        $totalAmountFees = Math::add($ongoingTotalAmountFees, $fees->_get()->byKeyOrKeys('BNB'));
        $totalAmountSpent = $ongoingTotalAmountSpent;
        $totalAmountReceived = $ongoingTotalAmountReceived;
        $totalAmountForFree = $ongoingTotalAmountForFree;

        $calculationLog['BNB'][] = [
            'action' => 'Fee',
            'broker' => $logEntry['broker'],
            'timestamp' => $logEntry['timestamp'],
            //'counter asset' => null,
            'spent' => $fees->_get()->byKeyOrKeys('BNB') . " BNB",
            'spent clean' => $fees->_get()->byKeyOrKeys('BNB'),
            'total amount' => $totalAmount,
            'total amount bought' => $totalAmountBought,
            'total amount sold' => $totalAmountSold,
            'total amount fees' => $totalAmountFees,
            'total amount spent' => $totalAmountSpent,
            'total amount received' => $totalAmountReceived,
            'total amount for free' => $totalAmountForFree,
            'received' => 0,
            'received in euros' => 0,
            'total balance in euros' => $ongoingTotalBalanceInEur,
            'total spent in euros' => $ongoingTotalSpentInEur,
            'total received in euros' => $ongoingTotalReceivedInEur,
            'current price per eur' => $bnbPricePerEur,
            'total price per eur' => Math::divide(-$ongoingTotalSpentInEur, Math::add($totalAmountBought, $totalAmountForFree)),
            'label' => '3withEur Fee',
            'asset' => 'BNB',
            'logEntry' => json_encode($logEntry, JSON_HEX_QUOT/*|JSON_UNESCAPED_UNICODE*/),
        ];
    }

}


$historicalData = Data::array(
    json_decode(file_get_contents(__DIR__ . "/historical-data.json"), true)
)->return();


// @todo Use File::json()
$Data = Data::array(json_decode(file_get_contents(__DIR__ . "/result/combined.json"), true));


$calculationLog = [];

$Data
    ->walk(function ($value) use (
        &$calculationLog, $historicalData,
        &$staking, &$nftExchange, &$getForFree,
        &$_2withoutEur,
        &$_2withEur_buy_feeInEur, &$_2withEur_sell_feeInEur,
        &$_2withEur_buy_feeNotInEur, &$_2withEur_sell_feeNotInEur,
        &$_2withEur_buy_noFee, &$_2withEur_sell_noFee,
        &$_3withEur_buy_multipleFeesNotInEur, &$_3withEur_buy_feesInBnb,
        &$_3withEur_sell_multipleFeesNotInEur, &$_3withEur_sell_feesInBnb,
        &$_3withoutEur
    ) {

        $assets = Data::array($value['balanceChanges'])->Keys()
            ->merge([
                Data::array($value['fees'] ?? [])->Keys()
            ])
            ->unique();

        if ($value['type'] === 'Staking') {
            $staking[] = $value;
            return;
        }

        if ($value['type'] === 'NFT exchange') {
            // @todo Required for correct amounts
            $nftExchange[] = $value;
            return;
        }

        if ($value['type'] === 'Get for free') {
            $getForFree[] = $value;
            Calc::_getForFree($value, $calculationLog);
            // @todo
            return;
        }

        if ($assets->_get()->count() === 1) {
            failure('Too few assets');
            showexit($value);
        }


        if ($assets->_get()->count() === 2 /*&& !$assets->_validate()->includes('EUR')*/) {

            # 2 with EUR
            if ($assets->_validate()->includes('EUR')) {

                $fees = Data::array($value['fees']);

                $noFees = false;
                if ($fees->Keys()->_get()->count() === 0) {
                    $noFees = true;
                }

                // If multiple, then failure
                if ($fees->Keys()->_get()->count() > 1) {
                    failure();
                    showexit($value);
                }

                $euros = $value['balanceChanges']['EUR'];

                if ($fees->Keys()->_validate()->includes('EUR')) {

                    if ($euros < 0) {
                        $_2withEur_buy_feeInEur[] = $value;
                        Calc::_2withEur_buy_feeInEur($value, $calculationLog);
                    } else {
                        $_2withEur_sell_feeInEur[] = $value;
                        Calc::_2withEur_sell_feeInEur($value, $calculationLog);
                    }

                } else {

                    if ($euros < 0) {

                        if ($noFees) {
                            $_2withEur_buy_noFee[] = $value;
                            Calc::_2withEur_buy_noFee($value, $calculationLog);
                        } else {
                            $_2withEur_buy_feeNotInEur[] = $value;
                            Calc::_2withEur_buy_feeNotInEur($value, $calculationLog);
                        }

                    } else {

                        if ($noFees) {
                            $_2withEur_sell_noFee[] = $value;
                            Calc::_2withEur_sell_noFee($value, $calculationLog);
                        } else {
                            $_2withEur_sell_feeNotInEur[] = $value;
                            Calc::_2withEur_sell_feeNotInEur($value, $calculationLog);
                        }

                    }
                }

                return;

                # 2 without EUR
            } else if (!$assets->_validate()->includes('EUR')) {

                // @todo
                $_2withoutEur[] = $value;
                Calc::_2withoutEur($value, $calculationLog, $historicalData);
                return;

            } else {
                failure('Unexpected');
                showexit($value);
            }

            failure('Unexpected');
            return;
        }

        if ($assets->_get()->count() > 2) {

            # 3 with EUR
            if ($assets->_validate()->includes('EUR')) {

                $euros = $value['balanceChanges']['EUR'];

                // Crypto bought
                if ($euros < 0) {

                    $balanceChanges = Data::array($value['balanceChanges'])->unset('EUR');

                    $euros = Math::add($euros, $value['fees']['EUR'] ?? 0);

                    // @todo DRY
                    if (!array_key_exists('fees', $value)) {
                        failure('No fees');
                        showexit($value);
                    }

                    $fees = Data::array($value['fees'])->unset('EUR');

                    # BUY; 3 with EUR; FEES NOT IN EUR
                    if ($fees->_get()->count() > 0) {

                        # BUY; 3 with EUR; MULTIPLE FEES NOT IN EUR
                        if ($fees->Keys()->_get()->count() > 1) {
                            //failure('BUY; 3 with EUR; MULTIPLE FEES NOT IN EUR');
                            //show($value);
                            $_3withEur_buy_multipleFeesNotInEur[] = $value;
                            Calc::_3withEur_buy_multipleFeesNotInEur($value, $calculationLog, $historicalData);
                            return;
                        } # BUY; 3 with EUR; FEES IN BNB
                        elseif ($fees->Keys()->_validate()->includes('BNB')) {
                            // @todo
                            $_3withEur_buy_feesInBnb[] = $value;
                            Calc::_3withEur_buy_feesInBnb($value, $calculationLog, $historicalData);
                            return;
                        } else {
                            failure('Unexpected fees');
                            showexit($value);
                        }

                    }

                    # Crypto sold
                } else {

                    $balanceChanges = Data::array($value['balanceChanges'])->unset('EUR');

                    $euros = Math::add($euros, $value['fees']['EUR'] ?? 0);

                    // @todo DRY
                    if (!array_key_exists('fees', $value)) {
                        failure('No fees');
                        showexit($value);
                    }

                    $fees = Data::array($value['fees'])->unset('EUR');

                    if ($balanceChanges->_get()->count() > 1) {
                        failure("Too many assets");
                        showexit($value);
                    }

                    if ($fees->_get()->count() > 0) {

                        # SELL; 3 with EUR; MULTIPLE FEES NOT IN EUR
                        if ($fees->Keys()->_get()->count() > 1) {
                            // @todo
                            $_3withEur_sell_multipleFeesNotInEur[] = $value;
                            return;
                        } # SELL; 3 with EUR; FEES IN BNB
                        elseif ($fees->Keys()->_validate()->includes('BNB')) {
                            // @todo
                            $_3withEur_sell_feesInBnb[] = $value;
                            Calc::_3withEur_sell_feesInBnb($value, $calculationLog, $historicalData);
                            return;
                        } else {
                            failure('Unexpected fees');
                            showexit($value);
                        }

                    }

                }

                return; // @todo Is it required?

                # 3 without EUR
            } else if (!$assets->_validate()->includes('EUR')) {

                $_3withoutEur[] = $value;
                Calc::_3withoutEur($value, $calculationLog, $historicalData);
                return;

            } else {
                failure('Unexpected');
                showexit($value);
            }
        }
    });

show(
/*count($staking),
count($nftExchange),
count($getForFree),
count($_2withoutEur),
count($_2withEur_buy_feeInEur),
count($_2withEur_sell_feeInEur),
count($_2withEur_buy_feeNotInEur),
count($_2withEur_sell_feeNotInEur),
count($_2withEur_buy_noFee),
count($_2withEur_sell_noFee),
count($_3withEur_buy_multipleFeesNotInEur),
count($_3withEur_buy_feesInBnb),
count($_3withEur_sell_multipleFeesNotInEur),
count($_3withEur_sell_feesInBnb),
count($_3withoutEur),
'Check',*/
//count($_3withEur_sell_multipleFeesNotInEur),
    $Data->_get()->count(),
    count($staking) +
    count($nftExchange) +
    count($getForFree) +
    count($_2withoutEur) +
    count($_2withEur_buy_feeInEur) +
    count($_2withEur_sell_feeInEur) +
    count($_2withEur_buy_feeNotInEur) +
    count($_2withEur_sell_feeNotInEur) +
    count($_2withEur_buy_noFee) +
    count($_2withEur_sell_noFee) +
    count($_3withEur_buy_multipleFeesNotInEur) +
    count($_3withEur_buy_feesInBnb) +
    count($_3withEur_sell_multipleFeesNotInEur) +
    count($_3withEur_sell_feesInBnb) +
    count($_3withoutEur),
);


file_put_contents(
    __DIR__ . '/result/calculated.full.json',
    json_encode($calculationLog, JSON_PRETTY_PRINT)
);