<?php

require_once "vendor/autoload.php";

require_once __DIR__ . "/Lib/Math.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Debugger\Debugger;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;
use Nikolajev\Filesystem\FilesList;
use Nikolajev\Filesystem\FilesListParams;


// @todo Works with full paths. Implement basenames separately!
$filePaths = FilesList::list(
    __DIR__ . '/data', // @todo Implement same path processing as in File - './data'
    (new FilesListParams())->includedFilenamePatterns(['*data/binance.*']) // @todo FilenamePatterns with basenames, FilepathPatters with full paths
);

$csvFiles = [];

Data::array($filePaths)
    ->walk(function ($value) use (&$csvFiles) {
        $csvFiles[] = File::csv($value);
    });


// @todo Throw exception if file does not exist!
$roughData = Data::array()
    ->merge(
        $csvFiles
    );

// @todo Use filesystem
file_put_contents(__DIR__ . '/result/test.binance.json', json_encode($roughData->reindex()->return()));

show($roughData->_get()->count());

$total = 0;

$roughData
    ->walk(function ($value) use (&$total) {
        list($userId, $utcTimestamp, $accountType, $info, $asset, $balanceDiff, $comment) = $value;

        if ($asset !== 'BNB' || new DateTime($utcTimestamp) >= new DateTime('2022-02-21 06:25:00')) {
            return ArrayObject::WALK__UNSET;
        }

        //$total += $balanceDiff;
        $total = Math::add($total, $balanceDiff);
    });

show(
    $roughData->_get()->count(), $total
);