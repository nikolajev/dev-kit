<?php

// @todo Use bootstrap
require_once "vendor/autoload.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Debugger\Debugger;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;
use Nikolajev\Filesystem\FilesList;
use Nikolajev\Filesystem\FilesListParams;

// @todo Works with full paths. Implement basenames separately!
$filePaths = FilesList::list(
    __DIR__ . '/result', // @todo Implement same path processing as in File - './data'
    (new FilesListParams())
        ->includedFilenamePatterns(['*result/*.json']) // @todo FilenamePatterns with basenames, FilepathPatters with full paths
        ->exludedFilenamePatterns(['*result/combined.json']) // @todo Does not work!!!
);

$jsonFiles = [];

Data::array($filePaths)
    ->walk(function ($value) use (&$jsonFiles) {

        $broker = null;


        // @todo Improve
        if (strpos($value, '/binance.json') !== false) {
            $broker = 'Binance';
        } elseif (strpos($value, '/coinbasepro.json') !== false) {
            $broker = 'Coinbase Pro';
        } elseif (strpos($value, '/coinbase.json') !== false) {
            $broker = 'Coinbase';
        } elseif (strpos($value, '/kriptomat.json') !== false) {
            $broker = 'Kriptomat';
        } else {
            return;
        }

        // @todo Use File::json()
        $result =
            Data::array(json_decode(file_get_contents($value), true))
                ->walk(function (&$value) use ($broker) {
                    $value['broker'] = $broker;
                })
                ->return();

        $jsonFiles[] = $result;
    });

$result = Data::array()
    ->merge($jsonFiles)
    ->return();


// @todo Implement in ArrayObject()

usort($result, function ($a, $b) {
    if (new DateTime($a['timestamp']) == new DateTime($b['timestamp'])) return 0;
    return (new DateTime($a['timestamp']) < new DateTime($b['timestamp'])) ? -1 : 1;
});

// @todo ->sortByValue() not implemented yet

// @todo Use filesystem + Get rid of '->return()'
file_put_contents(
    __DIR__ . '/result/combined.json',
    json_encode($result)
);

show(Data::array($result)->_get()->count());