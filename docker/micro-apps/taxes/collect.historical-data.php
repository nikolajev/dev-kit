<?php
// @todo Use bootstrap
require_once "vendor/autoload.php";

require_once __DIR__ . "/Lib/Math.php";
require_once __DIR__ . "/Lib/CoinbaseHistoricalData.php";
require_once __DIR__ . "/Lib/BinanceHistoricalData.php";

use Nikolajev\DataObject\Data;
use Nikolajev\DataObject\ArrayObject;

// @todo Use File::json()
$Data = Data::array(json_decode(file_get_contents(__DIR__ . "/result/combined.json"), true));

$historicalData = [];

$Data
    ->walk(function ($value) use (&$historicalData) {

        $assets = Data::array($value['balanceChanges'])->Keys()
            ->merge([
                Data::array($value['fees'] ?? [])->Keys()
            ])
            ->unique();

        if (
            ($assets->_get()->count() === 2 && !$assets->_validate()->includes('EUR')) ||
            $assets->_get()->count() > 2
        ) {

            showln($value['broker'], $value['timestamp'], $assets->return());

            if ($value['broker'] === 'Binance') {
                foreach ($assets->return() as $asset) {

                    if ($asset === 'EUR') {
                        continue;
                    }

                    $response = (new BinanceHistoricalData())->requestPriceHistoricalData(
                        $asset, new DateTime($value['timestamp']), (new DateTime($value['timestamp']))->modify("+ 1 minute")
                    );

                    if ($response !== null) {
                        $historicalData[$asset][$value['timestamp']] =
                            Data::array($response)->Column(1)->_get()->first();
                    }

                }
            } elseif ($value['broker'] === 'Coinbase Pro') {
                foreach ($assets->return() as $asset) {

                    if ($asset === 'EUR') {
                        continue;
                    }

                    $response = (new CoinbaseHistoricalData())->requestPriceHistoricalData(
                        "$asset-EUR", new DateTime($value['timestamp'])
                    );

                    if ($response !== null) {
                        $historicalData[$asset][$value['timestamp']] = $response[3];
                    }

                }
            }

            return; // continue
        }

        return ArrayObject::WALK__UNSET;
    });

file_put_contents(
    __DIR__ . '/result/test.historical-data.json',
    json_encode($Data->return())
);

file_put_contents(
    __DIR__ . '/result/historical-data.json',
    json_encode($historicalData)
);

//show($Data->Column('broker')->unique()->return());

