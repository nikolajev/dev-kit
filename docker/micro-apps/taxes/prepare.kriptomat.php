<?php

require_once "vendor/autoload.php";

use Nikolajev\DataObject\Data;
use Nikolajev\Debugger\Debugger;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;
use Nikolajev\Filesystem\FilesList;
use Nikolajev\Filesystem\FilesListParams;

// @todo Works with full paths. Implement basenames separately!
$filePaths = FilesList::list(
    __DIR__ . '/data', // @todo Implement same path processing as in File - './data'
    (new FilesListParams())->includedFilenamePatterns(['*data/kriptomat.*']) // @todo FilenamePatterns with basenames, FilepathPatters with full paths
);

$csvFiles = [];

Data::array($filePaths)
    ->walk(function ($value) use (&$csvFiles) {
        $csvFiles[] = File::csv($value)->toArrayObject()->slice(4)->reverse();
    });

$roughData = Data::array()
    ->merge($csvFiles);

$roughData
    ->walk(function (&$logEntry) use (&$finalReportEntries) {
        list(
            $timestamp, $transactionType, $asset, $amountTransacted, $pricePerCoin, $eurAmount,
            $transactedInclusiveOfKriptomatFees, $address, $notes
            ) = $logEntry;
        /*
                if($eurAmount !== $transactedInclusiveOfKriptomatFees){
                    //show($timestamp);
                    throw new Exception("Something wrong 1");
                }
        */
        if ($amountTransacted < 0 || $eurAmount < 0) {
            throw new Exception("Something wrong 2");
        }

        if (in_array($transactionType, ['Deposit', 'Withdraw'])) {
            return; // continue
        }

        $balanceChanges = [];
        $fees = [];

        if ($transactionType === 'Buy') {
            $eurAmountExclusiveOfFees = $amountTransacted * $pricePerCoin;
            $fee = $transactedInclusiveOfKriptomatFees - $eurAmountExclusiveOfFees;

            $balanceChanges = [
                $asset => $amountTransacted,
                'EUR' => -$eurAmountExclusiveOfFees,
            ];

            $fees = [
                'EUR' => -$fee,
            ];
        } else if ($transactionType === 'Sell') {

            $fee = $eurAmount - $amountTransacted * $pricePerCoin;

            $balanceChanges = [
                $asset => -$amountTransacted,
                'EUR' => $eurAmount,
            ];

            $fees = [
                'EUR' => -$fee,
            ];
        } else {
            throw new Exception("Unpredicted transaction type: $transactionType");
        }

        $finalReportEntries[] = [
            'type' => 'Crypto exchange',
            'timestamp' => date("Y-m-d H:i:s", strtotime($timestamp)),
            'balanceChanges' => $balanceChanges,
            'fees' => $fees,
            'comment' => $notes,
        ];
    });

// @todo Use filesystem
file_put_contents(__DIR__ . '/result/kriptomat.json', json_encode($finalReportEntries));