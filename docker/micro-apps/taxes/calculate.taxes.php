<?php
// @todo Use bootstrap
require_once "vendor/autoload.php";

require_once __DIR__ . "/Lib/Math.php";

use Nikolajev\DataObject\Data;


// @todo Use File::json()
$Data = Data::array(json_decode(file_get_contents(__DIR__ . "/result/calculated.full.json"), true));

$taxes = [];

$countTotalSells = 0;
$countNoTaxesSells = 0;
$countNoTaxesSells_samePrice = 0;
$countProfitableSells = 0;

$taxesTotal = 0;
$taxesData = [];

$year = 2021;

$Data
    ->walk(function ($logEntries, $asset) use ($year, &$taxesData, &$taxes, &$countTotalSells, &$countNoTaxesSells, &$countNoTaxesSells_samePrice, &$countProfitableSells, &$taxesTotal) {
        $LogEntries = Data::array($logEntries);

        $LogEntries
            ->walk(function ($logEntry) use ($year, &$taxesData, &$countTotalSells, &$countNoTaxesSells, &$countNoTaxesSells_samePrice, &$countProfitableSells, &$taxesTotal) {

                if((int)(new DateTime($logEntry['timestamp']))->format("Y") !== $year){
                    return; // Same as 'continue' in 'foreach'
                }

                if ($logEntry['action'] === 'Sell') {

                    $countTotalSells++;

                    $compare = Math::compare($logEntry['total price per eur'], $logEntry['current price per eur']);

                    if ($compare === 1) {
                        $countNoTaxesSells++;
                    } elseif ($compare === 0) {
                        $countNoTaxesSells_samePrice++;
                    } else {
                        $spent = Math::multiply(-$logEntry['total price per eur'], $logEntry['spent clean']);
                        $received = Math::multiply(-$logEntry['current price per eur'], $logEntry['spent clean']);
                        $profit = $received - $spent;
                        $tax = Math::multiply($profit, 0.2);
                        $taxesTotal = Math::add($taxesTotal, $tax);
                        $taxesData[] = [
                            $logEntry['broker'],
                            $logEntry['asset'],
                            $spent,
                            $received
                        ];
                        $countProfitableSells++;
                    }
                }

            });
    });

show($countTotalSells, $countNoTaxesSells, $countNoTaxesSells_samePrice, $countProfitableSells, $taxesTotal);

file_put_contents(
    __DIR__ . '/result/taxes-data.json',
    json_encode($taxesData, JSON_PRETTY_PRINT)
);