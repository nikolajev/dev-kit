<?php

use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;

require_once "vendor/autoload.php";

$roughData = File::csv('./data/coinbasepro.2020-01.2022-04.csv')->toArrayObject()->slice(1);



// @todo Cannot use it, as timestamp format is: 2021-02-20T15:07:12.948Z
$groupedByTimestamp = $roughData->GroupByField(2);

/*
$groupedByTimestamp = $roughData->GroupByField(2, function($value){
    return DateTime::createFromFormat("Y-m-d H:i:s", $value)->format("Y-m-d H:i:s");
});
*/

/* NB! Causes a failure 2020-05-26T15:58:25.858Z (Multiple Portfolios in one group)
$grouped = [];

$roughData
    ->walk(function ($value) use (&$grouped) {
        $groupTitle = date("Y-m-d H:i:s", strtotime($value[2]));
        $grouped[$groupTitle][] = $value;
    });

$groupedByTimestamp = Data::array($grouped);
*/

// @todo Use filesystem
//file_put_contents(__DIR__ . '/result/test.coinbasepro.json', json_encode($groupedByTimestamp->reindex()->return()));

$groupedByTimestamp
    ->walk(function (&$logEntries, $timestampKey) {

        $logEntriesArray = Data::array($logEntries);

        # GROUPS WITH MULTIPLE REPORT ENTRIES

        if ($logEntriesArray->_get()->count() > 1) {
            $extractedAccountTypes = $logEntriesArray->Column(1);

            if ($extractedAccountTypes->_validate()->valuesAreSame()) {
                if (\Nikolajev\Debugger\Debugger::isSilent()) {
                    throw new Exception("Log entries of same type inside one certain group: " . $extractedAccountTypes->var_export());
                } else {
                    trace(-3);
                    failure('Log entries of same type inside one certain group');
                    showexit($extractedAccountTypes->return());
                }
            }

            if ($extractedAccountTypes->_validate()->isSimilar(['withdrawal', 'deposit'])) { // Items' order is not important
                return ArrayObject::WALK__UNSET;
            } else if ($extractedAccountTypes->_validate()->isSimilar(['match', 'fee'])) { // Items' order & amount are not important


                if ($logEntriesArray->Column(0)->unique()->_get()->count() !== 1) {
                    if (\Nikolajev\Debugger\Debugger::isSilent()) {
                        throw new Exception("Different portfolios in one group: " . $extractedAccountTypes->var_export());
                    } else {
                        trace(-3);
                        failure('Different portfolios in one group');
                        showexit($logEntriesArray->Column(0)->unique()->return(), $logEntriesArray->return());
                    }
                }


                $finalReportEntry = [
                    'type' => 'Crypto exchange',
                    'timestamp' => date("Y-m-d H:i:s", strtotime($timestampKey)),
                    'comment' => "Portfolio: {$logEntriesArray->Column(0)->_get()->first()}",
                ];

                $balanceChanges = [];
                $fees = [];

                $logEntriesArray
                    ->walk(function ($value) use (&$balanceChanges, &$fees) {
                        list(
                            $portfolio, $type, $time, $amount, $balance, $amountOrBalanceUnit,
                            $transferId, $tradeId, $orderId
                            ) = $value;


                        if ($type === 'fee') {

                            if (array_key_exists($amountOrBalanceUnit, $fees)) {
                                $fees[$amountOrBalanceUnit] += $amount;
                                return;
                            }

                            $fees[$amountOrBalanceUnit] = $amount;
                            return;
                        }

                        if (array_key_exists($amountOrBalanceUnit, $balanceChanges)) {
                            $balanceChanges[$amountOrBalanceUnit] += $amount;
                            return;
                        }

                        $balanceChanges[$amountOrBalanceUnit] = $amount;

                    });

                return [ArrayObject::WALK__REPLACE_WITH, array_merge($finalReportEntry, [
                    'balanceChanges' => $balanceChanges,
                    'fees' => $fees,
                ])];


            } else {
                if (\Nikolajev\Debugger\Debugger::isSilent()) {
                    throw new Exception("Cannot process this combination: " . $logEntriesArray->var_export());
                } else {
                    trace(-3);
                    failure('Cannot process this combination');
                    showexit($logEntriesArray->return());
                }
            }

            throw new Exception("Script should never get here!");
        }


        # GROUPS WITH A SINGLE LOG ENTRY INSIDE

        if (!in_array($logEntriesArray->Column(1)->_get()->first(), ['deposit', 'withdrawal'])) {
            throw new Exception("Unexpected single log entry group: " . $logEntriesArray->var_export());
        }

        return ArrayObject::WALK__UNSET;
    });


// @todo Use filesystem
file_put_contents(__DIR__ . '/result/coinbasepro.json', json_encode($groupedByTimestamp->reindex()->return()));