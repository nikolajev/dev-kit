<?php

use Nikolajev\DataObject\Data;
use Nikolajev\Filesystem\File;
use Nikolajev\DataObject\ArrayObject;

require_once "vendor/autoload.php";

// @todo Implement
//$roughData = Data::array(File::csv('./data/binance.2021.01-03.csv'));

$roughData = File::csv('./data/coinbase.2020-01.2022-04.csv')->toArrayObject()->slice(8);


$roughData
    ->walk(function (&$logEntry) use (&$finalReportEntries) {
        list(
            $timestamp, $transactionType, $asset, $quantityTransacted, $spotPriceCurrency, $spotPriceAtTransaction,
            $subtotal, $totalInclusiveOfFees, $fees, $notes
            ) = $logEntry;

        if ($transactionType === 'Coinbase Earn') {
            $finalReportEntries[] = [
                'type' => 'Get for free',
                'timestamp' => date("Y-m-d H:i:s", strtotime($timestamp)),
                'balanceChanges' => [
                    $asset => $quantityTransacted,
                ],
                //'fees' => [],
                'comment' => $notes,
            ];

            return;
        }

        $finalReportEntries[] = [
            'type' => 'Crypto exchange',
            'timestamp' => date("Y-m-d H:i:s", strtotime($timestamp)),
            'balanceChanges' => [
                $asset => $transactionType === 'Buy' ? $quantityTransacted : -$quantityTransacted,
                $spotPriceCurrency => $transactionType === 'Buy' ? -$subtotal : $subtotal,
            ],
            'fees' => [
                $spotPriceCurrency => -$fees,
            ],
            'comment' => $notes,
        ];
    });

file_put_contents(__DIR__ . '/result/coinbase.json', json_encode($finalReportEntries));