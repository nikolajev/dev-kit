<?php

class CoinbaseHistoricalData
{
    public function requestPriceHistoricalData(string $product, \DateTime $DateTime, int $counter = 0)
    {
        if($counter === 0){
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, "https://api.pro.coinbase.com/products/$product");
            curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);

            if($result === '{"message":"NotFound"}'){
                return null;
            }
        }

        if ($counter === 25) {
            return null;
        }

        (int)$hours = $DateTime->format("H");
        (int)$minutes = $DateTime->format("i");

        $start = $DateTime->setTime($hours, $minutes, 0);

        $startString = str_replace('+0000', '.000Z', $start->format(\DateTime::ISO8601));

        $end = $DateTime->setTime($hours, $minutes, 59);

        $endString = str_replace('+0000', '.000Z', $end->format(\DateTime::ISO8601));

        $url = "https://api.pro.coinbase.com/products/$product/candles?" .
            "start=$startString&end=$endString";

        echo ".";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);

        $result = json_decode($output, true);

        curl_close($curl);

        if (!isset($result[0])) {
            // @todo Should be a param (-/+)
            return $this->requestPriceHistoricalData($product, $DateTime->modify("- 1 minute"), ++$counter);
        }

        return $result[0];
    }
}