<?php

class Math
{
    public static $scale = 20;

    private static function isIntegerString(string $input)
    {
        return preg_match('/^[-+]?\d+$/', $input);
    }

    private static function prepareDotFloatingPointString(string $input)
    {
        // If multiple dots  @todo Test
        if (preg_match('/.*\..*\..*/', $input)) {
            throw new \Exception("Multiple dots in '$input'");
        }

        // String contains both comma and dot (any order)
        if (preg_match('/^(?=.*\.)(?=.*,).*$/', $input)) {
            // If dot stands before comma @todo Test
            if (preg_match('/.*[.].*[,].*/', $input)) {
                throw new \Exception(
                    "Cannot prepare a string with a dot floating point from '$input'"
                );
            }

            return str_replace(",", "", $input);
        }

        // If string does not contain any dot
        if (preg_match('/^[^.]*$/', $input)) {
            // If string has only one comma
            if (preg_match('/^[^,]*,[^,]*$/', $input)) {
                return str_replace(",", ".", $input);
            }

            return str_replace(",", "", $input);
        }


        return $input;
    }

    /** @link https://stackoverflow.com/questions/21902817/convert-float-to-plain-string-representation */
    private static function exp2int($exp)
    {
        list($mantissa, $exponent) = explode("e", strtolower($exp));
        if ($exponent == '') return $exp;
        // Cointains dot
        if (preg_match('/.*\..*/', $mantissa)) {
            list($int, $dec) = explode(".", $mantissa);
        } else {
            $int = $mantissa;
            $dec = '';
        }

        bcscale(abs($exponent - strlen($dec)));

        $result = bcmul($mantissa, bcpow("10", $exponent));

        if (preg_match('/.*\..*/', $result)) {
            $result = rtrim($result, "0");

            // String ends with dot
            if (preg_match('/.*\.$/', $result)) {
                return rtrim($result, ".");
            }
        }

        return $result;
    }

    public static function prepareFloatString($input)
    {
        if ($input === null) {
            return '0';
        }

        if (is_float($input) || is_int($input)) {
            if (preg_match('/[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)/', $input)) {
                // Required for converting exponents (Either string or float)
                return self::exp2int($input);
            }

            return (string)$input;
        }

        if (!is_string($input)) {
            throw new \Exception('Input is neither integer, float, string or null');
        }

        // Strip whitespaces (including tabs and line ends)
        $input = preg_replace('/\s+/', '', $input);

        if (self::isIntegerString($input)) {
            return ltrim($input, '+');
        }


        // If string contains either comma or dot & is not exponent
        if (preg_match('/.*[,.].*/', $input) && !preg_match('/[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)/', $input)) {
            return ltrim(self::prepareDotFloatingPointString($input), '+');
        }

        // Required for converting exponents (Either string or float)
        return self::exp2int($input);
    }

    public static function floorWithPrecision($val, $precision)
    {
        $mult = pow(10, $precision); // Can be cached in lookup table
        return floor($val * $mult) / $mult;
    }

    public static function add($a, $b): string
    {
        return bcadd(self::prepareFloatString($a), self::prepareFloatString($b), self::$scale);
    }

    public static function subtract($a, $b): string
    {
        return bcsub(self::prepareFloatString($a), self::prepareFloatString($b), self::$scale);
    }

    public static function multiply($a, $b): string
    {
        return bcmul(self::prepareFloatString($a), self::prepareFloatString($b), self::$scale);
    }

    public static function divide($a, $b): string
    {
        return bcdiv(self::prepareFloatString($a), self::prepareFloatString($b), self::$scale);
    }

    public static function compare($a, $b): int
    {
        return bccomp(self::prepareFloatString($a), self::prepareFloatString($b), self::$scale);
    }
}