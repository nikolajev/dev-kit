<?php

class BinanceHistoricalData
{
    private function getTimestampInMilliseconds(\DateTime $DateTime)
    {
        return $DateTime->getTimestamp() . substr($DateTime->format('u'), 0, 3);
    }

    public function requestPriceHistoricalData(string $product, \DateTime $from, \DateTime $to, string $fiat = 'EUR', int $counter = 0)
    {
        file_get_contents(
            "https://www.binance.com/en/trade/{$product}_EUR",
            false, stream_context_create(['http' => ['ignore_errors' => true]])
        );

        if (strpos($http_response_header[0], '404 Not Found') !== false) {
            return null;
        }


        if ($counter === 25) {
            return null;
        }

        $startTime = $this->getTimestampInMilliseconds($from);

        $endTime = $this->getTimestampInMilliseconds($to);


        $url = "https://api.binance.com/api/v1/klines?symbol={$product}$fiat&interval=1m&startTime=$startTime" .
            "&endTime=$endTime&limit=1000"; // 1000 is max allowed value (only 720 needed)

        echo ".";

        $result = json_decode(@file_get_contents($url), true);

        if (!isset($result[0])) {
            // @todo Should be a param (-/+)
            //show("Try again $counter", file_get_contents($url));
            return $this->requestPriceHistoricalData($product, $from->modify("- 1 minute"), $to, $fiat, ++$counter);
        }

        return $result;
    }

}