<?php

namespace App\Lib\Rsync;

use App\Lib\Logger\LoggerFull;

class Rsync
{
    private string $fromPath;

    private string $toPath;

    private array $exclude = [
        'vendor',
        '.git',
        '.idea'
    ];

    /**
     * @param string $fromPath
     * @param string $toPath
     */
    public function __construct(string $fromPath, string $toPath)
    {
        $this->fromPath = $fromPath;
        $this->toPath = $toPath;

        if (!file_exists($this->fromPath)) {
            // @todo $this->processError(); // global param: Exception || showexit()
            throw new \Exception("'{$this->fromPath}' does not exist");
        }

        if (!file_exists($this->toPath)) {
            throw new \Exception("'{$this->toPath}' does not exist");
        }
    }

    private function prepareExcludeString(): string
    {
        $exclude = array_map(function ($value) {
            return "--exclude $value";
        }, $this->exclude);

        return implode(" ", $exclude);
    }

    private function prepareCommand(array $additionalFlags = [])
    {
        $additionalFlags = array_map(function ($value) {
            return "--$value";
        }, $additionalFlags);

        $additionalFlagsString = implode(" ", $additionalFlags);

        return "rsync -avc --delete $additionalFlagsString '{$this->fromPath}/' '{$this->toPath}' {$this->prepareExcludeString()}";
    }

    public function sync(): bool
    {
        if ($this->areDifferent()) {
            exec($this->prepareCommand(). " 2>&1", $output);

            $output = $this->removeExtraFromOutput($output);

            (new LoggerFull())->log("RSYNC: '{$this->fromPath}/' '{$this->toPath}'\n" . implode(PHP_EOL, $output));

            return true;
        }

        return false;
    }

    // Directory lines removed + default
    private function removeExtraFromOutput(array $output)
    {
        $result = [];

        foreach ($output as $line) {
            if (str_ends_with($line, "/")) {
                continue;
            } elseif (empty($line)) {
                continue;
            } elseif ($line === "sending incremental file list") {
                continue;
            } elseif (str_starts_with($line, "sent ") && str_ends_with($line, "sec")) {
                continue;
            } elseif (str_starts_with($line, "total size is ")) {
                continue;
            }

            $result[] = $line;
        }

        return $result;
    }

    public function areDifferent(): bool
    {
        exec($this->prepareCommand(['dry-run']) . " 2>&1", $output);

        $output = $this->removeExtraFromOutput($output);

        return count($output) > 0;
    }
}