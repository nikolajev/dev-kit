<?php

namespace App\Lib\ReposMap;

class ReposMapPhp7
{
    private $location;

    private $full;

    private $preparedData;


    use ReposMapTrait;

    private function str_contains(string $haystack, string $needle)
    {
        return strpos($haystack, $needle) !== false;
    }

    private function str_starts_with(?string $haystack, string $needle)
    {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }
}