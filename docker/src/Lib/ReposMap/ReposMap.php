<?php

namespace App\Lib\ReposMap;

class ReposMap
{
    private string $location;

    private array $full;

    private array $preparedData;

    use ReposMapTrait;

    private function str_contains(string $haystack, string $needle): bool
    {
        return str_contains($haystack, $needle);
    }

    private function str_starts_with(?string $haystack, string $needle): bool
    {
        return str_starts_with($haystack, $needle);
    }
}