<?php

namespace App\Lib\ReposMap;

use App\Lib\Composer\ComposerFile;
use App\Lib\Composer\ComposerLockFile;
use Nikolajev\Filesystem\File;

trait ReposMapTrait
{
    public function __construct()
    {
        // @todo Use File::json() later
        $this->location = dirname(__DIR__, 3) . "/repos.map.json";
        $this->full = (array)json_decode(file_get_contents($this->location), true);

        //$this->full = File::json('repos.map')->toArray()->return();

        $this->prepareData();
    }

    private function prepareMapData(array $map, string $type)
    {
        $result = [];

        foreach ($map['map'] as $title => $path) {

            $label = $title;

            if (!$this->str_contains($title, DIRECTORY_SEPARATOR)) {
                $title = $this->full['username.default'] . DIRECTORY_SEPARATOR . $title;
            }


            if ($this->str_starts_with($path, "!")) {
                $path = substr($path, 1);
            } elseif ($path === null) {
                $path = $this->full['repos.root'] . $map['root'] . $title;
            }

            $result[$label] = [
                'label' => $label,
                'title' => $title,
                'path' => $path,
                'type' => $type
            ];
        }

        return $result;
    }

    public function prepareData()
    {
        $result = array_merge(
            $this->prepareMapData($this->full['apps'], 'app'),
            $this->prepareMapData($this->full['packages'], 'package')
        );

        $this->preparedData = $result;
    }

    public function getAll()
    {
        return $this->preparedData;
    }

    public function getPath(string $label)
    {
        return $this->preparedData[$label]['path'];
    }

    public function getTitle(string $label)
    {
        return $this->preparedData[$label]['title'];
    }

    public function getType(string $label)
    {
        return $this->preparedData[$label]['type'];
    }

    public function getPackageUsers(string $packageLabel)
    {
        $result = [];

        $composer = new ComposerFile();
        $composerLock = new ComposerLockFile();

        $packageTitle = $this->getTitle($packageLabel);

        foreach ($this->preparedData as $label => $data) {
            $composer->parse($data['path']);

            if (in_array($packageTitle, array_keys($composer->getRequired()))) {

                $usedPackage = $composerLock->parse($data['path'])->findPackage($packageTitle);

                $result[$data['label']] = [
                    'path' => $data['path'],
                    'reference' => $usedPackage['source']['reference'],
                ];
            }

        }

        return $result;
    }

    public function add(string $label, string $type = 'package', ?string $path = null)
    {
        $this->full[$type . "s"]['map'][$label] = $path;

        // @todo Use File::json()
        // @todo No need for a separate dump()
        $this->dump();
        //File::array($this->location, $this->full)->add($path, [$type . "s", 'map', $label])->toJson()->save();
    }

    private function dump()
    {
        // @todo Use File::json()
        file_put_contents($this->location, json_encode($this->full, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        // @todo toJson() - add parameter $rewrite + check
        //File::array($this->location, $this->full)->toJson()->save();
    }

    // @todo Update usages
    public function getReposRoot()
    {
        return $this->full['repos.root'];
    }

    // @todo Update usages
    public function getDefaultUsername()
    {
        return $this->full['username.default'];
    }

    public function getPackageTypeRoot(string $type)
    {
        return $this->full[$type . "s"]['root'];
    }
}