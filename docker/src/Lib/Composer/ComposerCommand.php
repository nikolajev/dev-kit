<?php

namespace App\Lib\Composer;

use App\Lib\Logger\LoggerFull;

class ComposerCommand
{
    private string $composerlocation;

    private string $packageTitle;

    /**
     * @param string $composerlocation
     * @param string $packageTitle
     */
    public function __construct(string $composerlocation, string $packageTitle)
    {
        $this->composerlocation = $composerlocation;
        $this->packageTitle = $packageTitle;
    }

    // @todo Update later (more flexible)
    private function extractCorrespondingOutputLines(array $output)
    {
        $result = [];

        foreach ($output as $line) {
            if (
                str_contains($line, "- Upgrading") && str_contains($line, $this->packageTitle) &&
                !str_ends_with($line, ": Extracting archive")
            ) {
                $result[] = $line;
            }
        }

        return $result;
    }

    public function update(string $reference = null): bool
    {
        $cmd = "cd {$this->composerlocation} && ";

        $cmd .= $reference === null ?
            "composer update {$this->packageTitle}" :
            "composer update {$this->packageTitle}:dev-master#$reference";

        exec("$cmd 2>&1", $output);

        if (
            !in_array("Nothing to modify in lock file", $output) &&
            !in_array("Nothing to install, update or remove", $output)
        ) {
            $output = $this->extractCorrespondingOutputLines($output);

            (new LoggerFull())->log("COMPOSER UPDATE: $cmd\n" . implode(PHP_EOL, $output));
            show($cmd, $output);
            return true;
        }

        show($cmd, "Already up-to-date");
        return false;
    }

    public function createProject()
    {
        $cmd = "cd {$this->composerlocation} && composer create-project $this->packageTitle .";

        show($cmd);

        passthru($cmd);
    }
}