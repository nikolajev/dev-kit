<?php

namespace App\Lib\Composer;

// @todo DRY
use Nikolajev\Filesystem\File;

class ComposerLockFile
{
    private ?string $path = null;

    private array $data;

    public function __construct(string $path = null)
    {
        $this->setPath($path);
        $this->parse($this->path);
    }

    private function setPath(?string $path = null)
    {
        if ($path !== null) {
            $this->path = $path . DIRECTORY_SEPARATOR . "composer.lock";
        }
    }

    public function parse(string $path = null): self
    {

        $this->setPath($path);

        if ($this->path === null) {
            $this->data = [];
            return $this;
        }

        if (!file_exists($this->path)) {
            throw new \Exception("'{$this->path}' does not exist");
        }

        $this->data = json_decode(file_get_contents($this->path), true);

        // @todo ->toArray() - pass Params class as well (rewrite & ignoreExtension)
        /*$this->data =
            File::json($this->path, null)->toArray()->return();*/

        return $this;
    }

    public function findPackage(string $title): ?array
    {
        foreach ($this->data['packages'] as $package){
            if($package['name'] === $title){
                return $package;
            }
        }

        return null;
    }
}