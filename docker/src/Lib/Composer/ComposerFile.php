<?php

namespace App\Lib\Composer;

use Nikolajev\Filesystem\File;

class ComposerFile
{
    private ?string $path = null;

    private array $data;

    public function __construct(string $path = null)
    {
        $this->setPath($path);
        $this->parse($this->path);
    }

    private function setPath(?string $path = null)
    {
        if ($path !== null) {
            $this->path = $path . DIRECTORY_SEPARATOR . "composer.json";
        }
    }

    public function parse(string $path = null): self
    {

        $this->setPath($path);

        if ($this->path === null) {
            $this->data = [];
            return $this;
        }

        if (!file_exists($this->path)) {
            throw new \Exception("'{$this->path}' does not exist");
        }

        // @todo Use File::json()->toArray()->get()
        $this->data = json_decode(file_get_contents($this->path), true);
        //$this->data = File::json($this->path)->toArray()->return();

        return $this;
    }

    public function getRequired(bool $dev = false)
    {
        if ($dev) {
            return $this->data['require-dev'] ?? [];
        }

        return $this->data['require'] ?? [];
    }
}