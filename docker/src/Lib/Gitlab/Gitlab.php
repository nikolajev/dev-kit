<?php

namespace App\Lib\Gitlab;

use App\Lib\ReposMap\ReposMap;
use Dotenv\Dotenv;
use Nikolajev\Curl\Curl;
use Nikolajev\Curl\CurlParams;
use Nikolajev\Curl\JsonParams;

class Gitlab
{
    private string $gitlabToken;

    private string $packagistToken;

    public function __construct()
    {
        $env = new Dotenv(dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . "config");
        $env->overload();
        $this->gitlabToken = getenv('GITLAB_TOKEN');
        $this->packagistToken = getenv('PACKAGIST_TOKEN');
    }

    public function createProject(string $name, string $visibility = 'public')
    {
        /* @todo Remove later after updating nikolajev/curl README
         * $cmd = 'curl -s --header "PRIVATE-TOKEN: ' . $this->gitlabToken . '" -X POST "https://gitlab.com/api/v4/projects?name=' . $name . '&visibility=public"';
         * exec($cmd . " 2>&1", $output);
         * $response = json_decode($output[0]);
         */

        $response = Curl::json(
            "https://gitlab.com/api/v4/projects",
            (new CurlParams())
                ->method('POST')
                ->headers(["PRIVATE-TOKEN: " . $this->gitlabToken])
                ->urlQueryData(['name' => $name, 'visibility' => $visibility]),
            (new JsonParams())
                ->responseToArray(false)
        );

        $id = $response->id;
        $url = $response->http_url_to_repo;

        show('Create Gitlab repository', "GitLab id: $id", "URL: $url");

        return [$id, $url];
    }

    // @url https://stackoverflow.com/questions/52130188/how-to-delete-a-gitlab-repository-using-curl
    public function deleteProject(string $packageTitle): void
    {
        $packageTitle = str_replace("/", "%2F", $packageTitle);

        $response = Curl::json(
            "https://gitlab.com/api/v4/projects/$packageTitle",
            (new CurlParams())
                ->method('DELETE')
                ->headers(["PRIVATE-TOKEN: " . $this->gitlabToken])
        );

        showln("Delete Gitlab repository '$packageTitle'", $response);
    }

    public function packagistIntegration(int $id)
    {
        $defaultUsername = (new ReposMap())->getDefaultUsername();

        /* @todo Remove later after updating nikolajev/curl README
         * $cmd = "curl -s --header \"PRIVATE-TOKEN: {$this->gitlabToken}\" -X PUT 'https://gitlab.com/api/v4/projects/$id/integrations/packagist?username=$defaultUsername&token={$this->packagistToken}'";
         *
         * exec($cmd . " 2>&1", $output);
         */

        $response = Curl::json(
            "https://gitlab.com/api/v4/projects/$id/integrations/packagist",
            (new CurlParams())
                ->method('PUT')
                ->headers(["PRIVATE-TOKEN: " . $this->gitlabToken])
                ->urlQueryData(['username' => $defaultUsername, 'token' => $this->packagistToken])
        );

        showln('Packagist integration', $response);
    }

    public function createTodoIssue(int $id)
    {
        /* @todo Remove later after updating nikolajev/curl README
        $title = urlencode("To Do");
        $description = urlencode("- [ ] Initial release\n- [ ] README\n- [ ] tests");

        $cmd = "curl -s --header \"PRIVATE-TOKEN: {$this->gitlabToken}\" " .
            "-X POST 'https://gitlab.com/api/v4/projects/$id/issues?title=$title&description=$description'";

        exec($cmd . " 2>&1", $output);
        */

        $title = "To Do";
        $timestamp = (new \DateTime)->format("Y-m-d H:i");
        $description = "Created: $timestamp\n- [ ] Initial release\n- [ ] README\n- [ ] tests";

        $response = Curl::json(
            "https://gitlab.com/api/v4/projects/$id/issues",
            (new CurlParams())
                ->method('POST')
                ->headers(["PRIVATE-TOKEN: " . $this->gitlabToken])
                ->urlQueryData(['title' => $title, 'description' => $description])
        );

        showln('Create to-do issue in Gitlab', $response);
    }
}