<?php

namespace App\Lib\Dev;


class Helper
{
    public static function getEnvDirPath(string $filePath)
    {
        $packageTitle = implode(DIRECTORY_SEPARATOR,
            array_slice(
                explode(
                    DIRECTORY_SEPARATOR,
                    explode("/var/www/html/dev/tests/", $filePath)[1]
                ),
                0,
                2
            )
        );

        return "/var/www/html/dev/env/" . $packageTitle;
    }
}