<?php

namespace App\Lib\Packagist;

use App\Lib\ReposMap\ReposMap;
use Dotenv\Dotenv;
use Nikolajev\Curl\Curl;
use Nikolajev\Curl\CurlParams;

class Packagist
{
    private string $packagistToken;

    public function __construct()
    {
        $env = new Dotenv(dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . "config");
        $env->overload();
        $this->packagistToken = getenv('PACKAGIST_TOKEN');
    }

    public function createPackage(string $repoUrl)
    {
        $defaultUsername = (new ReposMap())->getDefaultUsername();

        /* @todo Remove later after updating nikolajev/curl README
         * $cmd = "curl -s -d '{\"repository\":{\"url\":\"$repoUrl\"}}' -X POST 'https://packagist.org/api/create-package?username=$defaultUsername&apiToken={$this->packagistToken}'";
         *
         * exec($cmd . " 2>&1", $output);
         */

        $response = Curl::json(
            "https://packagist.org/api/create-package",
            (new CurlParams())
                ->method('POST')
                ->urlQueryData(['username' => $defaultUsername, 'apiToken' => $this->packagistToken])
                ->postRequestData(['repository' => ['url' => $repoUrl]])
        );

        showln('Create packagist package', $response);
    }

    /* Should work in private packagist only, not allowed in public packagist (only from UI)
    public function deletePackage(string $packageTitle)
    {
        $defaultUsername = (new ReposMap())->getDefaultUsername();

        $response = Curl::json(
            "https://packagist.org/api/packages/$packageTitle",
            (new CurlParams())
                ->method('DELETE')
                ->urlQueryData(['username' => $defaultUsername, 'apiToken' => $this->packagistToken])
        );

        showln("Delete packagist package '$packageTitle'", $response);
    }
    */
}