<?php

namespace App\Lib\Git;

use App\Lib\ReposMap\ReposMap;
use Dotenv\Dotenv;

class Git
{
    private string $label;

    private string $url;

    private string $gitlabEmail;

    private string $gitlabPassword;

    private string $defaultUsername;

    private string $repoPath;

    public function __construct(string $label)
    {
        $this->label = $label;

        $reposMap = new ReposMap();
        $repoTitle = $reposMap->getTitle($this->label);
        $this->repoPath = $reposMap->getPath($this->label);

        $this->url = "gitlab.com/$repoTitle.git";

        $this->defaultUsername = (new ReposMap())->getDefaultUsername();

        $env = new Dotenv(dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . "config");
        $env->overload();

        $this->gitlabEmail = getenv('GITLAB_EMAIL');
        $this->gitlabPassword = getenv('GITLAB_PWD');

        $this->configure();
    }

    private function configure()
    {
        exec("git config --global user.email \"{$this->gitlabEmail}\"");;

        exec("git config --global user.name \"{$this->defaultUsername}\"");
    }

    public function _clone()
    {
        $reposMap = new ReposMap();

        $repoTitle = $reposMap->getTitle($this->label);

        $packageTypeRoot = $reposMap->getPackageTypeRoot($reposMap->getType($this->label));

        $cmd = "git clone https://{$this->url} {$reposMap->getReposRoot()}$packageTypeRoot$repoTitle";

        show($cmd);

        exec($cmd);
    }

    public function commitAndPushAll()
    {
        $cmdArray = [
            "cd {$this->repoPath}",
            "git add .",
            'git commit -a -m "Initial release"',
            "git push 'https://{$this->defaultUsername}:{$this->gitlabPassword}@{$this->url}' --all"
        ];

        $cmd = implode(" && ", $cmdArray);

        passthru($cmd);
    }

    public function getLatestRevisionNumber()
    {
        $cmdArray = [
            "cd {$this->repoPath}",
            "git rev-parse HEAD",
        ];

        $cmd = implode(" && ", $cmdArray);

        exec($cmd . " 2>&1", $output);

        return $output[0];
    }
}