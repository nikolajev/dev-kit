<?php

namespace App\Lib\Logger;

class Base
{
    protected $title;

    protected $logsRoot;

    public function __construct(string $title, string $logsRoot = "/var/www/html/logs/")
    {
        $this->title = $title;
        $this->logsRoot = $logsRoot;
    }

    public function log(string $message)
    {
        file_put_contents(
            $this->logsRoot . $this->title . ".log",
            (new \DateTime())->format("Y-m-d H:i:s") . " $message\n\n",
            FILE_APPEND
        );
    }
}