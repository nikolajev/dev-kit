<?php

namespace App\Lib\FileGenerator;

use App\Lib\ReposMap\ReposMap;

// @todo Rename to ComposerPackage (no Generator)
class ComposerPackageGenerator
{
    private string $title;

    private string $type;

    private string $path;

    public function __construct(string $label)
    {
        $reposMap = new ReposMap();

        // @todo $repo = $reposMap->get($label); $this->title = $repo->title();
        $this->title = $reposMap->getTitle($label);;
        $this->type = $reposMap->getType($label);
        $this->path = $reposMap->getPath($label);
    }

    public function generate(string $description)
    {
        $this->generateGitignore();
        $this->generateComposerJson($description);
        $this->generateREADME();
    }

    // @todo DRY
    private function generateGitignore()
    {
        $template = file_get_contents(
            dirname(__DIR__, 3) . DIRECTORY_SEPARATOR .
            implode(
                DIRECTORY_SEPARATOR,
                ['templates', 'FileGenerator', 'ComposerPackage', '.gitignore', $this->type . ".txt"]
            )
        );

        file_put_contents($this->path . DIRECTORY_SEPARATOR . ".gitignore", $template);
    }

    private function prepareNamespaceStringFromTitle()
    {
        list($username, $packageName) = explode("/", $this->title);

        // @todo Implement for username later as well
        // @todo Implement StringModifier::camelize() - CakePHP-like naming should be used
        $exploded = explode("-", $packageName);

        $exploded = array_map(function ($value) {
            return ucfirst($value);
        }, $exploded);

        return ucfirst($username) . '\\\\' . implode('', $exploded) . '\\\\';
    }

    // @todo DRY
    private function generateComposerJson(string $description)
    {
        $template = file_get_contents(
            dirname(__DIR__, 3) . DIRECTORY_SEPARATOR .
            implode(
                DIRECTORY_SEPARATOR,
                ['templates', 'FileGenerator', 'ComposerPackage', 'composer.json', $this->type . ".sprint.txt"]
            )
        );


        // @todo Implement in app template as well
        file_put_contents(
            $this->path . DIRECTORY_SEPARATOR . "composer.json",
            sprintf($template, $this->title, $description, $this->type, $this->prepareNamespaceStringFromTitle())
        );
    }

    // @todo DRY
    private function generateREADME()
    {
        $template = file_get_contents(
            dirname(__DIR__, 3) . DIRECTORY_SEPARATOR .
            implode(
                DIRECTORY_SEPARATOR,
                ['templates', 'FileGenerator', 'ComposerPackage', 'README.md', $this->type . ".txt"]
            )
        );

        file_put_contents($this->path . DIRECTORY_SEPARATOR . "README.md", $template);
    }
}