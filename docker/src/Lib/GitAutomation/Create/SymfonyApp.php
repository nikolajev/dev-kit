<?php

namespace App\Lib\GitAutomation\Create;

use App\Lib\Composer\ComposerCommand;
use App\Lib\Git\Git;
use App\Lib\Gitlab\Gitlab;
use App\Lib\ReposMap\ReposMap;

class SymfonyApp
{
    public function create($label, $version = null)
    {
        $version = $version ?? 'v5.4.99';

        $reposMap = new ReposMap;

        $reposMap->add($label, 'app');
        $reposMap->prepareData();

        $git = new Git($label);
        $gitlab = new Gitlab();

        list($gitlabId) = $gitlab->createProject($label, 'private');

        $git->_clone();

        $packagePath = $reposMap->getPath($label);

        exec("mv $packagePath/.git /var/www/html/tmp");

        (new ComposerCommand($packagePath, "symfony/website-skeleton:$version"))->createProject();

        exec("mv /var/www/html/tmp/.git $packagePath");


        file_put_contents(
            $packagePath . "/.gitignore",
            "\n\n###> Added by nikolajev/dev-kit ###\n/.env\n/.idea/\n/mysql/\n###< Added by nikolajev/dev-kit ###",
            FILE_APPEND
        );


        file_put_contents(
            $packagePath . "/docker-compose.yml",
            file_get_contents("/var/www/html/templates/FileGenerator/SymfonyApp/docker-compose.yml/app.txt")
        );


        exec("rm $packagePath/docker-compose.override.yml");


        file_put_contents("$packagePath/bin/run", "docker-compose up -d\ndocker-compose exec apache bash");
        exec("chmod 777 $packagePath/bin/run");

        // @todo bin/stop


        $git->commitAndPushAll();


        $gitlab->createTodoIssue($gitlabId);


        success();
    }
}