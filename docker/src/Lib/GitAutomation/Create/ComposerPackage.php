<?php

namespace App\Lib\GitAutomation\Create;

use App\Lib\FileGenerator\ComposerPackageGenerator;
use App\Lib\Git\Git;
use App\Lib\Gitlab\Gitlab;
use App\Lib\Packagist\Packagist;
use App\Lib\ReposMap\ReposMap;

class ComposerPackage
{
    public function create($label, $description)
    {
        (new ReposMap())->add($label, 'package');

        $git = new Git($label);
        $gitlab = new Gitlab();

        list($gitlabId, $gitlabUrl) = $gitlab->createProject($label);

        $git->_clone();

        (new ComposerPackageGenerator($label))->generate($description);

        $git->commitAndPushAll();

        (new Packagist())->createPackage($gitlabUrl);

        $gitlab->packagistIntegration($gitlabId);

        $gitlab->createTodoIssue($gitlabId);

        success();
    }
}