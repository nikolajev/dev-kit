<?php

namespace App\ConsoleLite\Git;

use App\Lib\Composer\ComposerCommand;
use App\Lib\Git\Git;
use App\Lib\ReposMap\ReposMap;
use Nikolajev\ConsoleLite\ConsoleCommand;

class UpdateAll extends ConsoleCommand
{
    public function exec()
    {
        list($label) = $this->getArguments();

        $reposMap = new ReposMap();

        $packageTitle = $reposMap->getTitle($label);

        $reference = (new Git($label))->getLatestRevisionNumber();

        foreach ($reposMap->getPackageUsers($label) as $usageData) {

            $path = $usageData['path'];
            $usedRevisionNumber = $usageData['reference'];

            if ($reference !== $usedRevisionNumber) {
                (new ComposerCommand($path, $packageTitle))->update($reference);
            }
        }
    }


}