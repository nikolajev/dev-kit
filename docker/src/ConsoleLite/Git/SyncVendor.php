<?php

namespace App\ConsoleLite\Git;

use App\Lib\ReposMap\ReposMap;
use App\Lib\Rsync\Rsync;
use Nikolajev\ConsoleLite\ConsoleCommand;

class SyncVendor extends ConsoleCommand
{
    public function exec()
    {
        $reposMap = new ReposMap();

        $repoPath = $reposMap->getPath($this->getArgument(1));
        $repoTitle = $reposMap->getTitle($this->getArgument(1));

        $vendorPackagePath = $reposMap->getPath($this->getArgument(0)) . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . $repoTitle;

        $success = (new Rsync($vendorPackagePath, $repoPath))->sync();

        success($success ? "Synced" : "Already up-to-date", true);
    }


}