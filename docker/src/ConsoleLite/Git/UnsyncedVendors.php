<?php

namespace App\ConsoleLite\Git;

use App\Lib\ReposMap\ReposMap;
use App\Lib\Rsync\Rsync;
use Nikolajev\ConsoleLite\ConsoleCommand;

class UnsyncedVendors extends ConsoleCommand
{
    public function exec()
    {
        $result = [];

        $reposMap = new ReposMap();

        foreach ($reposMap->getAll() as $label => $packageData) {
            foreach ($reposMap->getPackageUsers($label) as $userLabel => $usageData) {

                $path = $usageData['path'];

                $fromPath = $path . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . $packageData['title'];

                $unsynced = (new Rsync(
                    $fromPath,
                    $packageData['path']
                ))->areDifferent();

                if ($unsynced) {
                    $result[$label] = [
                        'location label' => $userLabel,
                        'location path' => $fromPath,
                        'commands' => [
                            "docker >  bin/lite git/syncVendor $userLabel $label  ",
                            "  root >  bin/open $label  ",
                            "docker >  bin/lite git/updateAll $label  ",
                        ]
                    ];
                }
            }
        }

        show($result);
    }
}