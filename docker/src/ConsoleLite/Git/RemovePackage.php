<?php

namespace App\ConsoleLite\Git;

use App\Lib\Gitlab\Gitlab;
use App\Lib\ReposMap\ReposMap;
use Nikolajev\ConsoleLite\ConsoleCommand;
use Nikolajev\Filesystem\File;

class RemovePackage extends ConsoleCommand
{
    // @todo Separate service/Lib!
    public function exec()
    {
        if (empty($this->getArgument(0))) {
            // @todo failureexit()
            failure("Package label missing");
            exit;
        }

        $label = $this->getArgument(0);

        $reposMap = new ReposMap();

        $type = $reposMap->getType($label);
        $title = $reposMap->getTitle($label);
        $path = $reposMap->getPath($label);

        if ($type === null) {
            failure("Package '$label' unknown");
            exit;
        }


        show("Remove from repos.map.json");

        // @todo File::json()
        $reposMapContents = json_decode(file_get_contents("/var/www/html/repos.map.json"), true);
        unset($reposMapContents["{$type}s"]['map'][$label]);
        // @todo File::json()
        file_put_contents(
            "/var/www/html/repos.map.json",
            json_encode($reposMapContents, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
        );

        /*
        File::json('repos.map')
            ->toArray()->unset(["{$type}s", 'map', $label])
            ->toJson()->save();
        */
        success(null, false);


        show("Remove from repositories");
        if (file_exists("$path/mysql")) {
            exec("sudo rm -rf $path/mysql");
        }
        exec("rm -rf $path");
        success(null, false);

        show("Remove from Gitlab");
        (new Gitlab())->deleteProject($title);
        success(null, false);


        if ($type !== 'app') {
            show(
                "Do not forget to remove from packagist!",
                "https://packagist.org/packages/$title",
                "Click 'Delete' / 'Abandon' button"
            );
        }

    }
}