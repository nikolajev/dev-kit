<?php

namespace App\ConsoleLite\Git;

use App\Lib\GitAutomation\Create\SymfonyApp;
use Nikolajev\ConsoleLite\ConsoleCommand;
use App\Lib\GitAutomation\Create\ComposerPackage;

class NewPackage extends ConsoleCommand
{
    // @todo Separate service/Lib!
    public function exec()
    {
        if (empty($this->getArgument(1))) {
            // @todo Use global config that can be updated on the fly. Singleton pattern?
            failure("Description missing!  bin/lite git/newPackage any-title 'Description here'  ");
            exit;
            // @todo cli_show() Show only if on CLI
            throw new \Exception("No description.  bin/lite git/newPackage any-title 'Description here'  ");
        }

        $label = $this->getArgument(0);
        $description = $this->getArgument(1);
        $type = $this->getArgument(2) ?? 'package';

        switch ($type) {
            case "package":
                (new ComposerPackage())->create($label, $description);
                break;
            case "symfony-app":
                $version = $this->getArgument(3) ?? null;
                // @todo Implement description later ($description)
                (new SymfonyApp())->create($label, $version);
                break;
            default:
                failure("Package type '$type' unknown");
        }
    }
}