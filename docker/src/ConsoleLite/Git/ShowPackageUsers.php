<?php

namespace App\ConsoleLite\Git;

use App\Lib\Git\Git;
use App\Lib\ReposMap\ReposMap;
use Nikolajev\ConsoleLite\ConsoleCommand;

class ShowPackageUsers extends ConsoleCommand
{
    public function exec()
    {
        $result = [];

        list($label) = $this->getArguments();

        if($label === null){
            // @todo exception()
            failure('No label passed');exit;
            //throw new \Exception('No label passed');
        }

        $reposMap = new ReposMap();

        $packageTitle = $reposMap->getTitle($label);

        $reference = (new Git($label))->getLatestRevisionNumber();

        foreach ($reposMap->getPackageUsers($label) as $label => $usageData) {

            $path = $usageData['path'];
            $usedRevisionNumber = $usageData['reference'];
            $result[$label] = [
                $usedRevisionNumber, $path . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . $packageTitle,
            ];
        }

        show($packageTitle, $reference, $result);
    }
}