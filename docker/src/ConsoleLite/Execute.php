<?php

namespace App\ConsoleLite;

use App\Lib\ReposMap\ReposMap;
use Nikolajev\ConsoleLite\ConsoleCommand;

class Execute extends ConsoleCommand
{
    public function exec()
    {
        $packageLabel = $this->getArgument(0);

        $scriptPath = $this->getArgument(1);

        $reposMap = new ReposMap();

        $repoPath = $reposMap->getPath($packageLabel);

        passthru("cd $repoPath && php $scriptPath");
    }
}