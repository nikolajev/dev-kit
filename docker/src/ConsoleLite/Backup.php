<?php

namespace App\ConsoleLite;

use App\Lib\ReposMap\ReposMap;
use Nikolajev\ConsoleLite\ConsoleCommand;

class Backup extends ConsoleCommand
{
    public function exec()
    {
        $reposMap = new ReposMap();

        $userApp = $this->getArgument(1) ?? 'dev-kit';

        $title = $reposMap->getTitle($this->getArgument(0));
        $userAppPath = $reposMap->getPath($userApp);

        $packageLocation = "$userAppPath/vendor/$title";

        $backupLocation = "/var/www/html/backup/$title." . (new \DateTime())->format("YmdHis");

        exec("mkdir -p $backupLocation && cp -R $packageLocation/. $backupLocation");

        success();
    }
}