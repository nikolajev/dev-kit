<?php

namespace App\ConsoleLite;

use App\Lib\ReposMap\ReposMap;
use Nikolajev\ConsoleLite\ConsoleCommand;

class Dev extends ConsoleCommand
{
    public function exec()
    {
        $packageLabel = $this->getArgument(0);

        $reposMap = new ReposMap();

        $repoTitle= $reposMap->getTitle($packageLabel);

        show($repoTitle);

        $rootPath = dirname(__DIR__, 2);

        foreach (array_filter(glob("$rootPath/dev/tests/$repoTitle/*"), 'is_file') as $file) {
            show("Executing: " . basename($file));
            require_once $file;
        }
    }
}