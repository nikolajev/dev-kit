<?php

namespace App\ConsoleLite;

use App\Lib\ReposMap\ReposMap;
use Nikolajev\ConsoleLite\ConsoleCommand;

class Test extends ConsoleCommand
{
    public function exec()
    {
        $packageLabel = $this->getArgument(0);

        $reposMap = new ReposMap();

        $repoPath = $reposMap->getPath($packageLabel);

        passthru("cd $repoPath && vendor/bin/phpunit tests");
    }
}