<?php

use Nikolajev\DataObject\Data;

$Array = Data::array([
    'colors' => [
        'failure' => 'red',
        'success' => 'green',
    ],
    'codes' => [
        'failure' => 0,
        'success' => 1,
    ],
    'names' => [
        'Philipp',
        'Natalja',
        'Mia Maria',
    ]
]);

$a = clone $Array;


show(
    $a->isAssoc(),
    $a->select('names')->isAssoc(),
    $a->select('codes')->return()
);


/*
$Array->rewrite([
    'colors' => [
        'failure' => 'orange',
    ],
    'codes' => [
        'success' => '01',
    ]
]);

$b = clone $Array;


$Array->rewrite([
    'colors' => [
        // Expect exception
        //'failure' => ['pink'],
        'failure' => 'pink',
    ],
]);

$c = clone $Array;

$Array->rewrite([
    // Expect exception
    'colors' => ['failure'],
    'test' => 'me',
]);

$Array->rewrite([
    // Expect exception
    'test' => 'me',
]);

$d = clone $Array;
*/
/*showln(
    $a->return(),
    $b->return(),
    $c->return()
);*/

$d = Data::array([
    'config' => [
        'params1' => [
            'param1.1' => 1,
            'param1.2' => 'second',
            'params1.3' => [
                'param1.3.1' => 1,
                'param1.3.2' => 'second',
            ]
        ],
        'param2' => 2,
        'param3' => 'third',
    ]
])
    ->selectOnce(['config', 'params1'])
    ->recursiveWalk(function (&$value) {
        if (is_numeric($value)) {
            $value = "__$value";
        }
    });

show($d->return()/*, $d->select()->return() /*$d->select(['config', 'params1'])->values()->first()*/);

//$d->select(['config', 'params1'])->debugSelected()->select()->debugSelected();

exit;