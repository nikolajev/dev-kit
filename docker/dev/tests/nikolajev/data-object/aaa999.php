<?php

use Nikolajev\DataObject\Data;
use Nikolajev\DataObject\ArrayObject;

$Array = Data::array()->merge([
    [1, 2, 3]
]);

$a = clone $Array;

$Array
    ->merge([
        [4, 5, 6],
    ])
    ->walk(function ($value) {
        if ($value === 4) {
            return ArrayObject::WALK__UNSET;
        }
    });

showln(
    $a->return(),
    $Array->return()
);

debug($Array->return());


$Array = Data::array([
    'a' => 1,
    'b' => 2,
    '999' => 999,
]);

debug($Array->return(), $Array->isAssoc());

$Array->walk(function ($value) {
    if ($value === 2) {
        return ArrayObject::WALK__UNSET;
    }
});

debug($Array->return(), $Array->isAssoc());

$Array->walk(function ($value) {
    if ($value === 1) {
        return ArrayObject::WALK__UNSET;
    }
});

debug($Array->return(), $Array->isAssoc());


exit;