<?php

use Nikolajev\Curl\Curl;
use Nikolajev\Curl\CurlParams;
use Dotenv\Dotenv;

$env = new Dotenv(dirname(__DIR__, 4) . DIRECTORY_SEPARATOR . "config");
$env->overload();

$token = getenv('GITLAB_TOKEN');

$url = "https://gitlab.com/api/v4/projects?private_token=$token&owned=true";
$url = "https://gitlab.com/api/v4/projects?owned=true";
$url = "https://gitlab.com/api/v4/projects";
$url = "https://gitlab.com/api/v4/projects?";

// @todo Debugger::plain() - no labels or any extra data (only output). Required for tests (do not catch debugger changes), sets $GLOBALS

show(
    json_decode(Curl::sendRequest(
        $url,
        (new CurlParams)
            ->agent('Agent')
            ->headers(["PRIVATE-TOKEN: $token"])
            ->urlQueryData(['owned' => true])
    ), true)[0]['name']
);

show(
    Curl::json(
        $url,
        (new CurlParams)
            ->headers(["PRIVATE-TOKEN: $token"])
            ->urlQueryData(['owned' => true])
    )[0]['name']
);