<?php

use Nikolajev\Filesystem\File;

show(
    File::json('composer.lock', '')->ignoreExtension()->getFullPath(),
    File::json('composer.lock2')->getFullPath(),
    File::json('composer.lock2', '')->getFullPath(),
);

exit;