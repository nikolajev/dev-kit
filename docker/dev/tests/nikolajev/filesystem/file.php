<?php

use Nikolajev\Filesystem\File;

$newArray = [];

debug(
// @todo 3rd parameter $rewrite (if file exists, but data provided) - defaults to false
    File::array('test', [
        'test' => 'me',
        'baby' => 'girl',
        // @todo true obsolete
    ], true)->unset('test')->return(),

    File::array('test')
        ->add('please', ['test', 'me'])
        ->filter(function ($value) {
            return !is_array($value);
        })
        ->add('me', 'test')
        ->map(function ($value) {
            return "$value!!!";
        })
        ->walk(function (&$value, $key) use (&$newArray) {
            $newArray[$key] = $value;
            if ($value === 'girl!!!') {
                return File\File2PhpArray::WALK__UNSET;
            }

            $value = "???$value";

            return [File\File2PhpArray::WALK__UPDATE_KEY, "???$key"];
        })
        ->var_export(),

    File::array('new-test', [])
        ->add('please', ['test', 'man', 'me'])
        ->add('now', ['test', 'man', 'me2'])
        ->filter(function ($value) {return $value === 'now';}, ['test', 'man'])
        ->map(function ($value) {return "$value!!!";}, ['test', 'man'])
        ->return(),

    File::array('new-test', [])
        ->add('please', ['test', 'man', 'me'])
        ->add('please2', ['test', 'man', 'me2'])
        //->filter(function ($value) {return $value === 'now';}, ['test', 'man'])
        //->map(function ($value) {return "$value!!!";})
        ->walk(function (&$value, $key) {
            if ($value === 'please') {
                return File\File2PhpArray::WALK__UNSET;
            }

            $value = "???$value";

            return [File\File2PhpArray::WALK__UPDATE_KEY, "???$key"];

        }, ['test', 'man'])
        ->return(),

);

show($newArray);

exit;

// @todo Debugger::exitOnFailure() - default value is TRUE, can be set to FALSE (uses $GLOBALS)

// @todo Use testing env or create composer.json inside test folder itself (required in this very case) - ONLY STATIC DATA!
show(
    File::json('/var/www/test/composer.json')->getFullPath(),
    File::json('./composer.json')->getFullPath(),
    File::json('composer.json')->getFullPath()
);

show(
    File::json('/var/www/test/composer.json')->getType(),
    File::json('./composer.txt')->getType(),
    File::json('composer')->getType()
);

show(File::json('/var/www/test/composer.json'));

success();
//exit;

debug(File::json('composer.json')->toArray()->return());

debug(File::json('composer')->getFullPath());

$type = 'app';

debug(
    File::json('composer')->toArray()->return(),
    File::json('composer')->toArray()->unset(['config', 'bin-dir'])->toJson()->return()
);
//exit;

show(File::json('composer.json')->toArray());
show(File::json('composer.json')->toArray()->toJson());

show(
    File::json('composer.json')->toArray()->toJson()->return(),
    File::json('composer.json')->toArray()->toJson(JSON_UNESCAPED_UNICODE)->return()
);

// @todo 'php-array' automatically adds ext 'php' in getFullPath(), 'json' type file adds 'json' extension automatically
dump(
    File::json('test.json', '')->toArray()->setData(['test' => 'me'])->toJson(),//->return(),
    File::array('test.php', ['test' => 'me'])->toJson(),//->return(),
    File::array('test', ['test' => 'me'])->toJson()//->return()
);

exit;