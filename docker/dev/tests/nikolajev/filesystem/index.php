<?php

use App\Lib\Dev\Helper;
use Nikolajev\Filesystem\FilesList;
use Nikolajev\Filesystem\FilesListParams;

$envDirPath = Helper::getEnvDirPath(__FILE__);

show(FilesList::list(
    $envDirPath,
    (new FilesListParams)
        ->excludedDirPatterns(['*/_git'])
        ->includedFilenamePatterns(['*.json'])

));
