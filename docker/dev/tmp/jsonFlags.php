<?php

require_once dirname(__DIR__, 2) . "/vendor/autoload.php";

show(
    JSON_PRETTY_PRINT,
    JSON_UNESCAPED_SLASHES,
    JSON_UNESCAPED_UNICODE,
    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES,
    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
    JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
);

const ONE = 1;
const TWO = 2;
const THREE = 3;

show(
    ONE,
    TWO,
    THREE,
    ONE | THREE,
    TWO | THREE
);

const _ONE = 128;
const _TWO = 64;
const _THREE = 256;

show(
    _ONE,
    _TWO,
    _THREE,
    _ONE | _THREE,
    _TWO | _THREE
);
