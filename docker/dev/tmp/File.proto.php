<?php

// @todo Update all file_get_contents / file_put_contents
class File
{
    public function setType()
    {

    }

    public function toArray()
    {

    }
}

File::json('composer.json')->toArray(); // array

File::json('composer.lock')->toArrayObject()->filter();

File::json('composer.json')->update(string '{}');
File::json('composer.json')->update(array []);
File::json('composer.json')->update(ArrayObject $data);

$file = File::json('repos.map.json');
$data = $file->toArrayObject()->unset(...);
$file->rewrite($data);

// $data = (new ArrayObject([]))->sort();

// ->rewrite() / ->append() / ->prepend() / ->delete() / ->create()

// ->sort()


/**

NB!!! No need to use actual file_exists checks in __constructor
 * 
 * AVOID ANY AUTOMATIC EXTENSION! always provide extension if required!
 *
 * this checks are required only when writing to file (rewrite), not reading
 *
 * ignore-extension option required! composer.lock is in JSON format
 *
 * ->toArray() should not check any actual file (ignore extension false by default)
 *
 * ->ignoreExtension() resets data!
 *
 * ->save() - if new file (add state), then save() works OK. If already exists, then ->save($rewrite true) - defaults to false (avoid human mistakes)
 *
 * __constructor: ignoreExtension() automatically if file exists without extension and isJson()
 *
 * ->toArray(): pass caller object ( jsonFile )
 *
 * ->toArray() - state: virtual file
 *
 * state: exists/new/virtual
 *
 * caller: null || FileBase
 *
 */

File::json('composer.lock')->setData()->save(/*rewrite*/ true);

$file = new File('composer.json'); // Automatically uses composer ROOT. (relative to root)

// ./composer.json - relative

// /var/www/html/composer.json - absolute

// Automatically identifies type  returns File\Json or:

$file->setType('txt');

File::json('', true)->toArray()->unset()->toJson()->save();


$jsonFile = new File('composer.json');
$jsonFile->toArray(); // returns File\Array

$jsonFile->toArray()->toCsv();

$jsonFile->merge();

$jsonFile->append()

    $jsonFile->rewriteMerge()

        $jsonFile->unset();

// ??
$jsonFile->toArray()->foreach(function ($value, $key) {

});

$jsonFile->toObject();

$jsonFile->get();

$jsonFile->get(['autoload', 'psr-4']); // gets File\Array

$array = new File\AssociativeArray();

$array->keys()->first();

$array->values()->first();

$array = new File\IndexedArray();

$array->first();

$array->first();


/// Both
$array->foreach(function ($value, $key) {

});


    $jsonFile->toArray()->toXml()->save();

    $jsonFile->delete();
    $jsonFile->move();
    $jsonFile->copy();
    $file->isEmpty();

    (new File('repos.map.json'))->toArray()->return()['key'];


    // LIVE EXAMPLE

    $reposMapContents = json_decode(file_get_contents("/var/www/html/repos.map.json"), true);
        unset($reposMapContents["{$type}s"]['map'][$label]);
        // @todo File::json()
        file_put_contents(
            "/var/www/html/repos.map.json",
            json_encode($reposMapContents, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)
        );


        // ONE LINER
    $jsonFile->toArray()->unset(["{$type}s", 'map', $label])->toJson((JsonParams())->prettyPrint(false))->save();

    $jsonFile
        ->toArray()
        ->unset(["{$type}s", 'map', $label])
        ->toJson((JsonParams())->prettyPrint(false))
        ->save();


    $jsonFile->toArray()->keys()->first();
    $jsonFile->toArray()->values()->first();
    $jsonFile->toArray()->get([$key1, $key2]); /// return() !!!
///  get() return PhpArrayFile or TextFile (if value is nto array: string, integer, etc.)