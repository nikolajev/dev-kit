<?php

// nikolajev/filesystem
// nikolajev/data-object (Nikolajev/DataObject/Array + use Nikolajev\Data)

File::json('composer.json')->toArray(); // nikolajev/filesystem
File::json('composer.json')->toString(); // nikolajev/filesystem

File::json('composer.json')->toArrayObject(); // nikolajev/data-object || nikolajev/data
File::json('composer.json')->toStringObject(); // nikolajev/data-object || nikolajev/data
// StringObject is an ultimate string parser & modifier

File::json('composer.json')->toHtmlStringObject(); // nikolajev/data-object || nikolajev/data

File::json('composer.json')->toArrayFile('composer.json.php')->create(); // nikolajev/filesystem
File::json('composer.json')->toStringFile('copy.composer.json')->create(); // nikolajev/filesystem

File::json('composer.json')->toArrayObject()->toCvsFile(); // nikolajev/filesystem

// ->update()
# On the background:
(new Nikolajev\DataObject\Array(File::json('composer.json')->toArray()))->toFile('composer.json.php');

#  Better
(new Nikolajev\Data\ArrayObject(File::json('composer.json')->toArray()))->toFile('composer.json.php');


Data::array(['test' => 'me'])
    ->filter($callback)
    ->return();


$file = File::json('repos.map.json');
$file->update($file->toArray());
$file->update($file->toArrayObject()->unset([$a, $b]));

File::json('new.json')->create(Data::array($a)->walk($callback));

# class ... extends DataObject
public function return();
public function toFile($title);

# class ... extends FileObject
public function create(string|array|DataObject|File $contents = null);
public function update(string|array|DataObject|File $contents = null);
# Specify modification chains for each File type or throw exception 'Does no support'

// + Array of files

// If $contents === null, then $this->data !== null
// If $contents !== null, then $this->data === null


File::json('composer.json')->toArrayObject()->unset(array $keys)->toFile(); // returns FileArray || PhpArrayFile

File::json('composer.json')->toArrayObject()->unset(array $keys)->backToFile(); // returns FileJson || JsonFile

File::json('composer.json')->toArrayObject()->unset(array $keys)->backToFile()->update();

// Modification history required for backToFile()
$this->modificationHistory = [
    [JsonFile::class, 'composer.json'],
    [ArrayObject::class],
];

# Nikolajev\Filesystem\File\JsonFile;
# Call with File::json()

# Nikolajev\Data\ArrayObject;
# call with Data::array()