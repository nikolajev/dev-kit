<?php

# This will merge everything (does not care about structure

File::csv('binance-report-2021.csv')->toArrayObject()->merge(
    File::csv('binance-report-2021_1.csv')->toArray(),
    File::csv('binance-report-2021_2.csv'), // ->toArrayObject() by default
    File::csv('binance-report-2021_3.csv'),
    File::csv('binance-report-2021_4.csv'),
);


class ArrayObjectMergeMethodParams
{
    public function sameRowFieldCount(): self
    {
        return $this;
    }

    public function sameFieldTypes(): self
    {
        return $this;
    }
}

class ArrayObjectMethodParams
{
    public static function merge(): ArrayObjectMergeMethodParams
    {
        return new ArrayObjectMergeMethodParams;
    }
}

class ArrayObject
{
    public static function methodParams(): ArrayObjectMethodParams
    {
        return new ArrayObjectMethodParams;
    }
}

class Data
{
    public static function array(): ArrayObject
    {
        return new ArrayObject;
    }
}


# Better
Data::array()
    ->merge(
        [
            File::csv('binance-report-2021_1.csv')->toArray(),
            File::csv('binance-report-2021_2.csv'), // ->toArrayObject() by default  [or just toArray()]
            File::csv('binance-report-2021_3.csv'),
            File::csv('binance-report-2021_4.csv'),
        ],
        //(new ArrayMergeParams())->sameRowCount(), // Might be obsolete, but more safe
        //->sameFieldTypes() # later

        # Better
        Data::array()->methodParams()->merge()
            ->sameRowFieldCount()
            ->sameFieldTypes()
    )
    ->sortByField( // or groupByField() & sortByField() separately
        2, // timestamp
        Data::array()->methodParams()->sortByField()
            ->createGroups( // This is a custom method, cannot be rendered automatically (Params classes should be easily extendable)
                function ($field1A, $fieldB) { // Grouping principe validation callback
                    return $field1A === $fieldB;
                },
                function ($field1) { // Group naming callback
                    return "group_$field1";
                }
            )
            # Better
            ->groupByField() // Auto
    )
    ->walk(function ($event) {
        Data::array($event) // Event types: buy crypto/ sell crypto/ get free crypto / deposit fiat / withdraw fiat / stake crypto

        // + event subtypes (Operation from csv)

            ->walk(function ($action){
                list($timestamp, $asset, $difference) = $action;

                $actionType = null;
                // spend / receive / fee

            });
    });

# if Data::array()->merge() gets File or 'array of Files (ArrayObject compatible) and other arrays', all of them are automatically converted to arrays