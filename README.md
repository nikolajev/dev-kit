# INIT

composer require nikolajev/dev-kit

cd dev-kit && bin/run

composer install

#  RUN

`bin/run`

# REQUIREMENTS

### Machine

- Linux OS
- PHP 7.2+
- Docker + docker-compose
- PhpStorm (any other IDE, 1 sec modifications required - change terminal command input)
- Curl
- Firefox (or any other browser, 1 sec modifications required - change terminal command )

### Online

- Gitlab free account
- Packagist free account

# .ENV `config/.env`

```env
export GITLAB_TOKEN=
export GITLAB_PWD=
export GITLAB_EMAIL=

export PACKAGIST_TOKEN=
```

# REPOS MAP.json `new App\Lib\ReposMap()`

```json
{
    "username.default": "nikolajev",
    "repos.root": "/var/www/repositories/",
    "apps": {
        "root": "Apps/",
        "map": {
            "dev-kit": "!/var/www/html"
        }
    },
    "packages": {
        "root": "Packages/",
        "map": {
            "debugger": null,
            "console-lite": null,
            "tests-lite": null,
            "logger-lite": null
        }
    }
}
```

# USAGE

## CONSOLE COMMANDS (FROM DOCKER)

```text
You are free to modify your vendor packages from inside any repository
(located inside dev-kit)
```

- Find unsynced vendor packages

`bin/lite git/unsyncedVendors`

- Sync vendor package

`bin/lite git/syncVendor` <location app/package label> <package-label>

bin/lite git/syncVendor dev-kit console-lite

`/var/www/html/vendor/`


## CONSOLE COMMANDS (FROM ROOT)

- Open package in PhpStorm (or other IDE)

`bin/open` <package-label>

bin/open console-lite

```text
Commit and push with the help of IDE (PhpStorm by default)
```

## CONSOLE COMMANDS (FROM DOCKER)

- Update package everywhere it is required

`bin/lite git/updateAll` <package-label>

bin/lite git/updateAll console-lite

- Create new package

`bin/lite git/newPackage` <any-title> '<description>'

bin/lite git/newPackage console-lite 'Lightweight PHP console utility'

## Testing

`bin/lite test data-object`


