* git/newPackage LOGGING
* git/newPackage DO NOT USE show()
* README.md
* git/deleteRepo (From everywhere)



# SHOW DEV SCRIPTS OUTPUT (ALL files together or separately) + COMPARE WITH SNAPSHOTS

`bin/lite dev dev-kit` (exec all scripts inside dev-kit)

`bin/lite dev dev-kit console-lite` (certain)

```
Simple script how dev-kit uses console-lite
```

# Git/NewPackage

Create issue #1 'To Do'

# All docker/src/Lib packages separately! (extract logics, not copy+paste)

# Telegram BOT (notifier)

# Simple tests

# Create a new Symfony app (all routine) + define config

# Update existing composer packages (.gitattributes, psr-4 autoload, etc.)

# Create dev files from console (add autoload.php automatically) NB!!! - No need for autoload.php

# nikolajev/dir (filesystem)

# nikolajev/api-lite (from renderscript)

# nikolajev/orm-lite (from coinbase pro bot)

# render Curl\Params-like classes automatically from console (nikolajev/params)

# Buy again renderscript.com (vuevuezela.com, vuequery.com, clitext.com)

# Republish Coinbase pro bot

# Gitlab CI (docker-compose)

# nikolajev/params 'bin/lite -p=params render' - Renders all 'params.render' files automatically

# nikolajev/console-lite plugins (Params should render required classes)

# nikolajev/filesystem (combines /file & /dir)

# Symfony plugins

# LARAVEL plugins (later)

# nikolajev/logger-lite  $logger = new Logger('full'). Get rid of LoggerFull

# Republish nikolajev/array-mixer

# nikolajev/debugger Debugger::return(show()) || Debugger::return(export())

Use Debugger::plain() + Debugger::show() in nikolajev/tests-lite

# Fetch unfinished 'to-do' tasks from Gitlab

# nikolajev/params - set allowed options (CurlParams::method()  GET, POST, PUT, DELETE)

# Check that all packages require other packages that they use (not only using dev-kit composer)

# repos.map.json -> config/repos.map.json

# bin/lite git/updateAll - show Gitlab response! Always!

/***
# Solve circular composer reference issue (use tag): filesystem <-> params 
NB!!! Split params: params & render-params
*/

# New functionality: Remove all comments in files 

# docker/templates/FileGenerator/ComposerPackage rename sprint.txt -> sprintf.txt

# 'bin/lite uns' does not show those packages that are not a first level dependency

Do not use only composer.json files but actual vendors as well

# Publish nikolajev/render-params as standalone plugin

# !!! Better solution

composer require nikolajev/render-params

bin/render-params

(Get rid of console-lite dependency)

# final protected function getOrSet(array $definedVars): mixed

# Move this todo contents to dev-kit Gitlab ToDo issue

# Check internet connection before execution of API actions

# Replace 'var/www/html/' everywhere

# 'bin/lite uns' - show diff

# 'bin/lite -p=params render' - render in vendor as well

# repos.map.json 

map:
debugger,
api-lite

exceptions-map:
debugger: !/var/www/html
...

# bin/lite backup vendor packages (nikolajev only) to be sure nothing disappeared + check later automatically after everything synced

bin/lite vendor/backup

# nikolajev/filsystem FilePhpArray->unset(): pass array instead of unknown num of parameters

# rollback vendor changes command required: remove package from vendor + composer update nikolajev/filesystem

# <?php declare(strict_types=1);

# Better key naming (location label -> user package label)

    "filesystem": {
        "location label": "data-object",
        "location path": "/var/www/repositories/Packages/nikolajev/data-object/vendor/nikolajev/filesystem",
        "commands": [
            "docker >  bin/lite git/syncVendor data-object filesystem  ",
            "  root >  bin/open filesystem  ",
            "docker >  bin/lite git/updateAll filesystem  "
        ]
    },


# Go to TAGS! At the moment dev-kit allows only dev-master without tags. Try separately

# FAILURE  'bin/lite git/syncVendor filesystem data-object'

tests/ should be excluded (otherwise syncing removes them)



